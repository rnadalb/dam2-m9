### **Excepcions i maneig d’errors: els perills del viatge espacial**

En aquest apartat abordarem el concepte de **gestió d'excepcions i maneig d'errors**, un aspecte essencial en la
programació per garantir que els programes siguin robustos i puguin gestionar situacions inesperades sense fallar.

### **Què són les excepcions?**

Una **excepció** és un esdeveniment que ocorre durant l'execució d'un programa i interromp el flux normal de les
instruccions. Les excepcions poden ser causades per errors previsibles (com dividir per zero, accedir a un element fora
dels límits d'un array, o esgotar el combustible en una nau) o per circumstàncies imprevisibles.

El **maneig d'excepcions** ens permet capturar aquests errors i reaccionar-hi de manera adequada, evitant que el
programa es trenqui.

### **Sintaxi del maneig d'errors*:

El maneig d'excepcions en Java segueix el següent patró:

```java
try {
    // Codi que pot generar una excepció
} catch (ExceptionType e) {
    // Codi per gestionar l'excepció
} finally {
    // Codi que s'executa sempre, independentment de si es llança o no una excepció
}
```

- **`try`**: bloc de codi que pot generar una excepció.
- **`catch`**: bloc que captura l'excepció i permet gestionar-la.
- **`finally`**: opcional, aquest bloc s'executa tant si hi ha excepció com si no.

### **Excepcions al context del viatge espacial**

En el viatge espacial, podem imaginar diversos tipus de problemes que podrien ocórrer i que necessitarien ser gestionats
amb excepcions:

1. **Manca de combustible**: si una nau intenta accelerar però no té prou combustible, s'hauria de llançar una excepció.
2. **Danys greus a la nau**: si una nau intenta realitzar maniobres amb danys massa grans, s'ha de gestionar l'error i
   impedir que es destrueixi la nau.
3. **Errors de navegació**: si una nau intenta navegar fora dels límits d'un sistema solar o cap a una zona perillosa,
   s'ha de gestionar aquest error.

### **Implementació**

#### **Classe `FuelException`**

Comencem definint una excepció **personalitzada** que es llançarà quan una nau es quedi sense combustible.

```java
// Excepció personalitzada per gestionar la manca de combustible
class FuelException extends Exception {
    public FuelException(String message) {
        super(message);
    }
}
```

#### **Classe `DamageException`**

Ara, una altra excepció personalitzada per gestionar danys greus a la nau.

```java
// Excepció personalitzada per gestionar els danys greus a la nau
class DamageException extends Exception {
    public DamageException(String message) {
        super(message);
    }
}
```

#### **Classe `Spaceship` amb gestió d'errors**

A continuació, actualitzem la classe `Spaceship` per integrar aquestes excepcions.

```java
// Classe base que representa una nau espacial genèrica amb gestió d'excepcions
public class Spaceship {
    private String name;
    private int fuelLevel;
    private int damageLevel;
    private int maxFuelCapacity;
    private final int maxDamageLevel = 100;  // La nau queda destruïda si supera aquest nivell

    // Constructor per inicialitzar la nau
    public Spaceship(String name, int fuelCapacity) {
        this.name = name;
        this.maxFuelCapacity = fuelCapacity;
        this.fuelLevel = fuelCapacity; // Inicialment, la nau té el màxim de combustible
        this.damageLevel = 0; // La nau comença sense danys
    }

    // Mètode per obtenir el nom de la nau
    public String getName() {
        return name;
    }

    // Mètode per accelerar la nau
    public void accelerate(int fuelUsed) throws FuelException {
        if (fuelLevel < fuelUsed) {
            throw new FuelException("No hi ha prou combustible per accelerar!");
        }
        fuelLevel -= fuelUsed;
        System.out.println(name + " ha accelerat, restant " + fuelUsed + " unitats de combustible.");
    }

    // Mètode per fer maniobres amb la nau
    public void performManeuver() throws DamageException {
        if (damageLevel > maxDamageLevel) {
            throw new DamageException(name + " està massa danyada per fer maniobres! Nau destruïda.");
        }
        System.out.println(name + " ha realitzat una maniobra.");
    }

    // Mètode per infligir danys a la nau
    public void takeDamage(int damage) {
        damageLevel += damage;
        System.out.println(name + " ha rebut " + damage + " unitats de dany. Danys totals: " + damageLevel);
    }

    // Mètode per repostar combustible
    public void refuel() {
        fuelLevel = maxFuelCapacity;
        System.out.println(name + " ha estat repostada al màxim de combustible.");
    }
}
```

### **Exemple**

Ara implementem un exemple pràctic on gestionem els errors de la nau durant un viatge espacial.

```java
public class Main {
    public static void main(String[] args) {
        // Creació d'una nau espacial
        Spaceship rocinante = new Spaceship("Rocinante", 100);

        try {
            // Accelerar la nau (correcte)
            rocinante.accelerate(30); // Ús de 30 unitats de combustible
            // Intentar fer una maniobra (correcte)
            rocinante.performManeuver();

            // Infligir danys a la nau
            rocinante.takeDamage(50);
            // Intentar fer una altra maniobra (correcte)
            rocinante.performManeuver();

            // Infligir més danys a la nau
            rocinante.takeDamage(60);
            // Intentar fer una maniobra amb danys crítics (llançarà una excepció)
            rocinante.performManeuver();

        } catch (FuelException e) {
            System.out.println("Error de combustible: " + e.getMessage());
        } catch (DamageException e) {
            System.out.println("Error de danys: " + e.getMessage());
        } finally {
            System.out.println("Finalització del viatge de la Rocinante.");
        }

        System.out.println(); // Separador per a la següent operació

        // Intentar accelerar sense prou combustible
        try {
            rocinante.accelerate(80); // Això llançarà una excepció de combustible
        } catch (FuelException e) {
            System.out.println("Error de combustible: " + e.getMessage());
            // Repostar la nau
            rocinante.refuel();
            System.out.println("La nau ha estat repostada.");
        } finally {
            System.out.println("Finalització de l'operació de combustible.");
        }
    }
}
```

### **Explicació del codi**:

1. **Excepcions personalitzades**: Hem creat dues excepcions personalitzades:
    - `FuelException` per a problemes de combustible.
    - `DamageException` per a problemes de danys crítics a la nau.

2. **Classe `Spaceship`**:
    - El mètode `accelerate()` llança una excepció de tipus `FuelException` si la nau no té prou combustible per
      accelerar.
    - El mètode `performManeuver()` llança una excepció de tipus `DamageException` si la nau té massa danys per
      maniobrar.
    - Hem afegit altres mètodes com `takeDamage()` per infligir danys i `refuel()` per repostar la nau.

3. **Maneig d'errors a la classe `Main`**:
    - Utilitzem blocs `try-catch-finally` per gestionar les excepcions. Si es produeix un error (com manca de
      combustible o danys greus), l'excepció és capturada i gestionada.
    - Si es detecta una manca de combustible, es pot repostar la nau utilitzant el mètode `refuel()` dins del bloc
      `catch`.

## **Exercici**:

### Context:
Ets el tècnic de comunicacions de la nau *Rocinante*, i has de gestionar la transmissió de dades amb una estació de Mart.
Per això, llegiràs i desxifraràs missatges codificats que arriben a través de fitxers de text. 

Els missatges codificats segueixen el format on els números representen la quantitat de repeticions del caràcter que segueix. Per exemple:
- Missatge codificat: `3A5C2B`
- Missatge desxifrat: `AAACCCCCBB`

### Objectiu:
Desenvolupa un petit programa interactiu que llegeixi un fitxer de text amb missatges codificats i desxifri aquests missatges. Durant el procés, s'ha de gestionar qualsevol excepció relacionada amb la lectura de fitxers i el format dels missatges.

#### Requisits tècnics:
- Pots utilitzar el mètode `decodeMessage` per decodificar missatges o crear un propi. Cap problema.
- El missatge ha de ser llegit d'un fitxer de text i desxifrat seguint el patró indicat.
- Gestiona les excepcions relacionades amb els fitxers, com ara `IOException` o altres ... que haureu d'esbrinar.

**Exemple d'implementació de `decodeMessage`**
```java
    /**
     * Desxifra un missatge codificat seguint la regla: els números indiquen quantes vegades
     * es repeteix el caràcter següent.
     *
     * @param message El missatge codificat a desxifrar.
     * @return El missatge desxifrat.
     */
    public static String decodeMessage(String message) {
        StringBuilder decoded = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            if (Character.isDigit(message.charAt(i))) {
                int count = Character.getNumericValue(message.charAt(i));
                char letter = message.charAt(i + 1);
                decoded.append(String.valueOf(letter).repeat(count));
                i++; // Saltar a la següent lletra després del número.
            }
        }
        return decoded.toString();
    }
```

#### Exemple de fitxer d'entrada (`input.txt`):
```
3A2B5C
2D1E3F
```

#### Sortida esperada:
```
AAABBCCCCC
DDFFF
```

#### Exemple de sortida del programa:

Si el fitxer `input.txt` conté el següent:
```
   3A2B5C
   2D1E3F
````

La sortida del programa seria:

```
=== Rocinante sniffer ===
1. Mostar fitxer
2. Desxifrar missatge
3. Sortir
Selecciona una opció: 1

Fitxer original
   3A2B5C
   2D1E3F

=== Rocinante sniffer ===
1. Mostar fitxer
2. Desxifrar missatge
3. Sortir
Selecciona una opció: 2

Missatges desxifrats:
   AAABBCCCCC
   DDEFFF

=== Rocinante sniffer ===
1. Desxifrar missatge amb java.io
2. Desxifrar missatge amb java.nio
3. Sortir
Selecciona una opció: 3
Sortint del programa...
```