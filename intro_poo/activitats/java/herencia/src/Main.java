import classes.BattleShip;
import classes.CargoShip;
import classes.ScoutShip;

// Classe principal per executar l'exercici
public class Main {
    public static void main(String[] args) {
        
        // Creació d'un cuirassat "Destructor de Mart"
        BattleShip martianDestroyer = new BattleShip("Destructor de Mart", 6000, 80, 10, 12);
        martianDestroyer.launch(); // Llançament de la nau
        martianDestroyer.accelerate(5); // Accelerar la nau
        martianDestroyer.upgradeWeapons(3); // Millorar l'armament
        martianDestroyer.statusReport(); // Mostrar l'informe d'estat del cuirassat

        System.out.println(); // Separador entre informes

        // Creació d'una nau de càrrega "Mineral Trader"
        CargoShip mineralTrader = new CargoShip("Mineral Trader", 5000, 20, 5, 3000);
        mineralTrader.launch(); // Llançament de la nau
        mineralTrader.accelerate(3); // Accelerar la nau
        mineralTrader.loadCargo(500); // Carregar recursos
        mineralTrader.statusReport(); // Mostrar l'informe d'estat de la nau de càrrega

        System.out.println(); // Separador entre informes

        // Creació d'una nau exploradora "Fast Scout"
        ScoutShip fastScout = new ScoutShip("Fast Scout", 4000, 10, 15, 1000);
        fastScout.launch(); // Llançament de la nau
        fastScout.accelerate(10); // Accelerar la nau
        fastScout.upgradeRadar(500); // Millorar l'abast del radar
        fastScout.statusReport(); // Mostrar l'informe d'estat de la nau exploradora
    }
}