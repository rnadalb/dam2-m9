package classes;

// Subclasse que representa una nau de càrrega (CargoShip)
public class CargoShip extends SpaceShip {

    // Atribut específic de la nau de càrrega
    private int cargoCapacity;

    // Constructor que crida al constructor de la superclasse
    public CargoShip(String name, int fuelCapacity, int crewSize, int currentSpeed, int cargoCapacity) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.cargoCapacity = cargoCapacity;
    }

    // Mètode per carregar recursos
    public void loadCargo(int amount) {
        cargoCapacity += amount;
        System.out.println(name + " ha carregat " + amount + " unitats de càrrega. Capacitat total: " + cargoCapacity);
    }

    // Sobreescriptura del mètode statusReport per afegir la capacitat de càrrega
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Capacitat de càrrega: " + cargoCapacity + " unitats");
    }
}