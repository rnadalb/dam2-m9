package classes;

public class SpaceShip {

    // Atributs comuns a totes les naus espacials
    protected String name;
    protected int fuelCapacity;
    protected int crewSize;
    protected int currentSpeed;

    // Constructor per inicialitzar la nau espacial
    public SpaceShip(String name, int fuelCapacity, int crewSize, int currentSpeed) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.crewSize = crewSize;
        this.currentSpeed = currentSpeed;
    }

    // Mètode per llançar la nau espacial
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }

    // Mètode per accelerar la nau
    public void accelerate(int amount) {
        currentSpeed += amount;
        System.out.println(name + " ha accelerat a una velocitat de " + currentSpeed + " km/s.");
    }

    // Mètode per mostrar l'estat de la nau
    public void statusReport() {
        System.out.println("Informe de l'estat de la nau " + name + ":");
        System.out.println("- Velocitat actual: " + currentSpeed + " km/s");
        System.out.println("- Capacitat de combustible: " + fuelCapacity + " unitats");
        System.out.println("- Mida de la tripulació: " + crewSize + " membres");
    }
}
