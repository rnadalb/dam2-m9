package classes;

// Subclasse que representa una nau exploradora (ScoutShip)
public class ScoutShip extends SpaceShip {

    // Atribut específic de la nau exploradora
    private int radarRange;

    // Constructor que crida al constructor de la superclasse
    public ScoutShip(String name, int fuelCapacity, int crewSize, int currentSpeed, int radarRange) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.radarRange = radarRange;
    }

    // Mètode per ampliar l'abast del radar
    public void upgradeRadar(int range) {
        radarRange += range;
        System.out.println(name + " ha ampliat l'abast del radar a " + radarRange + " km.");
    }

    // Sobreescriptura del mètode statusReport per afegir l'abast del radar
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Abast del radar: " + radarRange + " km");
    }
}