package classes;

// Subclasse que representa un cuirassat (Battleship)
public class BattleShip extends SpaceShip {

    // Atribut específic del cuirassat
    private int armamentLevel;

    // Constructor que crida al constructor de la superclasse
    public BattleShip(String name, int fuelCapacity, int crewSize, int currentSpeed, int armamentLevel) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.armamentLevel = armamentLevel;
    }

    // Mètode per millorar l'armament
    public void upgradeWeapons(int level) {
        armamentLevel += level;
        System.out.println(name + " ha millorat el seu nivell d'armament a " + armamentLevel + ".");
    }

    // Sobreescriptura del mètode statusReport per afegir el nivell d'armament
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Nivell d'armament: " + armamentLevel);
    }
}
