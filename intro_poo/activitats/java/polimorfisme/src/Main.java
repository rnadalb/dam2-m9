import classes.BattleShip;
import classes.CargoShip;
import classes.ScoutShip;
import classes.SpaceShip;

// Classe principal per executar el codi
public class Main {
    public static void main(String[] args) {

        // Creem objectes de diferents subclasses
        BattleShip battleship = new BattleShip("Destructor de Mart");
        CargoShip cargoShip = new CargoShip("Mineral Trader");
        ScoutShip scoutShip = new ScoutShip("Fast Scout");

        // Afegim les naus a una flota
        SpaceShip[] fleet = {battleship, cargoShip, scoutShip};

        // Llançar totes les naus
        for (SpaceShip ship : fleet) {
            ship.launch();
        }

        System.out.println(); // Separador

        // Executar les maniobres de cada nau
        for (SpaceShip ship : fleet) {
            ship.performManeuver();
        }

        System.out.println(); // Separador

        // Utilitzar la sobrecàrrega de mètodes per a cada nau
        battleship.performMaintenance("Làsers", 100);
        cargoShip.performMaintenance("Minerals", 500);
        scoutShip.performMaintenance(1000, "Asteroide desconegut");
    }
}