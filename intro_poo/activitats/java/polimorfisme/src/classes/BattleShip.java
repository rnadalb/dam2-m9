package classes;

// Subclasse que representa un cuirassat (Battleship)
public class BattleShip extends SpaceShip {

    // Constructor per inicialitzar el cuirassat
    public BattleShip(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performManeuver per la maniobra del cuirassat
    @Override
    public void performManeuver() {
        System.out.println(name + " està realitzant una maniobra de combat.");
    }

    // Sobrecàrrega de mètodes per fer manteniment al cuirassat
    public void performMaintenance() {
        System.out.println(name + " està fent manteniment rutinari del cuirassat.");
    }

    public void performMaintenance(String weaponType) {
        System.out.println(name + " està fent manteniment de l'arma: " + weaponType);
    }

    public void performMaintenance(String weaponType, int ammo) {
        System.out.println(name + " està fent manteniment complet de l'arma: " + weaponType + " i reposant " + ammo + " unitats de munició.");
    }
}