package classes;

// Classe base que representa una nau espacial genèrica
public class SpaceShip {
    protected String name;

    // Constructor per inicialitzar la nau
    public SpaceShip(String name) {
        this.name = name;
    }

    // Mètode per llançar la nau a l'espai
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }

    // Mètode genèric per realitzar maniobres, que serà sobrescrit per les subclasses
    public void performManeuver() {
        System.out.println(name + " està realitzant una maniobra genèrica.");
    }

    // Mètode genèric per realitzar manteniment, que serà sobrecarregat per cada subclasse
    public void performMaintenance() {
        System.out.println(name + " està fent manteniment rutinari.");
    }
}