package classes;

// Subclasse que representa una nau exploradora (ScoutShip)
public class ScoutShip extends SpaceShip {

    // Constructor per inicialitzar la nau exploradora
    public ScoutShip(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performManeuver per la maniobra d'exploració
    @Override
    public void performManeuver() {
        System.out.println(name + " està realitzant maniobres d'exploració per evitar obstacles.");
    }

    // Sobrecàrrega de mètodes per fer manteniment a la nau exploradora
    public void performMaintenance() {
        System.out.println(name + " està fent manteniment rutinari del sistema de radar.");
    }

    public void performMaintenance(int radarRange) {
        System.out.println(name + " està ajustant el sistema de radar per un abast de " + radarRange + " km.");
    }

    public void performMaintenance(int radarRange, String objectDetected) {
        System.out.println(name + " està ajustant el radar i analitzant l'objecte detectat: " + objectDetected + " en un abast de " + radarRange + " km.");
    }
}
