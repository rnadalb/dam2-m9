package classes;

// Subclasse que representa una nau de càrrega (CargoShip)
public class CargoShip extends SpaceShip {

    // Constructor per inicialitzar la nau de càrrega
    public CargoShip(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performManeuver per la maniobra de càrrega
    @Override
    public void performManeuver() {
        System.out.println(name + " està ajustant la trajectòria per carregar i descarregar mercaderies.");
    }

    // Sobrecàrrega de mètodes per fer manteniment a la nau de càrrega
    public void performMaintenance() {
        System.out.println(name + " està fent manteniment rutinari de la nau de càrrega.");
    }

    public void performMaintenance(String cargoType) {
        System.out.println(name + " està fent manteniment de la bodega de càrrega per al material: " + cargoType);
    }

    public void performMaintenance(String cargoType, int quantity) {
        System.out.println(name + " està fent manteniment de la bodega i gestionant " + quantity + " unitats de: " + cargoType);
    }
}