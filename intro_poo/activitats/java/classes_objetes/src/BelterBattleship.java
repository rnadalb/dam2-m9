// Classe que representa un cuirassat dels Belters
public class BelterBattleship {

    // Atributs del cuirassat
    private String name;            // Nom del cuirassat
    private int fuelCapacity;       // Capacitat de combustible
    private int crewSize;           // Mida de la tripulació
    private int currentSpeed;       // Velocitat actual de la nau
    private int armamentLevel;      // Nivell d'armament del cuirassat

    // Constructor per inicialitzar els atributs del cuirassat
    public BelterBattleship(String name, int fuelCapacity, int crewSize, int currentSpeed, int armamentLevel) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.crewSize = crewSize;
        this.currentSpeed = currentSpeed;
        this.armamentLevel = armamentLevel;
    }

    // Mètode per llançar el cuirassat
    public void launch() {
        System.out.println(name + " ha estat llançat a l'espai.");
    }

    // Mètode per accelerar el cuirassat
    public void accelerate(int amount) {
        currentSpeed += amount;
        System.out.println(name + " ha accelerat a una velocitat de " + currentSpeed + " km/s.");
    }

    // Mètode per millorar el nivell d'armament
    public void upgradeWeapons(int level) {
        armamentLevel += level;
        System.out.println(name + " ha millorat el seu nivell d'armament a " + armamentLevel + ".");
    }

    // Mètode per mostrar l'estat actual del cuirassat
    public void statusReport() {
        System.out.println("Informe de l'estat del cuirassat " + name + ":");
        System.out.println("- Velocitat actual: " + currentSpeed + " km/s");
        System.out.println("- Capacitat de combustible: " + fuelCapacity + " unitats");
        System.out.println("- Mida de la tripulació: " + crewSize + " membres");
        System.out.println("- Nivell d'armament: " + armamentLevel);
    }
}