// Classe principal per executar l'exercici
public class Main {
    // Mètode principal per executar el codi
    public static void main(String[] args) {
        // Creem un objecte que representa el cuirassat "Destructor de Cinturons"
        BelterBattleship battleship = new BelterBattleship("Destructor de Cinturons", 8000, 50, 8, 10);

        // Llançar el cuirassat
        battleship.launch(); // "Destructor de Cinturons ha estat llançat a l'espai."

        // Accelerar el cuirassat
        battleship.accelerate(5); // "Destructor de Cinturons ha accelerat a una velocitat de 13 km/s."

        // Millorar el nivell d'armament
        battleship.upgradeWeapons(5); // "Destructor de Cinturons ha millorat el seu nivell d'armament a 15."

        // Mostrar l'informe de l'estat actual del cuirassat
        battleship.statusReport();
        /*
        Informe de l'estat del cuirassat Destructor de Cinturons:
        - Velocitat actual: 13 km/s
        - Capacitat de combustible: 8000 unitats
        - Mida de la tripulació: 50 membres
        - Nivell d'armament: 15
        */
    }
}