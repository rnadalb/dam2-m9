### **Encapsulació: protegint la nau**

L'**encapsulació** és un dels quatre pilars fonamentals de la programació orientada a objectes, juntament amb l'herència, el polimorfisme i la composició. Es basa en la idea de protegir l'estat intern dels objectes i controlar com s'accedeix a les seves dades i comportaments. D'aquesta manera, es pot garantir que les dades siguin utilitzades de manera controlada i segura, evitant manipulacions innecessàries o errònies des de fora de la classe.

### **Què és l'encapsulació?**

L'**encapsulació** es refereix a l'agrupació de dades (atributs) i mètodes (comportament) dins d'una classe, i a la restricció de l'accés als components interns d'aquesta classe per mitjà de la visibilitat (modificadors d'accés com `private`, `protected`, i `public`).

- **Privatització de dades**: els atributs d'una classe són sovint declarats com a **privats** (`private`), la qual cosa impedeix que puguin ser modificats o accedits directament des de fora de la classe.
- **Mètodes d'accés**: per permetre un accés controlat a aquests atributs privats, es defineixen **mètodes públics** (`getters` i `setters`), que permeten consultar o modificar l'estat de la classe de manera segura.

### **Avantatges**:

1. **Protecció de les dades**: evita que els atributs siguin manipulats directament, prevenint errors o comportaments inesperats.
2. **Control d'accés**: garanteix que només es pugui accedir o modificar l'estat intern a través de mètodes específics.
3. **Flexibilitat**: permet canviar la implementació interna de la classe sense afectar les altres parts del codi que l’utilitzen, sempre que els mètodes públics es mantinguin.
4. **Fàcil manteniment**: el codi és més fàcil de mantenir, ja que els detalls d'implementació estan ocults i l'accés està limitat a través d'interfícies controlades.

### **Exemple**

Anem a encapsular els components interns de la classe `Spaceship`, protegint els atributs i definint mètodes d'accés controlats (`getters` i `setters`).

#### **Classe `Spaceship**


```java
public class SpaceShip {
    // Atributs
    private String name;
    private int fuelCapacity;
    private int crewSize;
    private int speed;

    // Constructor
    public SpaceShip(String name, int fuelCapacity, int crewSize, int speed) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.crewSize = crewSize;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(int fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public int getCrewSize() {
        return crewSize;
    }

    public void setCrewSize(int crewSize) {
        this.crewSize = crewSize;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    // Mètodes
    public void launch() {
        System.out.println(name + " ha iniciat el seu viatge a través de l'espai.");
    }

    public void accelerate(int amount) {
        speed += amount;
        System.out.println(name + " ha accelerat a una velocitat de " + speed + " km/s.");
    }
}
```

### **Explicació del codi**:

1. **Atributs Privats**:
    - **`name`**, **`fuelCapacity`**, **`crewSize`**, i **`speed`** són atributs **privats**. Això vol dir que no es pot accedir directament a aquests atributs des de fora de la classe `Spaceship`.
    - El fet que aquests atributs siguin privats ajuda a protegir l'estat intern de la nau, garantint que els valors només es poden modificar mitjançant mètodes controlats.

2. **Getters i setters**:
    - Els mètodes **públics** `getName()`, `getFuelCapacity()`, `getCrewSize()`, i `getSpeed()` són exemples de **getters**, que permeten accedir als valors privats de manera segura.
    - Els mètodes **públics** `setName()`, `setFuelCapacity()`, `setCrewSize()`, i `setSpeed()` són exemples de **setters**, que controlen com es poden modificar aquests valors privats.

3. **Control de l'estat**:
    - El mètode `launch()` permet llançar una nau a l'espai.
    - El mètode `accelerate()` accelera la nau, incrementant el valor amb el paràmetre que rep.

### **Exemple d'ús**

```java
public class Main {
    public static void main(String[] args) {
        SpaceShip spaceShip = new SpaceShip("Rocinante", 10, 10, 10);
        spaceShip.launch();
        spaceShip.accelerate(10);
        System.out.printf("La velocitat actual de la nau és: %d\n", spaceShip.getSpeed());
        spaceShip.setName("Donnager");
        spaceShip.launch();

    }
}
```

Fixa't bé en la sortida

```text
Rocinante ha iniciat el seu viatge a través de l'espai.
Rocinante ha accelerat a una velocitat de 20 km/s.
La velocitat actual de la nau és: 20
Donnager ha iniciat el seu viatge a través de l'espai.
```
