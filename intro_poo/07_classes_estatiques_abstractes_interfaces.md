### **Classes estàtiques, classes abstractes i Interfícies**

En aquest apartat, explorarem tres conceptes avançats de la programació orientada a objectes que tenen un paper fonamental en la construcció d'aplicacions robustes i escalables: **classes estàtiques**, **classes abstractes**, i **interfícies**.

---

### **1. Classes estàtiques**

Les **classes estàtiques** són classes que es poden utilitzar sense haver de crear una instància. Normalment, s'utilitzen per a agrupar funcionalitats que no depenen de l'estat d'un objecte específic, sinó que són més generals. Les classes estàtiques es comporten de manera similar als mètodes estàtics, però permeten encapsular més funcionalitat dins d’una sola classe.

#### **Quan Utilitzar classes estàtiques?**

Les classes estàtiques són útils quan necessitem funcions utilitàries o constants globals que no depenen de cap objecte en particular. Un bon exemple en el context d'una nau espacial és una classe estàtica que s'encarrega de càlculs astronòmics, com ara la velocitat relativa entre dos objectes, o una classe que emmagatzema informació constant, com les velocitats de la llum o altres unitats espacials.

#### **Exemple: classe d'utilitats `SpaceUtils`**

```java
// Classe estàtica per càlculs relacionats amb el viatge espacial
public class SpaceUtils {

    // Constants espacials
    public static final double LIGHT_SPEED = 299792.458;  // Velocitat de la llum en km/s

    // Mètode estàtic per calcular la velocitat relativa entre dues naus espacials
    public static double calculateRelativeSpeed(double speedShip1, double speedShip2) {
        return Math.abs(speedShip1 - speedShip2);
    }

    // Mètode estàtic per convertir distància en anys llum a quilòmetres
    public static double lightYearsToKm(double lightYears) {
        return lightYears * LIGHT_SPEED * 60 * 60 * 24 * 365;  // Conversió a km
    }

    // Mètode estàtic per verificar si una nau ha assolit la velocitat de la llum
    public static boolean hasReachedLightSpeed(double speed) {
        return speed >= LIGHT_SPEED;
    }
}
```

#### **Exemple**

```java
public class Main {
    public static void main(String[] args) {
        // Càlcul de la velocitat relativa entre dues naus
        double speedShip1 = 50000;  // Velocitat en km/s
        double speedShip2 = 75000;  // Velocitat en km/s
        double relativeSpeed = SpaceUtils.calculateRelativeSpeed(speedShip1, speedShip2);
        System.out.println("La velocitat relativa entre les dues naus és de: " + relativeSpeed + " km/s");

        // Conversió d'anys llum a quilòmetres
        double distanceInLightYears = 4.2;  // Distància a Alpha Centauri
        double distanceInKm = SpaceUtils.lightYearsToKm(distanceInLightYears);
        System.out.println("La distància a Alpha Centauri és de: " + distanceInKm + " quilòmetres.");

        // Verificar si una nau ha assolit la velocitat de la llum
        if (SpaceUtils.hasReachedLightSpeed(speedShip1)) {
            System.out.println("La nau ha assolit la velocitat de la llum!");
        } else {
            System.out.println("La nau encara no ha assolit la velocitat de la llum.");
        }
    }
}
```

### **2. Classes abstractes**

Les **classes abstractes** són classes que no es poden instanciar directament i serveixen com a plantilles per a altres classes. Una classe abstracta pot tenir mètodes complets (amb implementació) i mètodes abstractes (sense implementació), que han de ser implementats per les subclasses.

#### **Quan utilitzar classes abstractes?**

Les classes abstractes són útils quan volem definir un comportament genèric que serà compartit per diferents subclasses, però alhora volem forçar que les subclasses implementin alguns comportaments específics. Per exemple, podem tenir una classe abstracta `Spaceship` que defineix comportaments generals de les naus espacials, mentre que subclasses com `Battleship` o `CargoShip` implementaran comportaments concrets.

#### **Exemple: SpaceShip**

```java
// Classe abstracta que defineix el comportament bàsic d'una nau espacial
public abstract class SpaceShip {
    protected String name;
    protected int fuelLevel;

    // Constructor
    public SpaceShip(String name, int fuelLevel) {
        this.name = name;
        this.fuelLevel = fuelLevel;
    }

    // Mètode concret per llançar la nau
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }

    // Mètode abstracte que ha de ser implementat per les subclasses
    public abstract void performMission();

    // Mètode abstracte que les subclasses han d'implementar per consumir combustible
    public abstract void consumeFuel(int amount);
}
```

#### **Subclasses (herència)**

* **Classe `BattleShip`**
```java
// Subclasse de SpaceShip: Cuirassat
public class BattleShip extends SpaceShip {
    private int armamentLevel;

    public BattleShip(String name, int fuelLevel, int armamentLevel) {
        super(name, fuelLevel);
        this.armamentLevel = armamentLevel;
    }

    @Override
    public void performMission() {
        System.out.println(name + " està en una missió de combat.");
    }

    @Override
    public void consumeFuel(int amount) {
        fuelLevel -= amount;
        System.out.println(name + " ha consumit " + amount + " unitats de combustible. Combustible restant: " + fuelLevel);
    }
}
```
* **Classe `CargoShip`**
```java
// Subclasse de SpaceShip: Nau de Càrrega
public class CargoShip extends Spaceship {
    private int cargoCapacity;

    public CargoShip(String name, int fuelLevel, int cargoCapacity) {
        super(name, fuelLevel);
        this.cargoCapacity = cargoCapacity;
    }

    @Override
    public void performMission() {
        System.out.println(name + " està transportant mercaderies.");
    }

    @Override
    public void consumeFuel(int amount) {
        fuelLevel -= amount;
        System.out.println(name + " ha consumit " + amount + " unitats de combustible. Combustible restant: " + fuelLevel);
    }
}
```

#### **Exemple**

```java
public class Main {
    public static void main(String[] args) {
        // Crear un cuirassat i una nau de càrrega
        SpaceShip battleship = new Battleship("Destructor de Mart", 1000, 50);
        SpaceShip cargoShip = new CargoShip("Mineral Trader", 800, 3000);

        // Llançar les naus
        battleship.launch();
        cargoShip.launch();

        // Executar missions
        battleship.performMission();
        cargoShip.performMission();

        // Consumir combustible
        battleship.consumeFuel(200);
        cargoShip.consumeFuel(150);
    }
}
```

### **3. Interfícies**

Les **interfícies** són semblants a les classes abstractes, però tenen una diferència clau: no contenen cap implementació. Les interfícies només defineixen signatures de mètodes que les classes han d'implementar, i una classe pot implementar múltiples interfícies. Les interfícies permeten definir comportaments compartits entre classes no relacionades.

#### **Quan utilitzar interfícies?**

Les interfícies són útils quan volem definir un conjunt de comportaments (modelar) que poden ser compartits entre diferents classes, sense necessitar una jerarquia d'herència. Per exemple, podem definir una interfície `Refuelable` per indicar que una nau pot ser repostada, o una interfície `Repairable` per indicar que una nau es pot reparar.

#### **Exemple: `Refuelable` i `Repairable`**

```java
// Interfície que defineix la capacitat de repostar combustible
public interface Refuelable {
    void refuel();
}

// Interfície que defineix la capacitat de reparar-se
public interface Repairable {
    void repair();
}
```

#### **Classes que implementen les interfícies**

```java
// Nau que implementa la interfície Refuelable
public class SpaceShip implements Refuelable, Repairable {
    private String name;
    private int fuelLevel;
    private int damageLevel;

    public SpaceShip(String name, int fuelLevel) {
        this.name = name;
        this.fuelLevel = fuelLevel;
        this.damageLevel = 0;
    }

    // Implementació del mètode refuel() de la interfície Refuelable
    @Override
    public void refuel() {
        fuelLevel = 100;
        System.out.println(name + " ha estat repostada. Combustible al màxim.");
    }

    // Implementació del mètode repair() de la interfície Repairable
    @Override
    public void repair() {
        damageLevel = 0;
        System.out.println(name + " ha estat reparada completament.");
    }

    // Mètode per mostrar l'estat de la nau
    public void statusReport() {
        System.out.println(name + " té " + fuelLevel + "% de combustible i " + damageLevel + "% de danys.");
    }
}
```

**Recorda que per no tenir errors de compilació has de sobreescriure els mètodes definies a la interfície obligatòriament.**

#### **Exemple**

```java
public class Main {
    public static void main(String[] args) {
        // Crear una nau que implementa les interfícies
        SpaceShip rocinante = new SpaceShip("Rocinante", 50);

        // Mostrar l'estat inicial de la nau
        rocinante.statusReport();

        // Repostar la nau
        rocinante.refuel();

        // Reparar la nau
        rocinante.repair();

        // Mostrar l'estat final de la nau
        rocinante.statusReport();
    }
}
```

### **Comparació entre classes estàtiques, abstractes i interfícies**

| Característica        | Classe estàtica                        | Classe abstracta                                | Interfície                                                        |
|-----------------------|----------------------------------------|-------------------------------------------------|-------------------------------------------------------------------|
| **Instanciació**      | No es poden instanciar                 | No es poden instanciar                          | No es poden instanciar                                            |
| **Mètodes**           | Pot tenir mètodes estàtics             | Pot tenir mètodes complets i abstractes         | Només pot tenir mètodes abstractes                                |
| **Atributs**          | Pot tenir atributs estàtics            | Pot tenir atributs i mètodes concrets           | No pot tenir atributs (només constants finals)                    |
| **Ús**                |  Funcions generals o constants globals | Estructura comuna per a subclasses relacionades | Definir comportaments que poden compartir classes no relacionades |
| **Herència**          | No participa en herència               | Pot tenir subclasses                            | Una classe pot implementar múltiples interfícies                  |

 
### **Exercici**

### Context:
La nau *Rocinante* està en plena missió de comunicació amb una estació de Mart. Aquesta comunicació es realitza mitjançant fitxers de text que contenen missatges codificats. El teu objectiu és dissenyar un sistema de transmissió de missatges utilitzant fitxers i aplicar els conceptes de **Classe Estàtica**, **Classe Abstracta** i **Interfície** per simular diferents canals de transmissió.

### Objectiu:
Crear un sistema que llegeixi missatges codificats d'un fitxer, els desxifri i en transmeti la resposta a un altre fitxer. El sistema ha d'utilitzar **interfícies** per definir els comportaments de transmissió, **classes abstractes** per establir una base comú per als canals de comunicació, i **classes estàtiques** per a la gestió de les operacions sobre els fitxers i missatges.

### Requisits tècnics:

1. **Interfície `Transmissible`**:
    - Defineix els mètodes abstractes següents:
        - `void establirConnexio()`: Per establir una connexió amb l'estació.
        - `void transmetreMissatge(String missatge, String fitxerSortida)`: Transmet un missatge al fitxer de sortida.
        - `boolean validarMissatge(String missatge)`: Valida si el missatge compleix els requisits de seguretat.

2. **Classe abstracta `CanalDeTransmissio`**:
    - Implementa la interfície `Transmissible`.
    - Defineix un atribut `canalActiu` que indiqui si el canal està actiu o no.
    - Implementa el mètode `establirConnexio()` per activar el canal.
    - Deixa els mètodes `transmetreMissatge` i `validarMissatge` com a abstractes perquè les subclasses els implementin.

3. **Classes concretes per a canals específics**:
    - `CanalEncriptat`: Implementa la transmissió de missatges encriptats (utilitzant la inversió de l'ordre dels caràcters) i escriu el missatge desxifrat a un fitxer de sortida.
    - `CanalNoEncriptat`: Implementa la transmissió de missatges sense encriptació i escriu el missatge directament al fitxer de sortida.

4. **Classe estàtica `FileUtils`**:
    - Defineix una classe estàtica per gestionar operacions amb fitxers:
        - `static List<String> llegirMissatge(String fitxerEntrada)`: Llegeix un missatge d'un fitxer.
        - `static void escriureMissatge(String missatge, String fitxerSortida)`: Escriu un missatge a un fitxer.
        - `static String invertirMissatge(String missatge)`: Inverteix un missatge.
        - `static boolean esValid(String missatge)`: Valida si el missatge conté només lletres.

#### Diagrama de classes
```mermaid
classDiagram
class Transmissible {
<<interface>>
+establirConnexio() void
+transmetreMissatge(String missatge, String fitxerSortida) void
+validarMissatge(String missatge) boolean
}

    class CanalDeTransmissio {
        <<abstract>>
        -canalActiu: boolean
        +establirConnexio() void
        +transmetreMissatge(String missatge, String fitxerSortida) void
        +validarMissatge(String missatge) boolean
    }

    Transmissible <|-- CanalDeTransmissio

    class CanalEncriptat {
        +transmetreMissatge(String missatge, String fitxerSortida) void
        +validarMissatge(String missatge) boolean
    }

    class CanalNoEncriptat {
        +transmetreMissatge(String missatge, String fitxerSortida) void
        +validarMissatge(String missatge) boolean
    }

    CanalDeTransmissio <|-- CanalEncriptat
    CanalDeTransmissio <|-- CanalNoEncriptat

    class FileUtils {
        <<static>>
        +llegirMissatge(String fitxerEntrada) List~String~
        +escriureMissatge(String missatge, String fitxerSortida) void
        +invertirMissatge(String missatge) String
        +esValid(String missatge) boolean
    }

    class Main {
        +main(String[] args) void
        +mostrarMenu() void
        +mostrarFitxer(List~String~ missatges) void
        +transmetreMissatgeEncriptat(List~String~ missatges, String fitxerSortida) void
        +transmetreMissatgeNoEncriptat(List~String~ missatges, String fitxerSortida) void
    }

    Main --|> FileUtils
```


5. **Simulació de la comunicació**:
    - El sistema ha de llegir un missatge codificat des d'un fitxer d'entrada, desxifrar-lo i transmetre'l (escriure'l) a un fitxer de sortida mitjançant un dels canals de transmissió. Primer utilitza el canal encriptat i després el no encriptat.

### Codi d'exemple:

#### Classe estàtica `FileUtils`

```java
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Classe estàtica que proporciona funcions utilitàries per gestionar fitxers i missatges.
 */
public class UtilitatsDeFitxers {

    /**
     * Llegeix missatges d'un fitxer.
     * 
     * @param fitxerEntrada Ruta del fitxer d'entrada.
     * @return Una llista de línies del fitxer.
     */
    public static List<String> llegirMissatge(String fitxerEntrada) {
        try {
            return Files.readAllLines(Paths.get(fitxerEntrada));
        } catch (IOException e) {
            System.out.println("Error al llegir el fitxer: " + e.getMessage());
            return null;
        }
    }

    /**
     * Escriu un missatge a un fitxer.
     * 
     * @param missatge El missatge a escriure.
     * @param fitxerSortida Ruta del fitxer de sortida.
     */
    public static void escriureMissatge(String missatge, String fitxerSortida) {
        try {
            Files.writeString(Paths.get(fitxerSortida), missatge + "\n", StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("Error al escriure el fitxer: " + e.getMessage());
        }
    }

    /**
     * Inverteix l'ordre dels caràcters d'un missatge.
     * 
     * @param missatge El missatge a invertir.
     * @return El missatge invertit.
     */
    public static String invertirMissatge(String missatge) {
        return new StringBuilder(missatge).reverse().toString();
    }

    /**
     * Valida si un missatge conté només caràcters alfabètics.
     * 
     * @param missatge El missatge a validar.
     * @return True si el missatge és vàlid, False en cas contrari.
     */
    public static boolean esValid(String missatge) {
        return missatge.chars().allMatch(Character::isLetter);
    }
}
```

#### Exemple de sortida del programa

```
=== Rocinante Comms ===
1. Mostrar missatges del fitxer
2. Transmetre missatge encriptat
3. Transmetre missatge no encriptat
4. Sortir
Selecciona una opció: 1

Missatges al fitxer d'entrada:
   SalutacionsMart
   SomLaRocinante
   MissioCompleta

=== Rocinante Comms ===
1. Mostrar missatges del fitxer
2. Transmetre missatge encriptat
3. Transmetre missatge no encriptat
4. Sortir
Selecciona una opció: 2
Connexió establerta.
Transmetent missatge encriptat: traMsnoicatulaS
Transmetent missatge encriptat: etnanicoRaLmoS
Transmetent missatge encriptat: atelpmoCoissiM

=== Rocinante Comms ===
1. Mostrar missatges del fitxer
2. Transmetre missatge encriptat
3. Transmetre missatge no encriptat
4. Sortir
Selecciona una opció: 3
Connexió establerta.
Transmetent missatge no encriptat: SalutacionsMart
Transmetent missatge no encriptat: SomLaRocinante
Transmetent missatge no encriptat: MissioCompleta

=== Rocinante Comms ===
1. Mostrar missatges del fitxer
2. Transmetre missatge encriptat
3. Transmetre missatge no encriptat
4. Sortir
Selecciona una opció: 4
Sortint del programa...
```