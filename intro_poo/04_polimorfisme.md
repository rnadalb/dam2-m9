# **3. Polimorfisme: capacitats diverses per les naus espacials**

Després d’haver establert les bases de l'**[herència](03_herencia)**, ara ens centrem en el següent pilar fonamental de la programació orientada a objectes: el **polimorfisme**. Aquest concepte ens permet utilitzar una única interfície per a diferents tipus d'objectes, cosa que millora la flexibilitat i escalabilitat del nostre codi.

Cada tipus de nau espacial pot tenir comportaments diferents, però totes comparteixen certes accions comunes, com llançar-se a l'espai o realitzar operacions de manteniment. El polimorfisme ens permet definir aquestes accions de manera genèrica per a totes les naus espacials, mentre cada tipus de nau implementa la seva pròpia versió d’aquestes accions.

## **Què és el polimorfisme?**

El **polimorfisme** ens permet tractar objectes de diferents classes que tenen una relació d'herència amb la mateixa superclasse o implementen la mateixa interfície, fent servir el mateix mètode. Això significa que podem cridar un mètode en un objecte sense importar de quina subclasse sigui aquest objecte, ja que cada subclasse pot proporcionar la seva pròpia implementació d’aquest mètode.

El polimorfisme pot ocórrer de dues maneres:
1. **Polimorfisme en temps d'execució**: sobreescriptura de mètodes, que és el que ens interessa per a aquest apartat.
2. **Polimorfisme en temps de compilació**: sobrecàrrega de mètodes.

### **Polimorfisme en temps d'execució: sobreescriptura de mètodes**

Per exemplificar el polimorfisme, farem que diferents tipus de naus espacials sobrescriguin un mètode comú, per exemple, un mètode anomenat `performMission()`, que representarà la missió que ha de complir cada nau. Cada tipus de nau tindrà una missió diferent, però totes implementaran la interfície comuna de `performMission()`.  Observa el diagrama de classes.

```mermaid
classDiagram
    class Spaceship {
        -String name
        +launch()
        +performMission()
        +getName() String
    }

    class Battleship {
        +performMission()
        +loadWeapons(int amount)
        +loadWeapons(String weaponType)
        +loadWeapons(String weaponType, int amount)
    }

    class CargoShip {
        +performMission()
        +loadCargo(int amount)
        +loadCargo(String cargoType)
        +loadCargo(String cargoType, int amount)
    }

    class ScoutShip {
        +performMission()
        +scanArea(int range)
        +scanArea(String objectOfInterest)
        +scanArea(int range, String objectOfInterest)
    }

    Spaceship <|-- Battleship
    Spaceship <|-- CargoShip
    Spaceship <|-- ScoutShip
```

### **Classe base: SpaceShip**

```java
// Classe base que representa una nau espacial genèrica
public class SpaceShip {
    
    protected String name;

    public SpaceShip(String name) {
        this.name = name;
    }

    // Mètode genèric que serà sobrescrit per les subclasses
    public void performMission() {
        System.out.println(name + " està complint una missió genèrica.");
    }

    // Mètode per llançar la nau
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }
}
```

### **Subclasses: sobreescriptura del mètode `performMission()`**

Cada subclasse sobrescriurà el mètode `performMission()` per implementar la seva pròpia missió.

#### **Cuirassat (Battleship)**

```java
// Classe que representa un cuirassat, estenent la classe SpaceShip
public class Battleship extends SpaceShip {

    public Battleship(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performMission per la missió del cuirassat
    @Override
    public void performMission() {
        System.out.println(name + " està en una missió de combat contra una flota enemiga.");
    }
}
```

#### **Nau de càrrega (CargoShip)**

```java
// Classe que representa una nau de càrrega, estenent la classe SpaceShip
public class CargoShip extends SpaceShip {

    public CargoShip(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performMission per la missió de càrrega
    @Override
    public void performMission() {
        System.out.println(name + " està transportant recursos crítics a una estació espacial.");
    }
}
```

#### **Nau exploradora (ScoutShip)**

```java
// Classe que representa una nau exploradora, estenent la classe SpaceShip
public class ScoutShip extends SpaceShip {

    public ScoutShip(String name) {
        super(name);
    }

    // Sobreescriptura del mètode performMission per la missió d'exploració
    @Override
    public void performMission() {
        System.out.println(name + " està explorant un sector desconegut per trobar noves rutes espacials.");
    }
}
```

### **Exemple**

Ara veurem com podem utilitzar el polimorfisme per gestionar diferents tipus de naus a través de la seva superclasse `SpaceShip`. Crearem un array de naus espacials i farem que totes compleixin la seva missió sense haver d’especificar la seva tipologia concreta.

```java
public class Main {
    public static void main(String[] args) {

        // Creem objectes de diferents subclasses
        SpaceShip battleship = new Battleship("Destructor de Mart");
        SpaceShip cargoShip = new CargoShip("Mineral Trader");
        SpaceShip scoutShip = new ScoutShip("Fast Scout");

        // Llançar totes les naus
        battleship.launch();
        cargoShip.launch();
        scoutShip.launch();

        System.out.println(); // Separador entre llançaments i missions

        // Polimorfisme: totes les naus compleixen la seva missió
        SpaceShip[] fleet = {battleship, cargoShip, scoutShip};

        for (SpaceShip ship : fleet) {
            ship.performMission(); // Cada nau implementa la seva pròpia versió de performMission()
        }
    }
}
```

#### **Explicació del codi:**

1. **Classe `SpaceShip`**: És la superclasse que defineix el mètode `performMission()`, que serà sobrescrit per cada subclasse. Això ens permet cridar `performMission()` en objectes de qualsevol tipus de nau sense saber-ne la tipologia específica.

2. **Subclasses `Battleship`, `CargoShip`, `ScoutShip`**: Cada subclasse sobrescriu el mètode `performMission()` per implementar una missió específica: combat, transport de recursos, o exploració.

3. **Polimorfisme en acció**: En el mètode `main()`, es crea una flota de diferents tipus de naus espacials i es guarda en un array de tipus `SpaceShip`. Després, es recorre l'array i es crida el mètode `performMission()` per a cada nau. Aquí, el polimorfisme permet que cada nau realitzi la seva missió específica, tot i ser tractades com a objectes de la superclasse `SpaceShip`.


### **Polimorfisme en temps de compilació: sobrecàrrega de mètodes**

La **sobrecàrrega de mètodes** ens permet definir múltiples mètodes amb el mateix nom dins d'una mateixa classe, sempre que tinguin una signatura diferent. És a dir, els mètodes poden compartir el nom, però han de diferir en:
- El nombre de paràmetres.
- El tipus de paràmetres.
- L'ordre dels paràmetres.

Aquest tipus de polimorfisme és resolt durant la **compilació**, ja que el compilador sap quin mètode cridar en funció dels arguments proporcionats.

#### **Exemple: mètode `loadWeapons()` en Cuirassat**

En aquest exemple, veurem com un cuirassat pot sobrecarregar el mètode `loadWeapons()` per acceptar diferents paràmetres depenent de les necessitats de càrrega de les armes.

##### **Classe Cuirassat amb sobrecàrrega de mètodes**

```java
// Classe que representa un cuirassat, estenent la classe SpaceShip
public class Battleship extends SpaceShip {

    private int armamentLevel;

    // Constructor per inicialitzar el cuirassat
    public Battleship(String name, int fuelCapacity, int crewSize, int currentSpeed, int armamentLevel) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.armamentLevel = armamentLevel;
    }

    // Sobreescriptura del mètode performMission per la missió del cuirassat
    @Override
    public void performMission() {
        System.out.println(name + " està en una missió de combat contra una flota enemiga.");
    }

    // Sobrecàrrega 1: Carregar una quantitat de munició determinada
    public void loadWeapons(int amount) {
        System.out.println(name + " ha carregat " + amount + " unitats de munició.");
    }

    // Sobrecàrrega 2: Carregar un tipus específic d'armament
    public void loadWeapons(String weaponType) {
        System.out.println(name + " ha carregat armament del tipus: " + weaponType + ".");
    }

    // Sobrecàrrega 3: Carregar un tipus específic d'armament amb una quantitat específica
    public void loadWeapons(String weaponType, int amount) {
        System.out.println(name + " ha carregat " + amount + " unitats d'armament del tipus: " + weaponType + ".");
    }
}
```

### **Ús de la sobrecàrrega de mètodes**

Podem utilitzar qualsevol de les versions del mètode `loadWeapons()` depenent de la situació. El compilador sap quina versió ha de cridar basant-se en els paràmetres que se li passen.

#### **Exemple**

```java
public class Main {
    public static void main(String[] args) {

        // Creem un objecte de la classe Battleship
        Battleship martianDestroyer = new Battleship("Destructor de Mart", 6000, 80, 10, 12);

        // Cridem les diferents versions de loadWeapons
        martianDestroyer.loadWeapons(50); // Carrega una quantitat de munició
        martianDestroyer.loadWeapons("Làsers"); // Carrega un tipus específic d'armament
        martianDestroyer.loadWeapons("Torpedes", 20); // Carrega un tipus específic d'armament amb una quantitat concreta
    }
}
```

### **Explicació del codi:**

1. **Sobrecàrrega de mètodes**:
    - La classe `Battleship` defineix tres versions del mètode `loadWeapons()` amb diferents signatures:
        - Una versió accepta una quantitat de munició (`int amount`).
        - Una altra versió accepta un tipus d'armament (`String weaponType`).
        - La tercera versió accepta tant un tipus d'armament com una quantitat (`String weaponType, int amount`).

2. **Ús de la sobrecàrrega**: En el mètode `main()`, podem cridar qualsevol de les versions de `loadWeapons()` segons els arguments que passem. Això és possible gràcies a la sobrecàrrega de mètodes, que permet reutilitzar el mateix nom de mètode per a funcionalitats diferents basades en els paràmetres.

### **Exercici**

#### **Objectius**

1. Crear una jerarquia de classes basada en la ja generada anteriorment, però amb nous mètodes a sobrescriure i sobrecarregar.
2. Implementar la **sobreescriptura de mètodes** per personalitzar el comportament de cada nau espacial en termes de maniobres i manteniment.
3. Utilitzar la **sobrecàrrega de mètodes** per afegir flexibilitat en com cada nau realitza manteniment i maniobres.
4. Aplicar els dos tipus de polimorfisme (sobreescriptura i sobrecàrrega) dins d’un mateix programa per fer-lo més variat.

### **Descripció:**

#### **1. Crea la classe base `SpaceShip`**

Utilitza la mateixa classe `SpaceShip` com a base, però afegeix els mètodes següents:

- **`performManeuver()`**: Aquest mètode serà sobrescrit per indicar com cada nau espacial realitza maniobres.
- **`performMaintenance()`**: Aquest mètode serà sobrecarregat per donar suport a diferents tipus de manteniment que les naus espacials puguin necessitar.

#### **2. Crear subclasses que sobreescriguin `performManeuver()`**

Crea tres subclasses de `SpaceShip` i sobrescriu el mètode `performManeuver()` per personalitzar les maniobres que realitza cada nau espacial:

- **`Battleship`**: El cuirassat realitzarà maniobres de combat, com per exemple esquivar atacs o col·locar-se en formació de batalla.
- **`CargoShip`**: La nau de càrrega realitzarà maniobres de transport, com ajustar la trajectòria per carregar o descarregar mercaderies.
- **`ScoutShip`**: La nau exploradora realitzarà maniobres d'exploració, com canvis de direcció ràpids per evitar obstacles o per obtenir una millor vista d’un objecte espacial.

#### **3. Implementar la sobrecàrrega de mètodes `performMaintenance()`**

Afegeix mètodes sobrecarregats per realitzar diferents tipus de manteniment en cada nau espacial:

- **`Battleship`**: Sobrecàrrega del mètode `performMaintenance()` per:
    1. Realitzar un manteniment rutinari sense paràmetres.
    2. Realitzar un manteniment d'armes especificant el tipus d'arma.
    3. Realitzar un manteniment complet especificant el tipus d'arma i la quantitat de munició que s'ha de reposar.

- **`CargoShip`**: Sobrecàrrega del mètode `performMaintenance()` per:
    1. Realitzar un manteniment rutinari sense paràmetres.
    2. Realitzar manteniment a la bodega de càrrega especificant el tipus de material transportat.
    3. Realitzar manteniment especificant el tipus de material i la quantitat.

- **`ScoutShip`**: Sobrecàrrega del mètode `performMaintenance()` per:
    1. Realitzar un manteniment rutinari sense paràmetres.
    2. Realitzar manteniment al sistema de radar especificant la distància de detecció.
    3. Realitzar un manteniment complet del radar especificant la distància de detecció i el tipus d'objecte detectat.

#### **4. Crea una flota i practica**

Crea una flota de naus espacials amb les tres subclasses (`Battleship`, `CargoShip`, `ScoutShip`). Utilitza un array o llista per gestionar la flota.

1. Fes que totes les naus realitzin maniobres mitjançant el polimorfisme en temps d'execució (sobreescriptura del mètode `performManeuver()`).
2. Utilitza la sobrecàrrega de mètodes per realitzar diferents tipus de manteniment en cada nau.
