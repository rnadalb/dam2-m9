## **2. Herència: les diferents tipologies de naus espacials**

Després d’haver establert una nau base en el primer apartat, ara ens endinsem en el concepte de **herència**, un dels pilars fonamentals de la programació orientada a objectes. L'herència ens permet definir noves classes a partir d’una classe existent, reutilitzant i estenent la funcionalitat existent.

Les naus espacials tenen diferents funcions i característiques segons la seva tipologia. Així doncs, utilitzarem l'herència per crear diferents tipus de naus espacials a partir d'una classe base `SpaceShip`, evitant duplicar codi i mantenint una estructura organitzada.

### **Què és l'herència?**

L'**herència** és un mecanisme que ens permet crear una nova classe (subclasse o classe filla) a partir d’una classe existent (superclasse o classe pare). La subclasse hereta els atributs i mètodes de la superclasse i, a més, pot afegir o modificar funcionalitats específiques.

### **Aplicació en naus espacials**

Comencem definint una classe base `SpaceShip`, que contindrà els atributs i mètodes comuns a totes les naus espacials. A partir d'aquesta classe, crearem subclasses que representin naus específiques amb funcionalitats particulars, com ara **cuirassats**, **naus de càrrega**, o **exploradores**.

```mermaid
classDiagram
   class Spaceship {
      -String name
      -int fuelCapacity
      -int crewSize
      -int currentSpeed
      +launch()
      +accelerate(int amount)
      +statusReport()
   }

   class Battleship {
      -int armamentLevel
      +upgradeWeapons(int level)
      +statusReport()
   }

   class CargoShip {
      -int cargoCapacity
      +loadCargo(int amount)
      +loadCargo(String cargoType)
      +loadCargo(String cargoType, int amount)
      +statusReport()
   }

   class ScoutShip {
      -int radarRange
      +upgradeRadar(int range)
      +upgradeRadar(int range, String objectDetected)
      +statusReport()
   }

   Spaceship <|-- Battleship
   Spaceship <|-- CargoShip
   Spaceship <|-- ScoutShip
```

### **Classe base: SpaceShip**

```java
// Classe base que representa una nau espacial genèrica
public class SpaceShip {
    
    // Atributs comuns a totes les naus
    protected String name;
    protected int fuelCapacity;
    protected int crewSize;
    protected int currentSpeed;

    // Constructor per inicialitzar la nau espacial
    public SpaceShip(String name, int fuelCapacity, int crewSize, int currentSpeed) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.crewSize = crewSize;
        this.currentSpeed = currentSpeed;
    }

    // Mètode per llançar la nau espacial
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }

    // Mètode per accelerar la nau
    public void accelerate(int amount) {
        currentSpeed += amount;
        System.out.println(name + " ha accelerat a una velocitat de " + currentSpeed + " km/s.");
    }

    // Mètode per mostrar l'estat de la nau
    public void statusReport() {
        System.out.println("Informe de l'estat de la nau " + name + ":");
        System.out.println("- Velocitat actual: " + currentSpeed + " km/s");
        System.out.println("- Capacitat de combustible: " + fuelCapacity + " unitats");
        System.out.println("- Mida de la tripulació: " + crewSize + " membres");
    }
}
```

### **Subclasses: tipologies de naus espacials**

#### **Cuirassat (Battleship)**

Els **cuirassats** són naus pesants dissenyades per a la guerra. Afegeixen funcionalitats addicionals com un nivell d'armament elevat i capacitat d'atac.

```java
// Classe que representa un cuirassat, estenent la classe SpaceShip
public class Battleship extends SpaceShip {

    // Atribut específic del cuirassat
    private int armamentLevel;

    // Constructor que crida al constructor de la superclasse
    public Battleship(String name, int fuelCapacity, int crewSize, int currentSpeed, int armamentLevel) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.armamentLevel = armamentLevel;
    }

    // Mètode per millorar l'armament
    public void upgradeWeapons(int level) {
        armamentLevel += level;
        System.out.println(name + " ha millorat el seu nivell d'armament a " + armamentLevel + ".");
    }

    // Sobreescriptura del mètode statusReport per afegir el nivell d'armament
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Nivell d'armament: " + armamentLevel);
    }
}
```

#### **Nau de càrrega (CargoShip)**

Les **naus de càrrega** són dissenyades per transportar recursos com minerals o subministraments. Afegeixen atributs com la capacitat de càrrega.

```java
// Classe que representa una nau de càrrega, estenent la classe SpaceShip
public class CargoShip extends SpaceShip {

    // Atribut específic de la nau de càrrega
    private int cargoCapacity;

    // Constructor que crida al constructor de la superclasse
    public CargoShip(String name, int fuelCapacity, int crewSize, int currentSpeed, int cargoCapacity) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.cargoCapacity = cargoCapacity;
    }

    // Mètode per carregar recursos
    public void loadCargo(int amount) {
        cargoCapacity += amount;
        System.out.println(name + " ha carregat " + amount + " unitats de càrrega. Capacitat total: " + cargoCapacity);
    }

    // Sobreescriptura del mètode statusReport per afegir la capacitat de càrrega
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Capacitat de càrrega: " + cargoCapacity + " unitats");
    }
}
```

#### **Nau exploradora (ScoutShip)**

Les **naus exploradores** són més petites i ràpides, dissenyades per missions de reconeixement. Afegeixen atributs com l'abast de radar.

```java
// Classe que representa una nau exploradora, estenent la classe SpaceShip
public class ScoutShip extends SpaceShip {

    // Atribut específic de la nau exploradora
    private int radarRange;

    // Constructor que crida al constructor de la superclasse
    public ScoutShip(String name, int fuelCapacity, int crewSize, int currentSpeed, int radarRange) {
        super(name, fuelCapacity, crewSize, currentSpeed);
        this.radarRange = radarRange;
    }

    // Mètode per ampliar l'abast del radar
    public void upgradeRadar(int range) {
        radarRange += range;
        System.out.println(name + " ha ampliat l'abast del radar a " + radarRange + " km.");
    }

    // Sobreescriptura del mètode statusReport per afegir l'abast del radar
    @Override
    public void statusReport() {
        super.statusReport(); // Crida al mètode de la superclasse
        System.out.println("- Abast del radar: " + radarRange + " km");
    }
}
```

### **Exercici**

Amb la teoria presentada, ara és el moment de posar-la en pràctica.

#### **Exercici: faccions de naus espacials**

1. Crea una nau de cada tipus:
    - Un cuirassat anomenat "Destructor de Mart", amb una capacitat de combustible de 6000, una tripulació de 80, una velocitat inicial de 10 km/s i un nivell d'armament de 12.
    - Una nau de càrrega anomenada "Mineral Trader", amb una capacitat de combustible de 5000, una tripulació de 20, una velocitat de 5 km/s i una capacitat de càrrega inicial de 3000.
    - Una nau exploradora anomenada "Fast Scout", amb una capacitat de combustible de 4000, una tripulació de 10, una velocitat de 15 km/s i un abast de radar de 1000 km.

2. Llança cada nau i després:
    - Accelera cada nau a la seva velocitat màxima.
    - Millora l'armament del cuirassat, carrega recursos a la nau de càrrega, i amplia l'abast del radar de la nau exploradora.
    - Mostra l'informe d'estat complet per a cada nau.
