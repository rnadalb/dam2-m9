### **4. Composició: l'estació Tycho**

En aquest apartat treballarem un altre concepte important de la programació orientada a objectes: la **composició**. A diferència de la **herència**, la composició es basa en la relació "té un/a" entre objectes, en lloc de la relació "és un/a". Això significa que un objecte pot tenir altres objectes com a components o parts integrals.

L'**Estació Tycho** és una estació espacial massiva amb múltiples parts funcionals, com ara **docks de reparació**, **equips d'enginyeria**, **naus espacials estacionades**, i **torns de vigilància**. Aquestes parts formen el conjunt de l'estació, però no són la mateixa estació, sinó que en són components.

### **Composició vs herència**

- **Herència**: defineix una relació "**és un/a**" entre la superclasse i les subclasses (per exemple, un cuirassat és una nau espacial).
- **Composició**: defineix una relació "**té un/a**", on un objecte té altres objectes com a parts (per exemple, l'estació Tycho té docks, naus espacials, i equips d'enginyeria).

### **L'estació Tycho**

Per a aquest exercici, crearem una classe `TychoStation`, que serà l'estació espacial principal, i l'omplirem amb components com `Dock`, `RepairTeam`, i `SecurityTeam`. Cadascun d'aquests components serà una classe independent, i la classe `TychoStation` utilitzarà aquests components per realitzar diferents operacions.

1. **Classe `TychoStation`**: Representarà l'estació espacial que tindrà altres components com a part de la seva composició.
2. **Components**: Utilitzarem les classes `Dock`, `RepairTeam`, i `SecurityTeam` per representar els components de l'estació, però ara els mètodes com `dockShip()` i `repairShip()` acceptaran un objecte de tipus `SpaceShip` com a paràmetre.
3. **Operacions a L'Estació Tycho**: Implementarem mètodes dins de `TychoStation` que facin ús d'aquests components per gestionar les operacions de l'estació amb objectes `SpaceShip`.

### Diagrama UML

```mermaid
classDiagram
class TychoStation {
-Dock dock
-RepairTeam repairTeam
-SecurityTeam securityTeam
+dockShip(Spaceship ship)
+undockShip()
+repairDockedShip(Spaceship ship)
+patrolStation()
}

    class Dock {
        -String dockID
        -boolean occupied
        +parkShip(Spaceship ship)
        +releaseDock()
    }
    
    class RepairTeam {
        -String teamName
        +repairShip(Spaceship ship)
    }
    
    class SecurityTeam {
        -String teamName
        +patrol()
    }

    class Spaceship {
        -String name
        +launch()
        +performManeuver()
        +getName() String
    }

    TychoStation --> Dock : has a
    TychoStation --> RepairTeam : has a
    TychoStation --> SecurityTeam : has a
    Dock --> Spaceship : uses a
    RepairTeam --> Spaceship : repairs a
````
### **Exemple**

#### **Classe `SpaceShip`**

Reutilitzem la classe `SpaceShip` que ja hem definit anteriorment:

```java
// Classe base que representa una nau espacial genèrica
public class SpaceShip {
    protected String name;

    // Constructor per inicialitzar la nau
    public SpaceShip(String name) {
        this.name = name;
    }

    // Mètode per llançar la nau a l'espai
    public void launch() {
        System.out.println(name + " ha estat llançada a l'espai.");
    }

    // Mètode genèric per realitzar maniobres, que serà sobrescrit per les subclasses
    public void performManeuver() {
        System.out.println(name + " està realitzant una maniobra genèrica.");
    }

    // Mètode per obtenir el nom de la nau
    public String getName() {
        return name;
    }
}
```

#### **Classe `Dock`**

```java
// Classe que representa un Dock dins l'estació
class Dock {
    private String dockID;
    private boolean occupied;

    public Dock(String dockID) {
        this.dockID = dockID;
        this.occupied = false; // Per defecte, el dock està buit
    }

    // Mètode per aparcar una nau al dock
    public void parkShip(SpaceShip ship) {
        if (!occupied) {
            occupied = true;
            System.out.println("La nau " + ship.getName() + " ha estat aparcada al dock " + dockID);
        } else {
            System.out.println("El dock " + dockID + " ja està ocupat.");
        }
    }

    // Mètode per alliberar el dock
    public void releaseDock() {
        if (occupied) {
            occupied = false;
            System.out.println("El dock " + dockID + " ha estat alliberat.");
        } else {
            System.out.println("El dock " + dockID + " ja està buit.");
        }
    }
}
```
#### **Classe `RepairTeam`**
```java
// Classe que representa un equip de reparació dins l'estació
class RepairTeam {
    private String teamName;

    public RepairTeam(String teamName) {
        this.teamName = teamName;
    }

    // Mètode per reparar una nau
    public void repairShip(SpaceShip ship) {
        System.out.println("L'equip de reparació " + teamName + " està reparant la nau " + ship.getName() + ".");
    }
}
```
#### **Classe `SecurityTeam`**
```java
// Classe que representa l'equip de seguretat dins l'estació
class SecurityTeam {
    private String teamName;

    public SecurityTeam(String teamName) {
        this.teamName = teamName;
    }

    // Mètode per patrullar l'estació
    public void patrol() {
        System.out.println("L'equip de seguretat " + teamName + " està patrullant l'estació Tycho.");
    }
}
```

#### **Classe `TychoStation`**
```java
// Classe principal que representa l'Estació Tycho
public class TychoStation {
    private Dock dock;             // Composició: L'estació té un dock
    private RepairTeam repairTeam; // Composició: L'estació té un equip de reparació
    private SecurityTeam securityTeam; // Composició: L'estació té un equip de seguretat

    // Constructor per inicialitzar l'estació Tycho amb els seus components
    public TychoStation(String dockID, String repairTeamName, String securityTeamName) {
        this.dock = new Dock(dockID);
        this.repairTeam = new RepairTeam(repairTeamName);
        this.securityTeam = new SecurityTeam(securityTeamName);
    }

    // Mètode per gestionar l'aparcament d'una nau
    public void dockShip(SpaceShip ship) {
        dock.parkShip(ship);
    }

    // Mètode per alliberar el dock
    public void undockShip() {
        dock.releaseDock();
    }

    // Mètode per iniciar la reparació d'una nau
    public void repairDockedShip(SpaceShip ship) {
        repairTeam.repairShip(ship);
    }

    // Mètode per patrullar l'estació amb l'equip de seguretat
    public void patrolStation() {
        securityTeam.patrol();
    }
}
```

### **Exemple d'Ús**

Ara veurem com podem utilitzar la composició a l'Estació Tycho, fent servir objectes de la classe `SpaceShip`.

```java
public class Main {
    public static void main(String[] args) {
        // Crear l'estació Tycho amb els seus components
        TychoStation tychoStation = new TychoStation("Dock-7", "Equip Omega", "Seguretat Alfa");

        // Crear diferents naus espacials
        SpaceShip rocinante = new SpaceShip("Rocinante");
        SpaceShip donnager = new SpaceShip("Donnager");

        // Operacions dins de l'estació
        tychoStation.dockShip(rocinante);    // Aparcar una nau
        tychoStation.repairDockedShip(rocinante);  // Reparar la nau aparcada
        tychoStation.patrolStation();   // Patrullar l'estació
        tychoStation.undockShip();      // Alliberar el dock

        System.out.println(); // Separador per a la segona nau

        // Operacions amb la segona nau
        tychoStation.dockShip(donnager);  // Aparcar una altra nau
        tychoStation.repairDockedShip(donnager); // Reparar la nau
        tychoStation.undockShip();  // Alliberar el dock
    }
}
```

### **Explicació del codi:**

1. **Classe `SpaceShip`**: Representa una nau espacial genèrica, que és tractada com a objecte dins de l'estació Tycho i els seus components (Dock, RepairTeam, SecurityTeam).
    - El mètode `getName()` retorna el nom de la nau per utilitzar-lo dins de les operacions de l'estació.

2. **Classe `TychoStation`**:
    - Utilitza composició per incloure components com `Dock`, `RepairTeam`, i `SecurityTeam`.
    - Els mètodes `dockShip()` i `repairDockedShip()` ara accepten un objecte `SpaceShip` com a paràmetre, permetent treballar amb instàncies concretes de naus espacials.

3. **Composició**:
    - La classe `TychoStation` té un dock, un equip de reparació, i un equip de seguretat com a components.
    - Les naus espacials (`SpaceShip`) interactuen amb l'estació a través d'aquests components, i l'estació gestiona operacions com aparcar, reparar, i patrullar.

### **Exercici**

#### **Objectiu de l'exercici**:

L'alumnat ha de crear una estació espacial anomenada **Estació Omega** amb els seus components següents:

1. **Dock**: Un lloc per aparcar les naus.
2. **FuelTeam**: Un nou equip que s'encarrega de repostar les naus espacials.
3. **SecurityTeam**: L'equip de seguretat que patrulla l'estació.

#### **Passos**:

1. Afegeix una nova classe anomenada **`FuelTeam`**, que representi l'equip encarregat de repostar les naus espacials.
    - Crea un mètode `refuelShip()` que reposti una nau, utilitzant com a paràmetre un objecte `SpaceShip`.

2. Actualitza la classe **`TychoStation`** per incloure el nou component `FuelTeam` i afegeix un mètode per repostar les naus.

3. Crea objectes de naus espacials i fes que:
    - Aparquin a l'estació.
    - Repostin.
    - Reparin.
    - Patrullin l'estació.
    - Alliberin el dock.
