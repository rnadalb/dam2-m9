## **1. Classes i Objectes: la nau espacial**

En el context de la programació orientada a objectes (POO), els dos conceptes centrals són **classes** i **objectes**. Aquests dos conceptes ens permeten modelar el món real dins del nostre codi, creant estructures que representen entitats com naus espacials, membres de la tripulació, i sistemes de suport vital.

#### **Què és una classe?**
Una classe és una plantilla o motlle que defineix les propietats i els comportaments d’un conjunt d’objectes. Es tracta d’una representació abstracta que especifica què poden fer els objectes (mètodes) i com són (atributs). En altres paraules, una classe és com el plànol d'una nau espacial: defineix totes les parts i funcions que tindrà qualsevol nau basada en aquell plànol.

##### Exemple:
Imagina que estem dissenyant una nau espacial dels *[Belters](https://expanse.fandom.com/wiki/Belter)*. Podem crear una classe `SpaceShip` que defineixi els atributs generals d'una nau espacial i les accions que pot realitzar.

```java
public class SpaceShip {
    // Atributs
    String name;
    int fuelCapacity;
    int crewSize;
    int speed;

    // Constructor
    public SpaceShip(String name, int fuelCapacity, int crewSize, int speed) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.crewSize = crewSize;
        this.speed = speed;
    }

    // Mètodes
    public void launch() {
        System.out.println(name + " ha iniciat el seu viatge a través de l'espai.");
    }

    public void accelerate(int amount) {
        speed += amount;
        System.out.println(name + " ha accelerat a una velocitat de " + speed + " km/s.");
    }
}
```

En aquest exemple, la classe `SpaceShip` conté atributs com el nom, la capacitat de combustible, la mida de la tripulació i la velocitat. També defineix comportaments, com el llançament (`launch()`) i l'acceleració (`accelerate()`).

#### **Què és un objecte?**
Un objecte és una instància d'una classe, és a dir, una entitat concreta que segueix el patró definit per la classe. Si la classe és el plànol de la nau, l'objecte és la nau espacial construïda a partir d'aquest plànol.

##### Exemple:
Amb la classe `SpaceShip` definida, podem crear objectes que representin naus espacials concretes.

```java
public class Main {
    public static void main(String[] args) {
        // Creem un objecte 'SpaceShip' que representa una nau dels Belters
        SpaceShip rocinante = new SpaceShip("Rocinante", 5000, 5, 10);

        // Interactuem amb l'objecte 'rocinante'
        rocinante.launch(); // "Rocinante ha iniciat el seu viatge a través de l'espai."
        rocinante.accelerate(5); // "Rocinante ha accelerat a una velocitat de 15 km/s."
    }
}
```

En aquest exemple, hem creat una nau concreta anomenada *[Rocinante](https://expanse.fandom.com/wiki/Rocinante_(TV))*, que és una instància de la classe `SpaceShip`. Aquest objecte té un nom, una capacitat de combustible, una mida de tripulació i una velocitat, i podem interactuar amb ell a través dels mètodes que hem definit en la classe.

#### **Atributs i mètodes**
- **Atributs:** representen les característiques o dades d'un objecte. En el cas de la nau, els atributs serien com la capacitat de combustible o la mida de la tripulació.
- **Mètodes:** representen les accions que l'objecte pot dur a terme. Els mètodes defineixen el comportament de l'objecte, com l'acceleració o el llançament de la nau.

Els atributs solen ser variables que contenen informació sobre l'objecte, mentre que els mètodes són funcions que modifiquen els atributs o interactuen amb l'objecte d'alguna manera.

#### **Instanciació i ús d'objectes**
Quan es crea un objecte a partir d'una classe, es diu que s'està instanciant la classe. A partir de llavors, podem interactuar amb aquest objecte fent servir els seus mètodes i modificant els seus atributs.

### **Exercici**

Ara que entens el concepte de classes i objectes, és hora de posar-ho en pràctica.

#### Exercici: Cuirassat Belter “Destructor de Cinturons”

Crea una classe anomenada BelterBattleship que representi un cuirassat dels Belters amb els següents atributs:

	•	name: El nom del cuirassat (tipus String).
	•	fuelCapacity: La capacitat de combustible (tipus int).
	•	crewSize: La mida de la tripulació (tipus int).
	•	currentSpeed: La velocitat actual (tipus int).
	•	armamentLevel: El nivell d’armament del cuirassat (tipus int).

La classe ha de tenir un constructor per inicialitzar aquests atributs i mètodes per:

	•	Llençar el cuirassat (launch()).
	•	Accelerar (accelerate(int amount)).
	•	Millorar el nivell d’armament (upgradeWeapons(int level)).
	•	Fer un informe de la situació actual (statusReport()), que mostri per pantalla el nom del cuirassat, la seva velocitat actual, la capacitat de combustible restant i el nivell d’armament.

Després, crea un objecte que representi el cuirassat “Destructor de Cinturons” amb una capacitat de combustible de 8000, una tripulació de 50 membres, una velocitat inicial de 8 km/s i un nivell d’armament de 10. Fes que el cuirassat s’acceleri, millori el seu armament i mostri el seu estat.