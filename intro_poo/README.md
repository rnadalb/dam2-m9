# Introducció a la programació orientada a objectes

---

## **Introducció: El viatge dels *Belters* en el Sistema Solar**

Benvinguts al futur, on la humanitat s'ha expandit pel Sistema Solar. Les tensions polítiques entre la Terra, Mart, i el Cinturó d'Asteroides s'intensifiquen mentre les naus espacials dels *Belters* naveguen entre estacions espacials, asteroides plens de recursos i perilloses rutes comercials. Vosaltres, com a tripulació d'una nau espacial *Belter*, tindreu la missió de gestionar la vostra nau, assegurar la supervivència de la tripulació i negociar amb les altres faccions.

Per començar, haureu de crear les vostres pròpies naus i gestionar tot el que això comporta. Això vol dir dissenyar els components clau que formen la nau i tots els sistemes que l'envolten: motors, armament, tripulació i, per descomptat, els recursos limitats. Així doncs, ens endinsem en el món de les classes i els objectes, conceptes fonamentals de la programació orientada a objectes que ens permetran gestionar i simular tot l'univers de *[The Expanse](https://www.rottentomatoes.com/tv/the_expanse)*.

L'índex de continguts el trobareu aquí sota:
1. [Classes i objectes](./01_classes_objectes.md)
2. [Encapsulació](./02_encapsulacio.md)
3. [Herència](./03_herencia.md)
4. [Polimorfisme](./04_polimorfisme.md)
5. [Composició](./05_composicio.md)
6. [Excepcions i gestió d'errors](./06_excepcions_maneig_errors.md)
7. [Classes estàtiques, abstractes i interfícies](07_classes_estatiques_abstractes_interfaces.md)