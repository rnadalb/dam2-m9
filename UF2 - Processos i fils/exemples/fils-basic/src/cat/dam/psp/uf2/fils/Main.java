package cat.dam.psp.uf2.fils;

import cat.dam.psp.uf2.fils.fils.FilTipus1;
import cat.dam.psp.uf2.fils.fils.FilTipus2;
import cat.dam.psp.uf2.fils.fils.FilTipus4;

public class Main {
    public static void main(String[] args) {
        FilTipus1 f1 = new FilTipus1();

        FilTipus2 runnable = new FilTipus2();
        Thread f2 = new Thread(runnable);

        FilTipus2 runnable2 = new FilTipus2();
        Thread f3 = new Thread(runnable2, "Sóc de tipus 3");

        FilTipus4 f4 = new FilTipus4("Sóc de tipus 4");

//        f1.start();
//        f2.start();
//        f3.start();
//        f4.start();
        //sincronitzartFil1(f1, f2, f3, f4);
        sincronitzartFil14(f1, f2, f3, f4);
        //sincronitzartFil4321(f1, f2, f3, f4);
    }

    public static void sincronitzartFil1(Thread f1, Thread f2, Thread f3, Thread f4) {
        try {
            f1.start();
            f1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        f2.start();
        f3.start();
        f4.start();
    }

    public static void sincronitzartFil14(Thread f1, Thread f2, Thread f3, Thread f4) {

        try {
            f1.start();
            f1.join();
            f4.start();
            f4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        f2.start();
        f3.start();
    }

    public static void sincronitzartFil4321(Thread f1, Thread f2, Thread f3, Thread f4) {
        try {
            f4.start();
            f4.join();
            f3.start();
            f3.join();
            f2.start();
            f2.join();
            f1.start();
            f1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}