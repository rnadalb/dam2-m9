### Exercici 4: sistema de producció d vehícles

#### Enunciat
Estem desenvolupant un sistema de producció de vehicles en una fàbrica. Cada vehicle necessita ser fabricat abans de ser enviat als concessionaris. Per garantir la consistència de la producció i evitar condicions de cursa, utilitzarem mecanismes de sincronització amb Lock i ReentrantLock.

#### Tasques:

1.	Implementació de la fàbrica:
* Crea una classe `Fabrica` amb les següents característiques:
* Una variable privada `vehiclesDisponibles` que contindrà el nombre de vehicles fabricats i llestos per ser enviats.
* Un mètode `fabricarVehicle()` que permeti fabricar un vehicle.
* Un mètode `enviarVehicle()` que permeti enviar un vehicle si n’hi ha algun disponible.
* Utilitza un objecte `ReentrantLock` per sincronitzar l’accés a aquests mètodes.

2. Simulació del sistema de producció:
* Crea una classe `SimulacioFabrica` amb el mètode main per simular el sistema de producció.
* Inicialitza un objecte Fabrica amb un nombre inicial de vehicles disponibles.
*  Crea i inicia almenys cinc fils que intentin fabricar i enviar vehicles de manera concurrent. Cada fil ha d’intentar fabricar un vehicle o enviar un vehicle diverses vegades.