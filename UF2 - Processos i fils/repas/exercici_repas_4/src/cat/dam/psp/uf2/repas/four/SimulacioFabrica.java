package cat.dam.psp.uf2.repas.four;

import cat.dam.psp.uf2.repas.four.classes.Fabrica;

import java.util.Random;

public class SimulacioFabrica {
    private static final int MAX_THREADS = 5;
    private static final int MAX_EXEC = 10;

    public static void main(String[] args) throws InterruptedException {
        Fabrica fabrica = new Fabrica();
        Random random = new Random();
        Runnable tasca = () -> {
            for (int i = 0; i < MAX_EXEC; i++) {
                if (random.nextBoolean())
                    fabrica.fabricarVehicle();
                else
                    fabrica.enviarVehicle();
            }
        };

        for (int i = 0; i < MAX_THREADS; i++) {
            Thread t = new Thread(tasca, "Fil-" + (i + 1));
            t.start();
            t.join();
        }
        System.out.println("Simulació finalitzada");
    }
}