package cat.dam.psp.uf2.repas.four.classes;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Fabrica {
    private int vehiclesDisponibles = 10;
    private Lock re = new ReentrantLock();

    public void fabricarVehicle() {
        re.lock();
        try {
            vehiclesDisponibles++;
            System.out.printf("%s ha fabricat 1 vehicle. Total: %d\n",
                    Thread.currentThread().getName(),
                    vehiclesDisponibles);
        } finally {
            re.unlock();
        }
    }

    public void enviarVehicle() {
        re.lock();
        try {
            if (vehiclesDisponibles > 0) {
                vehiclesDisponibles--;
                System.out.printf("%s ha enviat 1 vehicle. Total: %d\n",
                        Thread.currentThread().getName(),
                        vehiclesDisponibles);
            } else {
                System.out.printf("%s no pot enviar vehicles. No hi ha disponibles\n",
                        Thread.currentThread().getName());
            }
        } finally {
            re.unlock();
        }
    }
}
