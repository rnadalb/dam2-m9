package cat.dam.psp.uf2.repas.five.classes;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CrustaciCruixent {
    private int cangreburguersDisponibles;
    private final Lock re = new ReentrantLock();

    public CrustaciCruixent(int cangreburguersDisponibles) {
        this.cangreburguersDisponibles = cangreburguersDisponibles;
    }

    public void prepararCangreburguer() {
        re.lock();
        try {
            cangreburguersDisponibles++;
            System.out.printf("%s ha preparat 1 Cangreburguer. Total: %d\n",
                    Thread.currentThread().getName(),
                    cangreburguersDisponibles);
        } finally {
            re.unlock();
        }
    }

    public void servirCangreburguer() {
        re.lock();
        try {
            if (cangreburguersDisponibles > 0) {
                cangreburguersDisponibles--;
                System.out.printf("%s ha servit 1 Cangreburguer. Total: %d\n",
                        Thread.currentThread().getName(),
                        cangreburguersDisponibles);
            } else {
                System.out.printf("%s no pot servir cap Cangreburguer. No hi ha disponibles\n",
                        Thread.currentThread().getName());
            }
        } finally {
            re.unlock();
        }
    }
}
