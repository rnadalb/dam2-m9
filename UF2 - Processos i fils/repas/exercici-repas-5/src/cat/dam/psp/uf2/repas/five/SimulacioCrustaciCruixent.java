package cat.dam.psp.uf2.repas.five;

import java.util.Random;

import cat.dam.psp.uf2.repas.five.classes.CrustaciCruixent;

public class SimulacioCrustaciCruixent {
    private static final int MAX_EXEC = 10;
    private static final int INITIAL_VALUE = 3;

    public static void main(String[] args) throws InterruptedException {
        CrustaciCruixent crustaciCruixent = new CrustaciCruixent(INITIAL_VALUE);
        Random random = new Random();
        Runnable tasca = () -> {
            for (int i = 0; i < MAX_EXEC; i++) {
                if (random.nextBoolean())
                    crustaciCruixent.prepararCangreburguer();
                else
                    crustaciCruixent.servirCangreburguer();
            }
        };
        Thread bob = new Thread(tasca, "Bob");
        Thread patrici = new Thread(tasca, "Patrici");
        Thread calamardo = new Thread(tasca, "Calamardo");
        Thread arenita = new Thread(tasca, "Arenita");
        Thread cranc = new Thread(tasca, "Sr. Cranc");

        bob.start();
        calamardo.start();
        patrici.start();
        arenita.start();
        cranc.start();
    }
}