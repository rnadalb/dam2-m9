### Exercici 5: El Crustaci Cruixent

Estem desenvolupant un sistema de producció de Cangreburgers al restaurant Crustaci Cruixent, gestionat pel Sr. Cranc. Cada Cangreburger necessita ser preparada abans de ser servida als clients. Per garantir la consistència de la producció i evitar condicions de cursa, utilitzarem mecanismes de sincronització amb Lock i ReentrantLock.


#### Tasques:

1. Implementació de la **_Crustaci Cruixent_**:

* Crea una classe `CrustaciCruixent` amb les següents característiques:
  * Una variable privada `cangreburgersDisponibles` que contindrà el nombre de Cangreburgers disponibles per servir.
  * Un mètode `prepararCangreburger()` que permeti preparar una Cangreburger.
  * Un mètode `servirCangreburger()` que permeti servir una Cangreburger si n’hi ha alguna disponible.
  * Utilitza un objecte `ReentrantLock` per sincronitzar l’accés a aquests mètodes.

2. Simulació del sistema de producció:
* Crea una classe `SimulacioCrustaciCruixent` amb el mètode main per simular el sistema de producció.
* Inicialitza un objecte **`CrustaciCruixent`** amb un nombre inicial de `Cangreburgers` disponibles.
* Crea i inicia almenys cinc fils que intentin preparar i servir Cangreburgers de manera concurrent. Cada fil ha d’intentar preparar una Cangreburger o servir una Cangreburger diverses vegades.

3.	Sincronització:
* Assegura’t que la classe `CrustaciCruixent` utilitza `ReentrantLock` correctament per evitar condicions de cursa.
* Cada vegada que un fil aconsegueix preparar o servir una `Cangreburger`, imprimeix un missatge indicant l’acció realitzada i el nombre de Cangreburgers disponibles per servir.