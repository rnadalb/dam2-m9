### Exercici sobre `Lock` i `ReentrantLock`

#### Enunciat

Estem dissenyant una aplicació per gestionar una biblioteca. En aquesta biblioteca, només hi ha un únic exemplar de cada llibre, i només un usuari pot agafar un llibre a la vegada. Per garantir que només un usuari pot accedir a un llibre en qualsevol moment, utilitzarem mecanismes de sincronització amb `Lock` i `ReentrantLock`.

#### Tasques:
1. **Implementació de la Biblioteca**:
    - Crea una classe `Biblioteca` amb les següents característiques:
        - Una variable privada `llibresDisponibles` que contindrà el nombre de llibres disponibles (inicialment, 3).
        - Un mètode `agafarLlibre()` que permeti a un usuari agafar un llibre si n'hi ha algun disponible.
        - Un mètode `retornarLlibre()` que permeti a un usuari retornar un llibre a la biblioteca.
        - Utilitza un objecte `ReentrantLock` per sincronitzar l'accés a aquests mètodes.

2. **Simulació de l'accés a la Biblioteca**:
    - Crea una classe `SimulacioBiblioteca` amb el mètode `main` per simular l'accés a la biblioteca.
    - Inicialitza un objecte `Biblioteca` compartit.
    - Crea i inicia almenys tres fils que intentin agafar i retornar llibres de manera concurrent. Cada fil ha d'intentar agafar un llibre, simular el temps de lectura (utilitzant `Thread.sleep`) i després retornar el llibre.

3. **Sincronització**:
    - Assegura't que la classe `Biblioteca` utilitza `ReentrantLock` correctament per evitar condicions de cursa.
    - Cada vegada que un fil aconsegueix agafar o retornar un llibre, imprimeix un missatge indicant l'acció realitzada i el nombre de llibres disponibles.

#### Requisits:
- Utilitza `ReentrantLock` per sincronitzar l'accés als mètodes `agafarLlibre` i `retornarLlibre`.
- Gestiona correctament els bloquejos per evitar deadlocks.
- El codi ha de ser eficient i fàcil de llegir.

#### Exemple de sortida:
```text
Fil-1 agafa un llibre. Llibres disponibles: 2
Fil-2 agafa un llibre. Llibres disponibles: 1
Fil-3 agafa un llibre. Llibres disponibles: 0
Fil-1 retorna un llibre. Llibres disponibles: 1
Fil-2 retorna un llibre. Llibres disponibles: 2
...
```

### Una mica de teoria...

#### `Lock` i `ReentrantLock`:
- **[Lock](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/locks/Lock.html)** és una interfície en Java que proporciona un mecanisme per obtenir un control d'accés exclusiu a una secció crítica. Permet a un fil bloquejar l'accés a un recurs compartit.
- **[ReentrantLock](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/locks/ReentrantLock.html)** és una implementació de l'interfície Lock. És un tipus de bloqueig que permet que el mateix fil pugui adquirir-lo diverses vegades (és a dir, és reentrant). Això facilita la gestió de la sincronització en estructures de dades més complexes on un fil pot necessitar accedir a recursos ja bloquejats per ell mateix.

#### Avantatges de `ReentrantLock` sobre `synchronized`:
- **Flexibilitat**: Permet intents de bloqueig amb temporitzadors (`tryLock`), verificació de bloquejos (`isLocked`), i altres operacions avançades.
- **Equitat**: Pot ser configurat per a adquirir el bloqueig de manera justa (`fairness`), evitant el bloqueig per fam.
- **Control**: Proporciona més control sobre el bloqueig i desbloqueig, ja que no està lligat al cicle de vida del mètode.