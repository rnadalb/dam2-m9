package cat.dam.psp.repas.uf2.one.classes;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Biblioteca {
    private int maxLlibres;
    private final Lock re = new ReentrantLock();

    public Biblioteca(int maxLlibres) {
        this.maxLlibres = maxLlibres;
    }

    public void agafarLlibre() {
        re.lock();
        try {
            if (maxLlibres > 0) {
                maxLlibres--;
                System.out.printf("%s - agafa un llibre. Llibres disponibles: %d\n", Thread.currentThread().getName(), maxLlibres);
            } else {
                System.out.printf("%s - intenta agafar un llibre.No hi ha disponibles\n", Thread.currentThread().getName());
            }
        } finally {
            re.unlock();
        }
    }

    public void retornarLlibre () {
        re.lock();
        try {
            maxLlibres++;
            System.out.printf("%s - retorna un llibre. Llibres disponibles: %d\n", Thread.currentThread().getName(), maxLlibres);
        } finally {
            re.unlock();
        }
    }
}
