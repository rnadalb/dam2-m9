package cat.dam.psp.repas.uf2.one;

import cat.dam.psp.repas.uf2.one.classes.Biblioteca;

import java.util.Random;

public class SimulacioBiblioteca {
    private static final int MAX_THREADS = 3;
    private static final int MAX_INTENTS = 5;
    private static final long TEMPS_PRESTEC = 500;

    public static void main(String[] args) {
        Biblioteca biblio = new Biblioteca(3);
        Random random = new Random();

        Runnable tasca = ()-> {
            for (int i = 0; i < MAX_INTENTS; i++) {
                biblio.agafarLlibre();
                // Temps de prèstec
                try {
                    Thread.sleep(TEMPS_PRESTEC);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                biblio.retornarLlibre();
            }
        };

        for (int i = 0; i < MAX_THREADS; i++) {
            Thread t = new Thread(tasca, "Fil-" + (i + 1));
            t.start();
        }
    }
}