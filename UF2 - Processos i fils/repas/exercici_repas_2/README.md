### Exercici 2: sistema de reserva d'entrades

#### Enunciat

Estem desenvolupant un sistema de reserva d'entrades per a espectacles. En aquest sistema, cada espectacle té un nombre màxim d'entrades disponibles, i múltiples usuaris poden intentar reservar entrades de manera concurrent. Per garantir la consistència de les reserves i evitar condicions de cursa, utilitzarem mecanismes de sincronització amb `Lock` i `ReentrantLock`.

#### Tasques:
1. **Implementació de la classe Espectacle**:
    - Crea una classe `Espectacle` amb les següents característiques:
        - Un espectacle ha de tenir un nom.
        - Una variable privada `entradesDisponibles` que contindrà el nombre d'entrades disponibles per un espectable determinat.
        - Un mètode `reservarEntrades(int quantitat)` que permeti a un usuari reservar un nombre determinat d'entrades si n'hi ha suficients disponibles.
        - Utilitza un objecte `ReentrantLock` per sincronitzar l'accés a aquest mètode.

2. **Simulació del sistema de reserva**:
    - Crea una classe `SimulacioReserva` amb el mètode `main` per simular el sistema de reserva.
    - Inicialitza diversos objectes `Espectacle` amb diferents nombres d'entrades disponibles.
    - Crea i inicia almenys cinc fils que intentin reservar entrades de manera concurrent per diferents espectacles. Cada fil ha d'intentar reservar un nombre aleatori d'entrades (entre 1 i 5) diverses vegades per a diferents espectacles.

3. **Sincronització**:
    - Assegura't que la classe `Espectacle` utilitza `ReentrantLock` correctament per evitar condicions de cursa.
    - Cada vegada que un fil aconsegueix reservar o no reservar entrades, imprimeix un missatge indicant l'acció realitzada i el nombre d'entrades disponibles per a l'espectacle corresponent.

#### Requisits:
- Utilitza [`ReentrantLock`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/locks/ReentrantLock.html) per sincronitzar l'accés al mètode `reservarEntrades`.
- Gestiona correctament els bloquejos per evitar deadlocks.

#### Exemple de sortida:
```text
Fil-1 reserva 3 entrades per a Espectacle: A. Entrades disponibles: 7
Fil-2 reserva 2 entrades per a Espectacle: B. Entrades disponibles: 8
Fil-3 intenta reservar 5 entrades per a Espectacle: A. No hi ha prou entrades disponibles.
Fil-1 reserva 1 entrada per a Espectacle: C. Entrades disponibles: 9
...
```