package cat.dam.psp.repas.uf2.two;

import java.util.Random;
import cat.dam.psp.repas.uf2.two.classes.Espectacle;

public class SimulacioReserva {
    private static final int MAX_PAUSA = 2500;

    public static void main(String[] args) {
        Espectacle espectacle1 = new Espectacle(8, "Kaiju nº 8");
        Espectacle espectacle2 = new Espectacle(5, "Jujutsu Kaisen");
        Espectacle espectacle3 = new Espectacle(3, "Dr. Stone");
        Random random = new Random();

        Runnable task = () -> {
            for (int i = 0; i < 10; i++) {
                // Quantitat d'entrades que intentarem reservar (màx 3)
                int quantitat = random.nextInt(2) + 1;
                // Ens aporta una selecció aleatòria a l'hora de reservar entrades
                // per un espectable concret
                int espectacleSeleccionat = random.nextInt(3);
                switch (espectacleSeleccionat) {
                    case 0:
                        espectacle1.reservarEntrades(quantitat);
                        break;
                    case 1:
                        espectacle2.reservarEntrades(quantitat);
                        break;
                    case 2:
                        espectacle3.reservarEntrades(quantitat);
                        break;
                }
                // Fem una pausa. Simula el procés de pagament de les entrades
                try {
                    Thread.sleep(random.nextInt(MAX_PAUSA));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(task, "Fil-" + (i + 1));
            thread.start();
        }

    }
}