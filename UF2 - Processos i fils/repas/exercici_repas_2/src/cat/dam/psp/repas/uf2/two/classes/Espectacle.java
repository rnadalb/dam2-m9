package cat.dam.psp.repas.uf2.two.classes;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Espectacle {
    private int entradesDisponibles;
    private String nom;
    private Lock re = new ReentrantLock();

    public Espectacle (int entradesDisponibles, String nom) {
        this.entradesDisponibles = entradesDisponibles;
        this.nom = nom;
    }

    public void reservarEntrades(int quantitat) {
        re.lock();
        try {
            if (entradesDisponibles >= quantitat) {
                entradesDisponibles -= quantitat;
                System.out.printf("%s reserva %d entrades per l'espectacle %s Entrades disponibles: %d\n",
                                  Thread.currentThread().getName(),
                                  quantitat,
                                  this.nom,
                                  this.entradesDisponibles);
            } else {
                System.out.printf("%s intenta reservar %d entrades per l'espectacle %s. No hi ha prou entrades.\n",
                        Thread.currentThread().getName(),
                        quantitat,
                        this.nom);
            }
        } finally {
            re.unlock();
        }
    }
}
