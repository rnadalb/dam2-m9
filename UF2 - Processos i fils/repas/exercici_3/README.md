### Exercici: Sistema de matriculació de vehicles

#### Enunciat

Estem desenvolupant un sistema de matriculació de vehicles provinents d'un concessionari. Cada vehicle necessita ser matriculat abans de ser entregat als clients. Per garantir la consistència de les matriculacions i evitar condicions de cursa, utilitzarem mecanismes de sincronització amb `Lock` i `ReentrantLock`.

#### Tasques:
1. **Implementació de la classe Concessionari**:
    - Crea una classe `Concessionari` amb les següents característiques:
        - Una variable privada `vehiclesDisponibles` que contindrà el nombre de vehicles disponibles per matricular.
        - Un mètode `matricularVehicle()` que permeti matricular un vehicle si n'hi ha algun disponible.
        - Utilitza un objecte `ReentrantLock` per sincronitzar l'accés a aquest mètode.

2. **Simulació del sistema de matriculació**:
    - Crea una classe `SimulacioMatriculacio` amb el mètode `main` per simular el sistema de matriculació.
    - Inicialitza diversos objectes `Concessionari` amb diferents nombres de vehicles disponibles.
    - Crea i inicia almenys **cinc** fils que intentin matricular vehicles de manera concurrent per diferents concessionaris. Cada fil ha d'intentar matricular un vehicle diverses vegades per a diferents concessionaris.

3. **Sincronització**:
    - Assegura't que la classe `Concessionari` utilitza `ReentrantLock` correctament per evitar condicions de cursa.
    - Cada vegada que un fil aconsegueix matricular o no matricular un vehicle, imprimeix un missatge indicant l'acció realitzada i el nombre de vehicles disponibles per a matricular.

#### Requisits:
- Utilitza [`ReentrantLock`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/locks/ReentrantLock.html) per sincronitzar l'accés al mètode `matricularVehicle`.
- Gestiona correctament els bloquejos per evitar deadlocks.

#### Exemple de sortida:
```
Fil-1 matricula un vehicle per al Concessionari A. Vehicles disponibles: 9
Fil-2 matricula un vehicle per al Concessionari B. Vehicles disponibles: 8
Fil-3 intenta matricular un vehicle per al Concessionari A. No hi ha vehicles disponibles.
Fil-1 matricula un vehicle per al Concessionari C. Vehicles disponibles: 9
...
```