package cat.dam.psp.repas.uf2.three;

import cat.dam.psp.repas.uf2.three.classes.Concessionari;

import java.util.Random;

public class SimulacioMatriculacio {
    public static void main(String[] args) {
        Concessionari concessionariA = new Concessionari(10, "Ferrari");
        Concessionari concessionariB = new Concessionari(10, "Lamborguini");
        Concessionari concessionariC = new Concessionari(10, "Bugatti");
        Random random = new Random();

        Runnable task = () -> {
            for (int i = 0; i < 10; i++) {
                int concessionariSeleccionat = random.nextInt(3);
                switch (concessionariSeleccionat) {
                    case 0:
                        concessionariA.matricularVehicle();
                        break;
                    case 1:
                        concessionariB.matricularVehicle();
                        break;
                    case 2:
                        concessionariC.matricularVehicle();
                        break;
                }
                try {
                    Thread.sleep(random.nextInt(100));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(task, "Fil-" + (i + 1));
            thread.start();
        }
    }
}