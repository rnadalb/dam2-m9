package cat.dam.psp.repas.uf2.three.classes;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Concessionari {
    private int vehiclesDisponibles;
    private String brand;
    private final Lock re = new ReentrantLock();

    public Concessionari(int vehiclesDisponibles, String brand) {
        this.vehiclesDisponibles = vehiclesDisponibles;
        this.brand = brand;
    }

    public void matricularVehicle() {
        re.lock();
        try {
            if (vehiclesDisponibles > 0) {
                vehiclesDisponibles--;
                System.out.printf("%s matricula un vehicle al concessionari: %s. Vehicles disponibles: %d\n",
                        Thread.currentThread().getName(),
                        brand,
                        vehiclesDisponibles);
            } else {
                System.out.printf("%s intenta matricular un vehicle al concessionari: %s. No hi ha vehicles disponibles.\n",
                        Thread.currentThread().getName(),
                        brand);
            }
        } finally {
            re.unlock();
        }
    }
}