package cat.dam.psp.uf2.sincronitza.fils.part2;


public class Productor extends OperacioBancaria {
    public Productor(CompteBancari compte) {
        super(compte);
    }

    @Override
    public void run() {
        while (true) {
            float quantitat = quantitatAleatoria();
            compte.ingressar(quantitat);
            System.out.printf("Productor ha ingressat: %.2f\n", quantitat);
            try {
                Thread.sleep(500); // Ajustar el temps de pausa si és necessari
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}