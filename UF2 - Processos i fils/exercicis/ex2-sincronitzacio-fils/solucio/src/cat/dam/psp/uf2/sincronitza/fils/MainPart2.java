package cat.dam.psp.uf2.sincronitza.fils;

import cat.dam.psp.uf2.sincronitza.fils.part2.CompteBancari;
import cat.dam.psp.uf2.sincronitza.fils.part2.Consumidor;
import cat.dam.psp.uf2.sincronitza.fils.part2.Productor;

public class MainPart2 {
    public static void main(String[] args) {
        CompteBancari compte = new CompteBancari("EUR", 1000.0f);

        Productor productor1 = new Productor(compte);
        Productor productor2 = new Productor(compte);
        Consumidor consumidor1 = new Consumidor(compte);
        Consumidor consumidor2 = new Consumidor(compte);

        productor1.start();
        productor2.start();
        consumidor1.start();
        consumidor2.start();
    }
}

