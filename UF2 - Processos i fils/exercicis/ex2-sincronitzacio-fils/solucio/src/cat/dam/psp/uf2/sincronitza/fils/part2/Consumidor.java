package cat.dam.psp.uf2.sincronitza.fils.part2;


public class Consumidor extends OperacioBancaria {
    public Consumidor(CompteBancari compte) {
        super(compte);
    }

    @Override
    public void run() {
        while (true) {
            float quantitat = quantitatAleatoria();
            try {
                compte.retirar(quantitat);
                System.out.printf("Consumidor ha retirat: %.2f\n", quantitat);
                Thread.sleep(500); // Ajustar el temps de pausa si és necessari
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}