package cat.dam.psp.uf2.sincronitza.fils.part2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class CompteBancari {
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition saldoSuficient = lock.newCondition();
    private float saldo;
    private final String moneda;

    public CompteBancari(String moneda, float saldoInicial) {
        this.moneda = moneda;
        this.saldo = saldoInicial;
    }

    public void ingressar(float quantitat) {
        lock.lock();
        try {
            saldo += quantitat;
            System.out.println("Saldo actual: " + saldo + " " + moneda);
            saldoSuficient.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void retirar(float quantitat) throws InterruptedException {
        lock.lock();
        try {
            while (saldo - quantitat < 0) {
                System.out.printf("Saldo negatiu %.2f %s\n", saldo, moneda);
                saldoSuficient.await();
            }
            saldo -= quantitat;
            System.out.println("Saldo actual: " + saldo + " " + moneda);
        } finally {
            lock.unlock();
        }
    }

    public float getSaldo() {
        lock.lock();
        try {
            return saldo;
        } finally {
            lock.unlock();
        }
    }
}
