package cat.dam.psp.uf2.sincronitza.fils.part1;

public abstract class OperacioBancaria extends Thread {
    protected CompteBancari compte;
    protected float quantitat;

    public OperacioBancaria(CompteBancari compte) {
        this.compte = compte;
    }

    protected float quantitatAleatoria() {
        return 100.0f + (float)(Math.random() * (900.0f));
    }
}