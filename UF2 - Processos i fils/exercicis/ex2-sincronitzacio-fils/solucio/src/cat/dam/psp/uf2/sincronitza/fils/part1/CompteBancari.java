package cat.dam.psp.uf2.sincronitza.fils.part1;

public class CompteBancari {
    private float saldo;
    private final String moneda;

    public CompteBancari(String moneda, float saldoInicial) {
        this.moneda = moneda;
        this.saldo = saldoInicial;
    }

    public synchronized void ingressar(float quantitat) {
        saldo += quantitat;
        System.out.printf("Saldo actual: %.2f %s\n", saldo, moneda);
        notifyAll();
    }

    public synchronized void retirar(float quantitat) throws InterruptedException {
        while (saldo - quantitat < 0) {
            System.out.printf("Saldo negatiu -%.2f %s\n", saldo, moneda);
            wait();
        }
        saldo -= quantitat;
        System.out.printf("Saldo actual: %.2f %s\n", saldo, moneda);
    }

    public synchronized float getSaldo() {
        return saldo;
    }
}
