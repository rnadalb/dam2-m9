package cat.dam.psp.ex4racingcars.classes;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class CarreraController {
    private final Pane pistaCarrera;
    private final List<Cotxe> cotxes;
    private final int limitX;
    private final int ampladaCarril;
    private static final double MAX_SPEED = 5.0;
    private final List<ResultatCarrera> resultats;

    public CarreraController(Pane pistaCarrera, int limitX, int ampladaCarril) {
        this.pistaCarrera = pistaCarrera;
        this.limitX = limitX;
        this.ampladaCarril = ampladaCarril;
        this.cotxes = new ArrayList<>();
        this.resultats = new ArrayList<>();
        inicialitzarCotxes();
    }

    private void inicialitzarCotxes() {
        Cotxe cotxe = new Cotxe(Color.RED, 0 * ampladaCarril, limitX, ampladaCarril, MAX_SPEED, this);
        cotxes.add(cotxe);
        pistaCarrera.getChildren().add(cotxe);
    }

    public void iniciarCarrera() {
        resultats.clear();
        for (Cotxe cotxe : cotxes) {
            cotxe.iniciarCarrera();
        }
    }

    public void pausarCarrera() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void reprendreCarrera() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void reiniciarCarrera() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void aturarCarrera() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void notificarFinalCarrera(Cotxe cotxe) {
        throw new UnsupportedOperationException("Not implemented yet");
    }


    private void mostrarResultats() {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}