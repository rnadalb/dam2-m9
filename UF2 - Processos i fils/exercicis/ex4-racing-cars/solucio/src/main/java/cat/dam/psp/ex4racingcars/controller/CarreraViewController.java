package cat.dam.psp.ex4racingcars.controller;

import cat.dam.psp.ex4racingcars.classes.CarreraController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class CarreraViewController implements Initializable {
    public Button btnIniciar;
    public Button btnPausar;
    public Button btnReprendre;
    public Button btnReiniciar;
    @FXML
    private Pane pistaCarrera;

    private CarreraController carreraController;
    private final int limitX = 600; // L'ample de la pista de carrera
    private final int ampladaCarril = 75; // L'alçada de cada carril

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnIniciar.setDisable(false);
        btnPausar.setDisable(true);
        btnReprendre.setDisable(true);
        btnReiniciar.setDisable(true);
        carreraController = new CarreraController(pistaCarrera, limitX, ampladaCarril);
    }

    @FXML
    private void handleIniciar() {
        if (btnIniciar.isDisabled()) {
            btnIniciar.setDisable(false);
        } else {
            carreraController.iniciarCarrera();
            btnIniciar.setDisable(true);
            btnPausar.setDisable(false);
            btnReprendre.setDisable(true);
            btnReiniciar.setDisable(false);
        }
    }

    @FXML
    private void handlePausar() {
        if (btnPausar.isDisabled()) {
            btnPausar.setDisable(false);
            btnReprendre.setDisable(false);
        } else {
            carreraController.pausarCarrera();
            btnPausar.setDisable(true);
            btnReprendre.setDisable(false);
        }
    }

    @FXML
    private void handleReprendre() {
        if (btnReprendre.isDisabled()) {
            btnReprendre.setDisable(false);
            btnPausar.setDisable(false);
        } else {
            carreraController.reprendreCarrera();
            btnReprendre.setDisable(true);
            btnPausar.setDisable(false);
        }
    }

    @FXML
    private void handleReiniciar() {
        carreraController.reiniciarCarrera();
        btnIniciar.setDisable(false);
        btnPausar.setDisable(true);
        btnReprendre.setDisable(true);
        btnReiniciar.setDisable(true);
    }

    @FXML
    private void handleSortir() {
        carreraController.aturarCarrera();
        Platform.exit();
    }

    public void aturarCarrera() {
        carreraController.aturarCarrera();
    }
}
