package cat.dam.psp.ex4racingcars;

import cat.dam.psp.ex4racingcars.controller.CarreraViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RacingCarsApp extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(RacingCarsApp.class.getResource("carrera-view.fxml"));
        Scene scene = new Scene(loader.load());// , 440, 526);
        CarreraViewController controller = loader.getController(); // Obtenim el controlador. Sempre desprès de load()
        stage.setResizable(false);
        stage.setTitle("GT Racing Cars");
        stage.setScene(scene);
        stage.show();

        // Aturar tots els fils i tancar l'aplicació
        // al polsar la X de la finestra
        stage.setOnCloseRequest(e -> {
            controller.aturarCarrera(); // Thread-Safe --> aturar els fils abans de tancar
            // System.exit(0); --> Surt a lo bèstia
            Platform.exit(); // Sortida controlada per la plataforma
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
