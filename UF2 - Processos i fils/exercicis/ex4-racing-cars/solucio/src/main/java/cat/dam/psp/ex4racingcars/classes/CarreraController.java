package cat.dam.psp.ex4racingcars.classes;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CarreraController {
    private final Pane pistaCarrera;
    private final List<Cotxe> cotxes;
    private final int limitX;
    private final int ampladaCarril;
    private static final double MAX_SPEED = 5.0;
    private final List<ResultatCarrera> resultats;

    public CarreraController(Pane pistaCarrera, int limitX, int ampladaCarril) {
        this.pistaCarrera = pistaCarrera;
        this.limitX = limitX;
        this.ampladaCarril = ampladaCarril;
        this.cotxes = new ArrayList<>();
        this.resultats = new ArrayList<>();
        inicialitzarCotxes();
    }

    private void inicialitzarCotxes() {
        Color[] colors = new Color[]{Color.RED, Color.GREEN, Color.BLUE, Color.PURPLE};
        for (int i = 0; i < 4; i++) {
            Cotxe cotxe = new Cotxe(colors[i], i * ampladaCarril, limitX, ampladaCarril, MAX_SPEED, this);
            cotxes.add(cotxe);
            pistaCarrera.getChildren().add(cotxe);
        }
    }

    public void iniciarCarrera() {
        resultats.clear();
        for (Cotxe cotxe : cotxes) {
            cotxe.iniciarCarrera();
        }
    }

    public void pausarCarrera() {
        for (Cotxe cotxe : cotxes) {
            cotxe.pausar();
        }
    }

    public void reprendreCarrera() {
        for (Cotxe cotxe : cotxes) {
            cotxe.reprendre();
        }
    }

    public void reiniciarCarrera() {
        // Eliminem els cotxes de la pista.
        // Si fem pistaCarrera.getChildren().clear() eliminarà les línies de la carretera també
        for (Cotxe cotxe : cotxes) {
            cotxe.pausar();
            // Si eliminem el runLater, el procés d'eliminacició dels cotxes seria fora del fil de JavaFX i donarà error
            // Comprova-ho tu mateix.
            Platform.runLater(() -> pistaCarrera.getChildren().remove(cotxe));
        }
        // Recorrem els botons per adaptar la GUI
        for (Node node : pistaCarrera.getChildren()) {
            if (node instanceof HBox) {
                for (Node button : ((HBox) node).getChildren()) {
                    button.setDisable(!button.getId().equals("btnIniciar") && (!button.getId().equals("btnSortir")));
                }
            }
        }
        cotxes.clear();
        inicialitzarCotxes();
    }

    public void aturarCarrera() {
        for (Cotxe cotxe : cotxes) {
            cotxe.pausar();
        }
    }

    public void notificarFinalCarrera(Cotxe cotxe) {
        ResultatCarrera resultat = new ResultatCarrera(cotxe.getColorCotxe(),
                cotxe.obtenirTempsCarrera());
        resultats.add(resultat);
        if (resultats.size() == cotxes.size()) {
            Platform.runLater(() -> {  // Error de JavaFX. No es pot fer fora del fil de JavaFX
                mostrarResultats();
                reiniciarCarrera();
            });
        }
    }


    private void mostrarResultats() {
        // Llista de resultats
        Collections.sort(resultats);
        Alert alertaResultats = new Alert(Alert.AlertType.INFORMATION);
        alertaResultats.setTitle("Resultats de la Carrera");
        alertaResultats.setHeaderText("Classificació de la Carrera");

        VBox contenidor = new VBox(10);
        for (ResultatCarrera resultat : resultats) {
            Label label = new Label(resultat.toString());
            Rectangle colorCotxe = new Rectangle(10, 10, resultat.getColorCotxe());
            contenidor.getChildren().addAll(colorCotxe, label);
        }

        alertaResultats.getDialogPane().setContent(contenidor);
        alertaResultats.showAndWait();

    }
}