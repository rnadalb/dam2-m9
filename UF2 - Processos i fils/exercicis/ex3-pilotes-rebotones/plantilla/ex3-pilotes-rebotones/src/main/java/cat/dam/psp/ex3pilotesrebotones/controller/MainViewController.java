package cat.dam.psp.ex3pilotesrebotones.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.List;

public class MainViewController {
    @FXML
    private Pane pilotaPane;

    private PilotaController pilotaController;
    private static final int DEFAULT_RADIUS = 20;

    // Inicialitza el controlador de pilotes
    public void initialize() {
        pilotaController = new PilotaController(pilotaPane, pilotaPane.getPrefWidth(), pilotaPane.getPrefHeight());
        pilotaController.addPilota(Color.RED, DEFAULT_RADIUS);
    }
}

