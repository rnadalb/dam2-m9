package cat.dam.psp.ex3pilotesrebotones.controller;

import cat.dam.psp.ex3pilotesrebotones.classes.Pilota;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class PilotaController {
    private final Pane pane;
    private final double limitX, limitY;
    private final List<Pilota> pilotes;

    public PilotaController(Pane pane, double limitX, double limitY) {
        this.pane = pane;
        this.limitX = limitX;
        this.limitY = limitY;
        this.pilotes = new ArrayList<>();
    }

    public void addPilota(Color color, int radius) {
        Pilota pilota = new Pilota(color, radius, limitX, limitY);
        pilotes.add(pilota);
        pane.getChildren().add(pilota);
        new Thread(pilota).start();
    }


}