package cat.dam.psp.ex3pilotesrebotones;

import cat.dam.psp.ex3pilotesrebotones.controller.MainViewController;
import cat.dam.psp.ex3pilotesrebotones.controller.PilotaController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainView extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource("main-view.fxml"));
        Scene scene = new Scene(loader.load(), 440, 526);
        MainViewController controller = loader.getController(); // Obtenim el controlador. Sempre desprès de load()
        stage.setResizable(false);
        stage.setTitle("Bouncing balls");
        stage.setScene(scene);
        stage.show();

        // Aturar tots els fils i tancar l'aplicació
        // al polsar la X de la finestra
        stage.setOnCloseRequest(e -> {
            controller.stopAll(); // Thread-Safe --> aturar els fils abans de tancar
            // System.exit(0); --> Surt a lo bèstia
            Platform.exit(); // Sortida controlada per la plataforma
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}

