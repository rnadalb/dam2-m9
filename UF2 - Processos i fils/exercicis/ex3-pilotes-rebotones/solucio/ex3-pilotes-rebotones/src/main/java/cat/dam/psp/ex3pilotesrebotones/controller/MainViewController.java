package cat.dam.psp.ex3pilotesrebotones.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

public class MainViewController {
    @FXML
    private Pane pilotaPane;
    @FXML
    private Slider sldVelocitat;
    @FXML
    private Label lblVelocitat;

    private PilotaController pilotaController;
    // Array amb els colors dels botons i pilotes
    private static final List<String> COLORS = Arrays.asList("#ff0000", "#0000ff", "#00ff00", "#800080");
    private static final int MAX_PILOTES = 4;
    private static final int DEFAULT_RADIUS = 20;

    // Inicialitza el controlador de pilotes
    public void initialize() {
        // Valors màxims i mínims del slider
        sldVelocitat.setMin(1.0);
        sldVelocitat.setMax(4.0);
        sldVelocitat.setValue(sldVelocitat.getMin());
        lblVelocitat.setText(String.valueOf(sldVelocitat.getValue()));

        // Actualitza la velocitat del pilota
        sldVelocitat.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                lblVelocitat.setText(String.format("%.2f", t1.doubleValue()));
                pilotaController.changeVelocity(t1.doubleValue());
            }
        });

        pilotaController = new PilotaController(pilotaPane, pilotaPane.getPrefWidth(), pilotaPane.getPrefHeight());
        for (int i = 0; i < MAX_PILOTES; i++) {
            pilotaController.addPilota(Color.web(COLORS.get(i)), DEFAULT_RADIUS);
        }
    }
    // Botons que permeten pausar/reanudar les pilotes
    @FXML
    private void togglePilota1() {
        pilotaController.pauseOrResumePilota(0);
    }

    @FXML
    private void togglePilota2() {
        pilotaController.pauseOrResumePilota(1);
    }

    @FXML
    private void togglePilota3() {
        pilotaController.pauseOrResumePilota(2);
    }

    @FXML
    private void togglePilota4() {
        pilotaController.pauseOrResumePilota(3);
    }

    // Aturar tots els fils i tancar l'aplicació
    public void stopAll() {
        pilotaController.stopAll();
    }

    // Disseny Thread-Safe. Abans de tancar l'aplicació cal aturar tots els fils
    @FXML
    private void exitApplication() {
        stopAll();
        Stage stage = (Stage) pilotaPane.getScene().getWindow();
        stage.close();
    }
}

