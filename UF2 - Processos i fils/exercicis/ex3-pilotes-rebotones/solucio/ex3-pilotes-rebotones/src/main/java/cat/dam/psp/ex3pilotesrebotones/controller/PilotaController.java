package cat.dam.psp.ex3pilotesrebotones.controller;

import cat.dam.psp.ex3pilotesrebotones.classes.Pilota;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class PilotaController {
    private final Pane pane;
    private final double limitX, limitY;
    private final List<Pilota> pilotes;

    public PilotaController(Pane pane, double limitX, double limitY) {
        this.pane = pane;
        this.limitX = limitX;
        this.limitY = limitY;
        this.pilotes = new ArrayList<>();
    }

    public void addPilota(Color color, int radius) {
        Pilota pilota = new Pilota(color, radius, limitX, limitY);
        pilotes.add(pilota);
        pane.getChildren().add(pilota);
        new Thread(pilota).start();
    }

    public void pauseOrResumePilota(int index) {
        if (index >= 0 && index < pilotes.size()) {
            pilotes.get(index).pauseOrResume();
        }
    }

    // Actualitza la velocitat de totes les pilotes
    public void changeVelocity(double velocity) {
        for (Pilota p : pilotes) {
            p.setVelocitat(velocity);
        }
    }

    // Pausa totes les pilotes (Atura els fils)
    public void stopAll() {
        for (Pilota pilota : pilotes) {
            pilota.stop();
        }
    }
}