package cat.dam.pfp.uf2.ex1;

import cat.dam.pfp.uf2.ex1.classes.*;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void sincronitzacioAsincrona(String content) {
        Thread aCounter = new ACounter(content);
        Thread eCounter = new ECounter(content);
        Thread iCounter = new ICounter(content);
        Thread oCounter = new OCounter(content);
        Thread uCounter = new UCounter(content);

        System.out.println("Inici d'execució asíncron");
        long startAsync = System.currentTimeMillis();

        aCounter.start();
        eCounter.start();
        iCounter.start();
        oCounter.start();
        uCounter.start();

        try {
            aCounter.join();
            eCounter.join();
            iCounter.join();
            oCounter.join();
            uCounter.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endAsync = System.currentTimeMillis();
        System.out.println("Temps d'execució asíncron: " + (endAsync - startAsync) + " ms");
    }

    public static void sincronitzacioSincrona(String content) {
        Thread aCounter = new ACounter(content);
        Thread eCounter = new ECounter(content);
        Thread iCounter = new ICounter(content);
        Thread oCounter = new OCounter(content);
        Thread uCounter = new UCounter(content);

        System.out.println("Inici d'execució síncron");
        long startSync = System.currentTimeMillis();

        try {
            aCounter.start();
            aCounter.join();

            eCounter.start();
            eCounter.join();

            iCounter.start();
            iCounter.join();

            oCounter.start();
            oCounter.join();

            uCounter.start();
            uCounter.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endSync = System.currentTimeMillis();
        System.out.println("Temps d'execució síncron: " + (endSync - startSync) + " ms");
    }


    public static void main(String[] args) {
        String content;
        try {
            content = new String(Files.readAllBytes(Paths.get("lipsum32.txt"))).toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        sincronitzacioSincrona(content);
        sincronitzacioAsincrona(content);

    }
}
