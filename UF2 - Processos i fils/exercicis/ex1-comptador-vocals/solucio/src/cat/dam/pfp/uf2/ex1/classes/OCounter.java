package cat.dam.pfp.uf2.ex1.classes;

public class OCounter extends Thread {
    private String text;
    private int count = 0;

    public OCounter(String text) {
        this.text = text;
    }

    public void run() {
        for (char c : text.toCharArray()) {
            if (c == 'o' || c == 'O') count++;
        }
        System.out.printf("\tRecompte de o's: %d\n", count);
    }

    public int getCount() {
        return count;
    }
}