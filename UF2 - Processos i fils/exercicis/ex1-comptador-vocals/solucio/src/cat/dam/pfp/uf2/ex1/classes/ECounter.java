package cat.dam.pfp.uf2.ex1.classes;

public class ECounter extends Thread {
    private String text;
    private int count = 0;

    public ECounter(String text) {
        this.text = text;
    }

    public void run() {
        for (char c : text.toCharArray()) {
            if (c == 'e' || c == 'E') count++;
        }
        System.out.printf("\tRecompte de e's: %d\n", count);
    }

    public int getCount() {
        return count;
    }
}