package cat.dam.pfp.uf2.ex1.classes;

public class ACounter extends Thread {
    private String text;
    private int count = 0;

    public ACounter(String text) {
        this.text = text;
    }

    public void run() {
        for (char c : text.toCharArray()) {
            if (c == 'a' || c == 'A') count++;
        }
        System.out.printf("\tRecompte de a's: %d\n", count);
    }

    public int getCount() {
        return count;
    }
}