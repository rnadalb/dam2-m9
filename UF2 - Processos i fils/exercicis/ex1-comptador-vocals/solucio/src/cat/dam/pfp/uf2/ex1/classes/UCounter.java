package cat.dam.pfp.uf2.ex1.classes;

public class UCounter extends Thread {
    private String text;
    private int count = 0;

    public UCounter(String text) {
        this.text = text;
    }

    public void run() {
        for (char c : text.toCharArray()) {
            if (c == 'u' || c == 'U') count++;
        }
        System.out.printf("\tRecompte de u's: %d\n", count);
    }

    public int getCount() {
        return count;
    }
}