package cat.dam.pfp.uf2.ex1.classes;

public class ICounter extends Thread {
    private String text;
    private int count = 0;

    public ICounter(String text) {
        this.text = text;
    }

    public void run() {
        for (char c : text.toCharArray()) {
            if (c == 'i' || c == 'I') count++;
        }
        System.out.printf("\tRecompte de i's: %d\n", count);
    }

    public int getCount() {
        return count;
    }
}