package cat.dam.pfp.uf2.ex1.classes;

public class VocalCounter extends Thread {
    private char vocalToCount;
    private String text;
    private int count = 0;

    public VocalCounter(char vocalToCount, String text) {
        this.vocalToCount = vocalToCount;
        this.text = text;
    }

    @Override
    public void run() {
        for (char c : text.toCharArray()) {
            if (Character.toLowerCase(c) == vocalToCount) {
                count++;
            }
        }
        System.out.println("Nombre de '" + vocalToCount + "': " + count);
    }

    public int getCount() {
        return count;
    }
}

