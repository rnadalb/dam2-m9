package cat.dam.pfp.uf2.ex1;

import cat.dam.pfp.uf2.ex1.classes.VocalCounter;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    private static final char[] VOCALS = {'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) {
        String content;
        try {
            content = new String(Files.readAllBytes(Paths.get("lipsum32.txt"))).toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        recompteSincron(content, VOCALS);
        recompteAsincron(content, VOCALS);
    }

    public static void recompteSincron(String content, char[] vocals) {
        long start = System.currentTimeMillis();

        for (char vocal : vocals) {
            VocalCounter counter = new VocalCounter(vocal, content);
            counter.start();
            try {
                counter.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("Temps d'execució síncron: " + (end - start) + " ms");
    }

    public static void recompteAsincron(String content, char[] vocals) {
        VocalCounter[] counters = new VocalCounter[vocals.length];

        long start = System.currentTimeMillis();

        for (int i = 0; i < vocals.length; i++) {
            counters[i] = new VocalCounter(vocals[i], content);
            counters[i].start();
        }

        for (VocalCounter counter : counters) {
            try {
                counter.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("Temps d'execució asíncron: " + (end - start) + " ms");
    }
}
