# Criptografia

[TOC]

## Introducció

La criptografia és l'art i la ciència de protegir la informació mitjançant tècniques de codificació. La criptografia ha
estat una eina crucial per a la comunicació segura i la protecció de la informació al llarg de la història.

### Història de la Criptografia

La criptografia té arrels antigues, amb els primers exemples coneguts que daten del 1900 aC en l'antic Egipte. Al llarg
de la història, la criptografia ha evolucionat de manera significativa:

- **Criptografia Clàssica (fins al segle XIX)**: Inicialment es van utilitzar mètodes simples de substitució i
  transposició. Un exemple destacat és el xifrat de Cèsar, utilitzat
  per [Juli Cèsar](https://ca.wikipedia.org/wiki/Xifratge_de_C%C3%A8sar) per a comunicacions militars.

- **Criptografia Mecànica (segle XIX - principis del segle XX)**: Amb l'avènyiment de les màquines, la criptografia es
  va tornar més avançada. Un exemple és la màquina [Enigma](https://ca.wikipedia.org/wiki/M%C3%A0quina_Enigma)
  utilitzada durant la Segona Guerra Mundial.

- **Criptografia Moderna (mitjan segle XX - present)**: El desenvolupament de la teoria de la informació i l'arribada
  dels ordinadors van portar a mètodes criptogràfics més avançats. Aquesta era va veure el naixement de la criptografia
  de clau pública i privada, juntament amb moltes altres innovacions en criptografia simètrica i asimètrica.

La criptografia continua evolucionant amb l'avanç de la tecnologia, incloent l'arribada de la criptografia quàntica, que
promet proporcionar seguretat contra les amenaces futures posades per
les [computadores quàntiques](https://ca.wikipedia.org/wiki/Ordinador_qu%C3%A0ntic).

## Criptografia Simètrica

### Introducció

La criptografia simètrica és un tipus de criptografia on es fa servir la mateixa clau per a xifrar i desxifrar la
informació. És una de les formes més antigues i més ràpides de criptografia, i s'utilitza àmpliament en situacions on la
velocitat és una consideració crucial. Malgrat la seva eficiència, la criptografia simètrica presenta reptes, com ara la
distribució segura de claus i l'escassetat de claus en escenaris de gran escala.

### Definició

La criptografia simètrica fa servir algoritmes que requereixen que tant l'emissor com el receptor tinguin accés a una
única clau secreta. Alguns dels algoritmes simètrics més comuns inclouen AES (Advanced Encryption Standard), DES (Data
Encryption Standard) i Triple DES.

![Criptografia simètrica](resources/xifrat_simetric.png)

En aquest diagrama:

1. L'Emissor (E) utilitza una **clau secreta** per a xifrar el missatge original o *text clar*.
2. La **clau secreta** proporciona el **text xifrat**.
3. L'Emissor **envia** el text xifrat al Receptor.
4. El **Receptor** utilitza l**a mateixa clau decreta** per a desxifrar el text xifrat.
5. La **clau secreta** proporciona el text **desxifrat** (el missatge original), al Receptor.

### Algorismes més habituals de xifratge simètric

#### 1. **[AES (Advanced Encryption Standard)](https://ca.wikipedia.org/wiki/Advanced_Encryption_Standard)**:

- **Descripció**: AES és un estàndard de xifratge adoptat pel govern dels EUA. És un algoritme de bloc que xifra les
  dades en blocs de 128 bits, amb claus de 128, 192 o 256 bits.
- **Exemple**:

```java
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class SymmetricEncryptionExample {

   // Genera una clau simètrica AES amb validació de la mida de la clau
    private static SecretKey generateAESKey(int keysize) throws Exception {
        // Validació de la mida de la clau
        if (keysize != 128 && keysize != 192 && keysize != 256) {
            throw new IllegalArgumentException("Mida de clau no suportada. Utilitza 128, 192 o 256 bits.");
        }

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(keysize); // Inicialitza amb la mida de clau correcta
        return keyGenerator.generateKey();
    }

    // Xifra un text donat utilitzant AES
    public static String encrypt(String data, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    // Desxifra el text xifrat utilitzant AES
    public static String decrypt(String encryptedData, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decodedBytes = Base64.getDecoder().decode(encryptedData);
        byte[] decryptedBytes = cipher.doFinal(decodedBytes);
        return new String(decryptedBytes);
    }

    public static void main(String[] args) throws Exception {
        String originalMessage = "Hola, criptografia simètrica!";
        SecretKey secretKey = generateAESKey();

        String encryptedMessage = encrypt(originalMessage, secretKey);
        System.out.println("Missatge xifrat: " + encryptedMessage);

        String decryptedMessage = decrypt(encryptedMessage, secretKey);
        System.out.println("Missatge desxifrat: " + decryptedMessage);
    }
}
```

#### Com generem una clau de xifratge personalitzada?

👉Aquí trobareu un exemple ...

```java
    /**
     * Xifra una cadena de text utilitzant un algorisme de xifratge simètric proporcionat.
     *
     * @param text El text que es vol xifrar.
     * @param clau La clau de xifratge proporcionada per l'usuari (16, 24 o 32 bytes).
     * @param algoritme El nom de l'algorisme de xifratge (ex: "AES/CBC/PKCS5Padding" o "DES/ECB/PKCS5Padding")
     * @return El text xifrat en Base64.
     * @throws Exception Si es produeix un error en el procés de xifratge o la clau té una longitud incorrecta.
     */
    public static String xifrar(String text, String clau, String algorisme) throws Exception {
        try {
            // Convertir la clau proporcionada en bytes i verificar la seva longitud
            byte[] clauBytes = clau.getBytes("UTF-8");
            int clauLength = clauBytes.length;

            // Verificar que la longitud de la clau sigui de 16, 24 o 32 bytes
            if (clauLength != 16 && clauLength != 24 && clauLength != 32) {
                throw new IllegalArgumentException("La clau ha de tenir una longitud de 16, 24 o 32 bytes.");
            }

            // Crear la clau secreta per a l'algorisme especificat
            SecretKeySpec secretKey = new SecretKeySpec(clauBytes, algoritme.split("/")[0]);

            // Configurar el Cipher amb l'algorisme especificat
            Cipher cipher = Cipher.getInstance(algoritme);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            // Xifrar el text
            byte[] textXifrat = cipher.doFinal(text.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(textXifrat);
        } catch (Exception e) {
            throw new Exception("Error en el procés de xifratge amb l'algorisme " + algorisme + ": " + e.getMessage(), e);
        }
    }
```

Sobre el codi anterior ...

| Component     | Exemple (`AES/CBC/PKCS5Padding`) | Exemple (`DES/ECB/PKCS5Padding`) | Descripció                                                                                                                                                                                                                                                                                                                                                      |
|---------------|----------------------------------|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Algorisme** | `AES`                            | `DES`                            | L'algorisme de xifratge que es farà servir. **AES** (Advanced Encryption Standard) és un algorisme de xifratge modern que suporta longituds de clau de 128, 192 i 256 bits (16, 24 o 32 bytes). **DES** (Data Encryption Standard) és un algorisme més antic amb una clau de 64 bits (actualment es considera menys segur).                                     |
| **Mode**      | `CBC`                            | `ECB`                            | El mode d'operació que defineix com es processa cada bloc de dades. **CBC** (Cipher Block Chaining) xifra cada bloc combinant-lo amb el bloc anterior, requerint un vector d'inicialització (IV) per al primer bloc. **ECB** (Electronic Codebook) xifra cada bloc de manera independent, cosa que fa que el xifratge sigui menys segur per patrons repetitius. |
| **Padding**   | `PKCS5Padding`                   | `PKCS5Padding`                   | Especifica el tipus de padding per completar els blocs de dades al tamany requerit. **PKCS5Padding** afegeix bytes de farciment perquè el bloc final tingui la longitud requerida, seguint l'estàndard **PKCS#5**. Això és necessari perquè AES i DES treballen amb blocs de mida fixa (AES de 128 bits i DES de 64 bits).                                      |

Encara potser més segur, aplicant el que anomenem **salt**.

## Què és un salt?

Un **salt** (o vector d'inicialització, **IV**) és una dada aleatòria que s'afegeix al procés de xifratge per assegurar
que cada operació produeixi un resultat únic, fins i tot si el mateix text s'està xifrant amb la mateixa clau. Això
evita que es puguin identificar patrons repetitius en el text xifrat, fent-lo més segur.

### Mirem-ho amb un exemple:

Suposem que volem xifrar el text **"Hola"** dues vegades amb la mateixa clau i **sense** utilitzar un salt.

- **Primera operació**:
    - Text original: "Hola"
    - Text xifrat (sense salt): `AB12CD34`

- **Segona operació**:
    - Text original: "Hola"
    - Text xifrat (sense salt): `AB12CD34` (El resultat és el mateix)

En aquest cas, qualsevol que vegi el text xifrat podrà notar que es repeteix el mateix resultat i podria deduir que el
text original és idèntic en ambdós casos.

Ara, si afegim un salt aleatori a cada operació de xifratge:

- **Primera operació**:
    - Salt generat: `1234`
    - Text original: "Hola"
    - Text xifrat (amb salt): `XY78AB90`

- **Segona operació**:
    - Salt generat: `5678`
    - Text original: "Hola"
    - Text xifrat (amb salt): `CD56EF12` (El resultat és diferent)

En aquest cas, encara que el text original és el mateix, el resultat final de cada operació és diferent gràcies al salt.
Això fa que sigui molt més difícil per a un atacant identificar patrons o deduir informació sobre el contingut original,
fins i tot si veu múltiples textos xifrats amb la mateixa clau.

Per exemple:

```java
    /**
     * Xifra o desxifra una cadena de text segons el valor de l'argument `xifrar`.
     *
     * @param text El text que es vol xifrar o desxifrar.
     * @param clau La clau de xifratge proporcionada per l'usuari (16, 24 o 32 bytes).
     * @param algorisme El nom de l'algorisme de xifratge (ex: "AES/CBC/PKCS5Padding").
     * @param xifrar Indica si es vol xifrar (true) o desxifrar (false) el text.
     * @return El text xifrat o desxifrat en Base64.
     * @throws Exception Si es produeix un error en el procés de xifratge o desxifratge.
     */
    public static String processarText(String text, String clau, String algorisme, boolean xifrar) throws Exception {
        try {
            // Convertir la clau proporcionada en bytes i verificar la seva longitud
            byte[] clauBytes = clau.getBytes(StandardCharsets.UTF_8);
            int clauLength = clauBytes.length;

            // Verificar que la longitud de la clau sigui de 16, 24 o 32 bytes
            if (clauLength != 16 && clauLength != 24 && clauLength != 32) {
                throw new IllegalArgumentException("La clau ha de tenir una longitud de 16, 24 o 32 bytes.");
            }

            // Crear la clau secreta per a l'algorisme especificat
            SecretKeySpec secretKey = new SecretKeySpec(clauBytes, algorisme.split("/")[0]);

            // Configurar el Cipher amb l'algorisme especificat
            Cipher cipher = Cipher.getInstance(algorisme);

            if (xifrar) {
                // Generar un IV aleatori per al xifratge
                byte[] ivBytes = new byte[16];
                SecureRandom secureRandom = new SecureRandom();
                secureRandom.nextBytes(ivBytes); // salt
                IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

                // Iniciar el xifratge
                cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
                byte[] textXifrat = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));

                // Concatenar l'IV amb el text xifrat i codificar en Base64
                byte[] ivTextXifrat = new byte[ivBytes.length + textXifrat.length];
                System.arraycopy(ivBytes, 0, ivTextXifrat, 0, ivBytes.length);
                System.arraycopy(textXifrat, 0, ivTextXifrat, ivBytes.length, textXifrat.length);
                return Base64.getEncoder().encodeToString(ivTextXifrat);
            } else {
                // Decodificar el text xifrat des de Base64 i extreure l'IV
                byte[] ivTextXifrat = Base64.getDecoder().decode(text);
                byte[] ivBytes = Arrays.copyOfRange(ivTextXifrat, 0, 16);
                byte[] textXifrat = Arrays.copyOfRange(ivTextXifrat, 16, ivTextXifrat.length);
                IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

                // Iniciar el desxifratge
                cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
                byte[] textDesxifrat = cipher.doFinal(textXifrat);
                return new String(textDesxifrat, StandardCharsets.UTF_8);
            }
        } catch (Exception e) {
            throw new Exception("Error en el procés de xifratge/desxifratge amb l'algorisme " + algorisme + ": " + e.getMessage(), e);
        }
    }
```

Un exemple d'ús:

```java
public static void main(String[] args) {
    String text = "Missatge secret";
    String clau = "123456789abcefgh"; // Exemple de clau de 16 bytes (canvia la longitud per provar, 1 caràcter 1 byte)
    String algoritme = "AES/CBC/PKCS5Padding"; // Exemple amb IV suportat

    try {
        // Xifrar el text
        String textXifrat = processarText(text, clau, algoritme, true);
        System.out.println("Text original: " + text);
        System.out.println("Text xifrat: " + textXifrat);

        // Desxifrar el text
        String textDesxifrat = processarText(textXifrat, clau, algoritme, false);
        System.out.println("Text desxifrat: " + textDesxifrat);
    } catch (Exception e) {
        System.err.println(e.getMessage());
    }
}
```

#### 2. **[DES (Data Encryption Standard)](https://ca.wikipedia.org/wiki/DES)**:

- **Descripció**: DES és un algoritme de xifratge de bloc que va ser l'estàndard de xifratge anterior a AES. Xifra les
  dades en blocs de 64 bits amb una clau de 56 bits.
- **Exemple**:

```java
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.util.Base64;

public class DESEncryptionExample {

    // Xifra un text utilitzant DES
    public static String encrypt(String data, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    // Desxifra un text xifrat utilitzant DES
    public static String decrypt(String encryptedData, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decodedBytes = Base64.getDecoder().decode(encryptedData);
        byte[] decryptedBytes = cipher.doFinal(decodedBytes);
        return new String(decryptedBytes);
    }

    public static void main(String[] args) throws Exception {
        String originalMessage = "Hola, criptografia amb DES!";
        SecretKey secretKey = generateDESKey(56); // DES només suporta 56 bits

        String encryptedMessage = encrypt(originalMessage, secretKey);
        System.out.println("Missatge xifrat amb DES: " + encryptedMessage);

        String decryptedMessage = decrypt(encryptedMessage, secretKey);
        System.out.println("Missatge desxifrat amb DES: " + decryptedMessage);
    }

    // Genera una clau simètrica DES amb validació de la mida de la clau
    private static SecretKey generateDESKey(int keysize) throws Exception {
        // Validació de la mida de la clau
        if (keysize != 56) {
            throw new IllegalArgumentException("Mida de clau no suportada per DES. Ha de ser 56 bits.");
        }

        KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
        keyGenerator.init(keysize);
        return keyGenerator.generateKey();
    }
}
```

#### 3. **[Triple DES (3DES)](https://ca.wikipedia.org/wiki/Triple_DES)**:

- **Descripció**: 3DES és una variant més segura de DES que aplica l'algoritme DES tres vegades amb dues o tres claus
  diferents.
- **Exemple**:

```java
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class TripleDESEncryptionExample {

    // Xifra un text utilitzant 3DES
    public static String encrypt(String data, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    // Desxifra un text xifrat utilitzant 3DES
    public static String decrypt(String encryptedData, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decodedBytes = Base64.getDecoder().decode(encryptedData);
        byte[] decryptedBytes = cipher.doFinal(decodedBytes);
        return new String(decryptedBytes);
    }

    public static void main(String[] args) throws Exception {
        String originalMessage = "Hola, criptografia amb 3DES!";
        SecretKey secretKey = generateTripleDESKey(168); // Prova amb 112 o 168 bits

        String encryptedMessage = encrypt(originalMessage, secretKey);
        System.out.println("Missatge xifrat amb 3DES: " + encryptedMessage);

        String decryptedMessage = decrypt(encryptedMessage, secretKey);
        System.out.println("Missatge desxifrat amb 3DES: " + decryptedMessage);
    }

    // Genera una clau simètrica 3DES amb validació de la mida de la clau
    private static SecretKey generateTripleDESKey(int keysize) throws Exception {
        // Validació de la mida de la clau
        if (keysize != 112 && keysize != 168) {
            throw new IllegalArgumentException("Mida de clau no suportada per 3DES. Utilitza 112 o 168 bits.");
        }

        KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");
        keyGenerator.init(keysize);
        return keyGenerator.generateKey();
    }
}
```

#### Comparació entre els algorismes de xifrat simètric

**[AES](https://binaryterms.com/advanced-encryption-standard-aes.html)** (_Advanced Encryption Standard_) és un
algorisme de criptografia simètrica àmpliament utilitzat per la seva seguretat i eficiència. Va ser adoptat com a
estàndard per l’Institut Nacional d’Estàndards i Tecnologia (NIST) el 2001. AES xifra dades en blocs de 128 bits, i
permet utilitzar claus de 128, 192 o 256 bits, proporcionant diferents nivells de seguretat.

Funcionament:

1. **Tipus de xifratge**: Simètric per blocs, significa que utilitza la mateixa clau per xifrar i desxifrar.
2. **Mida del bloc**: 128 bits per cada bloc de dades.
3. **Mida de la clau**: Pot ser de 128, 192 o **256 bits**, el que ofereix diferents nivells de seguretat.
4. **Rondes**: AES aplica múltiples rondes de transformacions sobre les dades. El nombre de rondes depèn de la mida de
   la clau:
    * 10 rondes per AES-128.
    * 12 rondes per AES-192.
    * 14 rondes per AES-256.

Cada ronda inclou passos com la substitució de bytes, el desplaçament de files, la barreja de columnes i l’addició de la
clau.

Comparació amb DES i 3DES:

1. **DES (Data Encryption Standard)**:
    * Mida del bloc: 64 bits.
    * Mida de la clau: 56 bits (realment, la clau és de 64 bits, però 8 bits s’utilitzen per a paritat).
    * Rondes: 16.
    * Seguretat: DES es considera obsolet per ser vulnerable als atacs de força bruta per causa de la seva curta mida de
      clau. Amb els avenços en el poder de computació, DES es pot trencar fàcilment.
2. **3DES (Triple DES)**:
    * Mida del bloc: 64 bits.
    * Mida de la clau: 168 bits (3 claus de 56 bits).
    * Rondes: 48 (es fa DES tres vegades sobre el mateix bloc).
    * Seguretat: Encara que més segur que DES, 3DES és molt menys eficient i més lent que AES, i encara utilitza blocs
      de 64 bits, la qual cosa el fa vulnerable a alguns tipus d’atacs moderns, com el birthday attack.

En resum:

* AES és més segur, ràpid i eficient que DES i 3DES, gràcies a la seva mida de bloc més gran, claus més llargues i la
  resistència a atacs coneguts.
* DES ja no es considera segur per a l’ús modern.
* 3DES, tot i que millora la seguretat de DES, ha estat progressivament substituït per AES per la seva menor eficiència
  i perquè també comença a mostrar vulnerabilitats en entorns moderns.

## Criptografia Asimètrica

La criptografia de clau pública i clau privada, també coneguda com criptografia asimètrica, és un mètode de criptografia
que utilitza un parell de claus: una clau pública i una clau privada. La clau pública es pot compartir amb tothom,
mentre que la clau privada es manté secreta. La criptografia asimètrica és fonamental per a moltes aplicacions de
seguretat, com ara el SSL/TLS que protegeix les comunicacions a Internet.

### Funcionament

La criptografia asimètrica opera amb un parell de claus: una clau pública i una clau privada. Les claus estan
matemàticament relacionades de tal manera que el que es xifra amb una clau només es pot desxifrar amb l'altra clau del
parell, i viceversa.

👉 **Per exemple**: imagina que vols enviar un missatge secret a un amic o amiga o rebre'n un, però vols assegurar-te que
ningú més pot llegir el missatge o fingir ser un de vosaltres. Aquí és on la criptografia asimètrica entra en joc.

1. **Clau pública i clau privada**:
    - Cada un de vosaltres té un parell de claus: una clau pública que tothom pot veure i una clau privada que només
      conegueu vosaltres mateixos.
    - El més interessant d'aquest sistema és que el que es xifra amb una clau (pública) només es pot desxifrar amb
      l'altra clau (privada).

2. **Enviar missatges secrets**:
    - Si el teu amic vol enviar-te un missatge secret, utilitzarà la teva clau pública per xifrar el missatge.
    - Una vegada xifrat, el missatge només es pot desxifrar amb la teva clau privada, assegurant que només tu pots
      llegir el missatge.

3. **Signatura digital**:
    - Si vols enviar un missatge al teu amic i voler que ell sàpiga que el missatge és autèntic i prové de tu, pots
      signar el missatge amb la teva clau privada.
    - La signatura digital és com un segell únic que prova que el missatge prové de tu. El teu amic pot utilitzar la
      teva clau pública per verificar la signatura i assegurar-se que el missatge és teu i no ha estat modificat per
      ningú més.

4. **Verificar qui envia el missatge**:
    - Si algú us envia un missatge signat amb la seva clau privada, podeu utilitzar la seva clau pública per verificar
      la signatura. Si la signatura és vàlida, això confirma que el missatge prové d'ell i no ha estat modificat en el
      camí.

Així, la criptografia asimètrica us permet intercanviar missatges secrets i verificar l'origen dels missatges de manera
segura, fins i tot en un món digital on hi ha molts espiadimonis i persones que intenten fer-se passar per altres.

![Criptografia asimètrica](resources/xifrat_asimetric.png)

### Què és resum d'un missatge o _Hash_

En criptografia, un resum de missatge o hash és com una empremta digital única d'un conjunt de dades, com ara un
missatge. Es crea processant el missatge a través d'un algoritme de hash específic
com [SHA-256](https://ca.wikipedia.org/wiki/SHA-2), que transforma el missatge en una seqüència fixa de caràcters,
independentment de la longitud del missatge original.

Aquí van algunes característiques clau dels resums de missatge o hashes:

1. **Unicitat**:
    - Cada missatge té un hash únic. Si canvia tan sols una lletra del missatge, l'hash canviarà completament.

2. **Irreversibilitat**:
    - No pots tornar enrere des de l'hash al missatge original. És com triturar un document; no pots tornar a juntar les
      tires de paper per recuperar el document original.

3. **Longitud fixa**:
    - No importa com de llarg o curt sigui el missatge original, l'hash sempre tindrà la mateixa longitud.

4. **Ràpid de calcular**:
    - Els algoritmes de hash estan dissenyats per ser molt ràpids, de manera que es pot calcular l'hash d'un missatge
      quasi instantàniament.

#### Exemple:

* Amb Java.

```java
import java.security.MessageDigest;

public class ExempleHash {

    public static void main(String[] args) throws Exception {
        String missatge = "Hola món!";
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hashBytes = md.digest(missatge.getBytes());
        StringBuilder hashHex = new StringBuilder();
        for (byte b : hashBytes) {
            hashHex.append(String.format("%02x", b));
        }
        System.out.println("Hash del missatge: " + hashHex.toString());
    }
}
```

* Per terminal

```shell
➜  ~ echo 'Hola món!' | openssl sha256
SHA2-256(stdin)= 65932047c4a9df62c5c79e45439de003a3a577bb07b066b1b104b0fe6278e692
```

### Signatura Digital

La signatura digital és un mecanisme que permet verificar l'origen i la integritat d'un missatge o document, de manera
similar a com ho faria una signatura manual en un document de paper. Utilitza la criptografia asimètrica per crear una "
signatura" única basada en el contingut del missatge i la clau privada del remitent.

Aquí estan els passos bàsics per crear i verificar una signatura digital:

1. **Creació de la signatura**:
    - El remitent crea un hash del missatge.
    - El remitent xifra aquest hash amb la seva clau privada per crear la signatura digital.

2. **Verificació de la signatura**:
    - El receptor desxifra la signatura digital amb la clau pública del remitent per obtenir el hash.
    - El receptor crea un hash del missatge rebut i compara aquest hash amb el hash desxifrat de la signatura.
    - Si coincideixen, això confirma que el missatge és original i prové del remitent declarat.

##### Avantatges de la signatura digital:

- **Autenticitat**: Verifica que el missatge prové del remitent declarat.
- **Integritat**: Assegura que el missatge no ha estat modificat durant la transmissió.
- **No Repudiació**: El remitent no pot negar haver enviat el missatge.

##### Desavantatges de la signatura digital:

- **Complexitat**: Requereix la gestió de claus i l'ús d'algoritmes criptogràfics.
- **Rendiment**: Pot ser lent, especialment amb missatges molt llargs o sistemes amb recursos limitats.

### Exemples:

#### Generació de claus, xifratge i desxifratge

```java
import java.security.*;
import javax.crypto.Cipher;

public class ExempleCriptografiaAsimetrica {

    public static void main(String[] args) throws Exception {
        // Generar un parell de claus
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair parellDeClaus = keyGen.generateKeyPair();
        PublicKey clauPublica = parellDeClaus.getPublic();
        PrivateKey clauPrivada = parellDeClaus.getPrivate();

        // Xifrar un missatge amb la clau pública
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, clauPublica);
        byte[] textXifrat = cipher.doFinal("Text secret".getBytes());

        // Desxifrar el missatge amb la clau privada
        cipher.init(Cipher.DECRYPT_MODE, clauPrivada);
        byte[] textDesxifrat = cipher.doFinal(textXifrat);
        System.out.println("Text desxifrat: " + new String(textDesxifrat));
    }
}
```

En aquest exemple, primer generem un parell de claus RSA. Després xifrem un missatge utilitzant la clau privada i
desxifrem el missatge xifrat utilitzant la clau pública.

#### Signatura digital

```java
import java.security.*;

public class ExempleSignaturaDigital {

    public static void main(String[] args) throws Exception {
        // Generar un parell de claus
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair parellDeClaus = keyGen.generateKeyPair();
        PrivateKey clauPrivada = parellDeClaus.getPrivate();
        PublicKey clauPublica = parellDeClaus.getPublic();

        // Crear una signatura digital del missatge
        Signature signatura = Signature.getInstance("SHA256withRSA");
        signatura.initSign(clauPrivada);
        signatura.update("Missatge per signar".getBytes());
        byte[] signaturaDigital = signatura.sign();

        // Verificar la signatura digital
        signatura.initVerify(clauPublica);
        signatura.update("Missatge per signar".getBytes());
        boolean esValida = signatura.verify(signaturaDigital);
        System.out.println("La signatura és vàlida? " + esValida);
    }
}
```

En aquest exemple, primer generem un parell de claus RSA. Després signem digitalment un missatge utilitzant la clau
privada i verifiquem la signatura utilitzant la clau pública.

#### Signatura digital amb fitxers

```java
import java.security.*;
import java.io.*;

public class ExempleSignaturaDigitalFitxer {

    public static void main(String[] args) throws Exception {
        // Generar un parell de claus
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair parellDeClaus = keyGen.generateKeyPair();
        PrivateKey clauPrivada = parellDeClaus.getPrivate();
        PublicKey clauPublica = parellDeClaus.getPublic();

        // Emmagatzemar les claus en fitxers
        try (ObjectOutputStream oosPriv = new ObjectOutputStream(new FileOutputStream("clauPrivada.ser"));
             ObjectOutputStream oosPub = new ObjectOutputStream(new FileOutputStream("clauPublica.ser"))) {
            oosPriv.writeObject(clauPrivada);
            oosPub.writeObject(clauPublica);
        }

        // Crear una signatura digital del missatge
        Signature signatura = Signature.getInstance("SHA256withRSA");
        signatura.initSign(clauPrivada);
        signatura.update("Missatge per signar".getBytes());
        byte[] signaturaDigital = signatura.sign();

        // Recuperar les claus dels fitxers
        try (ObjectInputStream oisPriv = new ObjectInputStream(new FileInputStream("clauPrivada.ser"));
             ObjectInputStream oisPub = new ObjectInputStream(new FileInputStream("clauPublica.ser"))) {
            clauPrivada = (PrivateKey) oisPriv.readObject();
            clauPublica = (PublicKey) oisPub.readObject();
        }

        // Verificar la signatura digital amb les claus recuperades
        signatura.initVerify(clauPublica);
        signatura.update("Missatge per signar".getBytes());
        boolean esValida = signatura.verify(signaturaDigital);
        System.out.println("La signatura és vàlida? " + esValida);
    }
}
```

En aquest exemple, primer generem un parell de claus RSA i després les emmagatzemem en fitxers utilitzant
`ObjectOutputStream`. Creem una signatura digital del missatge amb la clau privada original. Posteriorment, recuperem
les claus dels fitxers utilitzant `ObjectInputStream` i verifiquem la signatura digital amb la clau pública recuperada. 
