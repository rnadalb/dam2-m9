# Unitat Formativa 1: Seguretat i Criptografia

## Introducció
La seguretat i la criptografia són dos elements clau en el desenvolupament d'aplicacions modernes. Aquesta unitat ensenyarà com protegir les aplicacions i les dades aplicant criteris de seguretat en l’accés, emmagatzematge i transmissió de la informació.

### Continguts
- [Criptografia](DAM2_M9_UF1_Criptografia.md)
- [Seguretat](DAM2_M9_UF1_Seguretat.md)

### Activitats
- Criptografia
    -   [Activitat 01 - Xifrat Cèsar](activitats/act_u_xifrat_cesar/README.md)
    -   [Activitat 02 - Xifrat simètric: operació _e·ni-gm·a_](activitats/act_dos_xifrat_simetric/README.md)
    -   [Activitat 03 - Xifrat asimètric: _e·ni-gm·a evolved_](activitats/act_tres_xifrat_asimetric/README.md)
  
- Seguretat
   - _En procés ..._ 