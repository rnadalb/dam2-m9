package dam.psp.xifrat.simetric.utils;

import dam.psp.xifrat.simetric.crypto.KeySize;

public class AppDefaults {
    public static final String DEFAULT_ALGORITHM = "AES";
    public static final String EXTENSION_ENCRYPTED = "encrypted";
    public static final String EXTENSION_DECRYPTED = "decrypted";
    public static final String EXTENSION_DIGEST = "digest";
    public static final String DEFAULT_DIGEST = "SHA-256";
    public static final KeySize LOW = KeySize.LOW;
    public static final KeySize MEDIUM = KeySize.MEDIUM;
    public static final KeySize HIGH = KeySize.HIGH;
}
