package cat.dam.psp.uf1.act01.security;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName ("Test Case: CaesarCipher")
@TestMethodOrder (MethodOrderer.OrderAnnotation.class)
class CaesarCipherTest {

    @Test
    @DisplayName ("El resultat de xifrar 'a' amb un desplaçament de 1 és 'b'")
    @Order (1)
    void test_convertim_1_caracter_amb_un_desplacament_de_1() {
        String entrada = "a"; // Preparem la prova (Sabem el que entra)
        String esperat = "b"; // Sabem el que surt
        int shiftHey = 1;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }

    @Test
    @DisplayName ("El resultat de xifrar 'a' amb un desplaçament de 2 és 'c'")
    @Order (2)
    void test_convertim_1_caracter_amb_un_desplacament_de_2() {
        String entrada = "a"; // Preparem la prova (Sabem el que entra)
        String esperat = "c"; // Sabem el que surt
        int shiftHey = 2;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }

    @Test
    @DisplayName ("El resultat de xifrar 'ab' amb un desplaçament de 1 és 'bc'")
    @Order (3)
    void test_convertim_2_caracters_amb_un_desplacament_de_1() {
        String entrada = "ab"; // Preparem la prova (Sabem el que entra)
        String esperat = "bc"; // Sabem el que surt
        int shiftHey = 1;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }

    @Test
    @DisplayName ("El resultat de xifrar 'z' amb un desplaçament de 1 és 'a'")
    @Order (4)
    void test_convertim_ultim_caracter_amb_un_desplacament_de_1() {
        String entrada = "z"; // Preparem la prova (Sabem el que entra)
        String esperat = "a"; // Sabem el que surt
        int shiftHey = 1;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }

    @Test
    @DisplayName ("El resultat de xifrar 'za' amb un desplaçament de 1 és 'ab'")
    @Order (4)
    void test_convertim_ultim_i_primer_caracter_amb_un_desplacament_de_1() {
        String entrada = "za"; // Preparem la prova (Sabem el que entra)
        String esperat = "ab"; // Sabem el que surt
        int shiftHey = 1;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }

    @Test
    @DisplayName ("El resultat de xifrar 'Hola' amb un desplaçament de 1 és 'jpmb'")
    @Order (5)
    void test_convertim_ultim__i_primer_caracter_amb_un_desplacament_de_1() {
        String entrada = "Hola"; // Preparem la prova (Sabem el que entra)
        String esperat = "ipmb"; // Sabem el que surt
        int shiftHey = 1;
        // Provem la funció a testar
        String resultat = CaesarCipher.encrypt(entrada, shiftHey);
        // Llancem l'asserció
        assertEquals(esperat, resultat, String.format("CaesarCipher.encrypt(%s, %s) falla", entrada, shiftHey));
    }
}