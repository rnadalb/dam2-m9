package cat.dam.psp.uf1.act01.security;

public class CaesarCipher {

    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    public static String encrypt(String clearMessage, Integer shiftKey) {
        clearMessage = clearMessage.toLowerCase();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < clearMessage.length(); i++){
            int index = ALPHABET.indexOf(clearMessage.charAt(i));
            int keyValue = (index + shiftKey) % ALPHABET.length();
            sb.append(ALPHABET.charAt(keyValue));
        }
        return sb.toString();
    }
}
