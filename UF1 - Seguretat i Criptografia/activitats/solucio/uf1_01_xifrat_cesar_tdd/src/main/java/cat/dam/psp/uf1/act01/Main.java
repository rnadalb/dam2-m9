package cat.dam.psp.uf1.act01;

import static cat.dam.psp.uf1.act01.security.CaesarCipher.encrypt;

public class Main {
    public static void main(String[] args) {
        String encryptedMessage, clearMessage = "a";
        int key = 1;
        encryptedMessage = encrypt(clearMessage, key);
        System.out.printf("\nEl missatge xifrat és: %s\n", encryptedMessage);
    }
}
