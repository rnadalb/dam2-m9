# Activitat 1: Xifrat simètric: xifrat Cèsar

## Introducció: el xifrat Cèsar

El xifrat Cèsar, també conegut com xifrat per desplaçament, codi de Cèsar o decalatge de Cèsar, és una de les tècniques de xifratge més simples i que més s'ha utilitzat al llarg de la història. Es tracta d'una mena de xifratge per substitució en el qual cada lletra del text original és reemplaçada per una altra lletra que es troba avançada un nombre fix de posicions en l'alfabet.

Per exemple, amb un desplaçament de 3, la lletra A seria substituïda per la lletra D, la lletra B seria reemplaçada per la lletra E, etc. Aquest mètode de codificació deu el seu nom a Juli Cèsar, qui l'usava per a comunicar-se amb els seus generals.

```text
    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
    ^
    D E F G H I J K L M N O P Q R S T U V W X Y Z A B C
```

El xifratge Cèsar desplaça cada lletra un determinat nombre d’espais en l’alfabet. En aquest exemple, s’utilitza un desplaçament de tres espais, de manera que una lletra A en el text original es converteix en una lletra D en el text codificat.

En moltes ocasions, el xifrat Cèsar forma part de sistemes més complexos de codificació, com el xifratge [Vigenère](https://ca.wikipedia.org/wiki/Xifratge_de_Vigen%C3%A8re) o, fins i tot, el sistema ROT13. Com tots els xifratges de substitució alfabètica simple, el xifratge Cèsar es desxifra amb facilitat, per la qual cosa, en la pràctica no protegeix les comunicacions amb massa seguretat.

Des del punt de vista matemàtic, el xifratge César pot ser modelitzat mitjançant una operació de suma en aritmètica 
modular 26 (si l’alfabet utilitzat té 26 lletres). Així, cada lletra de l'alfabet llatí (de la A fins la Z) queda 
identificada per un enter de 0 a 25. El text pla es codifica introduint el desplaçament de tres posicions, és a dir, 
sumant el valor de la clau (en aquest cas, 3). En conseqüència:

```text
    <lletra xifrada> = (<lletra clara> + clau)   mod26
    <lletra clara>   = (<lletra xifrada> - clau) mod26


    LLETRA A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z
    CODI   0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
```

## Activitats

Pots escollir ger la teva aplicació en mode consola o en mode gràfic.

### 1. Codifica i descodifica el següent missatge utilitzant una clau de desplaçament de valor 7

```text
Text original: La sort està tirada

Text xifrat:   ___________________
```

### 2. Programa en Java

Utilitzant Java com a llenguatge base, crea un programa que permeti xifrar i desxifrar una paraula utilitzant el xifratge Cèsar. El programa ha de mostrar la següent sortida:

```text
    Introdueix una paraula per xifrar: Papallona  
    Introdueix el desplaçament: 3  
    El missatge xifrat és: sdsdoorqd  
    Vols desxifrar el missatge? (s/n): s  
    El missatge desxifrat és: papallona  
    Programa finalitzat
```
### 3. Nova utilitat

Afegeix una nova funcionalitat que permeti llegir un conjunt de paraules separades per `<ENTER>`, emmagatzemades en un fitxer de text, i genera un nou fitxer amb totes les paraules xifrades.




### Codi d’Honor

1.	Autenticitat en l’aprenentatge: Utilitza la IA per entendre els problemes i desenvolupar les teves habilitats, no per evadir els reptes d’aprenentatge.
2.	Col·laboració ètica: Col·labora de manera ètica i transparent. Ajuda els teus companys a comprendre els obstacles, però no els donis solucions completes si això comprometria la seva comprensió.
3.	Reconeixement dels recursos: Si utilitzes codi o solucions d’altres fonts, cita i reconeix adequadament aquests recursos.
4.	Responsabilitat personal: La responsabilitat del teu aprenentatge és teva. Utilitza les eines com a suport, no com a substitut de l’esforç.
