# Repositori del mòdul M09-PSP (Programació de Serveis i Processos)
> Curs 2024 - 2025
> 
>  **[Institut Eugeni d'Ors](https://www.ies-eugeni.cat) - [Vilafranca del Penedès](https://goo.gl/maps/CX6KV3ZGNsR2)**

***Professorat***
* [Rubén Nadal](https://www.linkedin.com/in/rnadalb/)



## Recursos en línia (updating ...)

* Moodle del mòdul: [DAM2-M09-PSP](https://www.ies-eugeni.cat/course/view.php?id=816)

## Guia de l'alumnat
[Presentació del mòdul at ies-eugeni.cat](https://www.ies-eugeni.cat/mod/resource/view.php?id=190737)