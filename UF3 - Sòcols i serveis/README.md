# Unitat Formativa 3: Sòcols i serveis

[[_TOC_]]

## Fonaments de la comunicació en xarxa

La comunicació en xarxa és un pilar fonamental en el món de la informàtica i les telecomunicacions, permetent la
interconnexió d'ordinadors i dispositius a través del món per compartir recursos i informació.

### Introducció a les xarxes de computadors

Una xarxa de computadors és un conjunt d'equips informàtics o nodes connectats entre si per mitjà de dispositius físics
i programari, que permeten la comunicació i l'intercanvi de dades. Les xarxes poden ser de diferents tipus i abastar des
d'una petita oficina fins al global Internet, connectant milions de dispositius arreu del món.

### Conceptes clau: protocols i ports

Un protocol de xarxa és un conjunt estandarditzat de regles que permet als ordinadors amb diferent maquinari i
programari comunicar-se entre ells, actuant com una llengua franca en el món digital. Això facilita la interacció entre
màquines similars a com les persones utilitzen una llengua comuna per a comunicar-se malgrat tenir llengües maternes
diferents.

- **[IP](https://www.rfc-es.org/rfc/rfc0791-es.txt) (_Internet Protocol_)**: És el protocol encarregat de dirigir i
  encaminar els paquets de dades des d'un node origen a un node destinatari a través de diverses xarxes. Cada dispositiu
  en una xarxa té assignada una adreça IP única, que l'identifica dins de la xarxa.

- **[TCP](https://www.rfc-es.org/rfc/rfc0793-es.txt) (_Transmission Control
  Protocol_) / [UDP](https://www.rfc-es.org/rfc/rfc0768-es.txt) (_User Datagram Protocol_)**: Són protocols de transport
  que s'utilitzen per establir la manera en què les dades són enviades i rebudes a través de la xarxa. TCP proporciona
  una comunicació fiable, establint una connexió entre el client i el servidor, garantint que les dades arribin
  ordenades i sense errors. UDP, per contra, ofereix una comunicació més ràpida però menys fiable, sense establir una
  connexió prèvia ni garantir l'arribada de paquets.

- **Ports**: Un port és un número que s'utilitza per identificar processos específics o serveis de xarxa en un host dins
  de la xarxa. Permeten a múltiples serveis funcionar simultàniament en un mateix dispositiu, facilitant la gestió del
  tràfic de xarxa.

### El model OSI

El model OSI (Open Systems Interconnection) és un model **conceptual** que caracteritza i estandarditza les funcions
d'un sistema de telecomunicacions o informàtic en set capes d'abstracció. Aquest model va ser introduït per l'ISO (
Organització Internacional per a l'Estandardització) en 1984. L'objectiu del model OSI és facilitar la comunicació entre
sistemes heterogenis i proporcionar un enteniment clar del procés de comunicació en xarxa, dividint-lo en capes amb
funcions específiques.

#### Capes del model OSI

1. **Capa física**: És responsable de la transmissió i recepció de dades no estructurades a través d'un medi físic.
   Gestiona la connexió física entre dispositius, incloent aspectes com els connectors, la tensió elèctrica i altres
   característiques físiques. **Protocols habituals**: Ethernet, USB, Bluetooth.

2. **Capa d'enllaç de dades**: Proporciona una connexió de dades fiable entre dos nodes adjacents, gestionant
   l'encapsulació de dades en trames i el control d'errors. **Protocols habituals**: PPP (Point-to-Point Protocol), IEEE
   802.11 (Wi-Fi), Ethernet.

3. **Capa de xarxa**: S'encarrega de la transmissió de dades entre dispositius en diferents xarxes. Inclou funcions com
   el direccionament, l'encaminament i el control de tràfic. **Protocols habituals**: IP (Internet Protocol), ICMP (
   Internet Control Message Protocol).

4. **Capa de transport**: Ofereix transferència de dades fiable, transparent i eficient entre punts finals, gestionant
   la segmentació, el control de flux i el control d'errors. **Protocols habituals**: TCP (Transmission Control
   Protocol), UDP (User Datagram Protocol).

5. **Capa de sessió**: Controla les sessions entre aplicacions, proporcionant serveis com l'establiment, la gestió i la
   finalització de sessions. **Protocols habituals**: NetBIOS (Network Basic Input/Output System), SSH (Secure Shell).

6. **Capa de presentació**: Assegura que les dades siguin comprensibles per les capes superiors, realitzant la traducció
   de formats de dades, la compressió i la xifrat. **Protocols habituals**: SSL (Secure Sockets Layer), TLS (Transport
   Layer Security).

7. **Capa d'aplicació**: Proporciona serveis de xarxa a les aplicacions de l'usuari, com ara l'accés a la xarxa, correu
   electrònic i transferència de fitxers. **Protocols habituals**: HTTP (Hypertext Transfer Protocol), FTP (File
   Transfer Protocol), SMTP (Simple Mail Transfer Protocol).

### El model TCP/IP

El model TCP/IP és un conjunt de protocols de comunicació que permeten la connexió d'ordinadors en xarxes extenses com
Internet. A diferència del model OSI, que té set capes, el model TCP/IP es compon de quatre capes que es correlacionen
amb les del model OSI.

#### Capes del model TCP/IP

1. **Capa d'accés a la xarxa o interfície de xarxa**: Aquesta capa correspon a la capa física i la capa d'enllaç de
   dades del model OSI. S'encarrega de la transmissió de dades a través del medi físic, incloent aspectes com
   l'adreçament físic ([MAC](https://ca.wikipedia.org/wiki/Adre%C3%A7a_MAC)), la topologia de la xarxa i els protocols
   d'accés al medi com Ethernet o Wi-Fi.

2. **Capa d'internet**: Equivalent a la capa de xarxa en el model OSI, aquesta capa s'encarrega de l'encaminament de
   paquets d'informació (datagrames) a través de diverses xarxes. Utilitza el Protocol d'Internet (IP) per a
   l'adreçament i el control de la ruta dels paquets de dades.

3. **Capa de transport**: Aquesta capa és idèntica a la seva homòloga del Model OSI i proporciona mecanismes per a la
   transferència de dades entre sistemes. Utilitza protocols com TCP per connexions orientades a la connexió i UDP per
   connexions no orientades a la connexió.

4. **Capa d'aplicació**: Aquesta capa engloba les funcions de les tres capes superiors del model OSI (presentació,
   sessió i aplicació). Proveeix protocols que permeten a les aplicacions d'usuari accedir a la xarxa. Exemples comuns
   de protocols d'aquesta capa són HTTP per a la navegació web, SMTP per a l'enviament de correus electrònics, i FTP per
   a la transferència de fitxers.

#### Comparació amb el model OSI

El model TCP/IP va ser creat amb un enfocament més pràctic i orientat a la implementació en comparació amb el Model OSI.
Mentre que el model OSI separa clarament les funcions en set capes diferents, el model TCP/IP agrupa algunes d'aquestes
funcions en menys capes per simplificar l'arquitectura i millorar l'eficiència en el món real. La següent imatge mostra
la correspondència entre les capes dels dos models:

![Model osi vs model tcp/ip](/resources/osi_tcpip_proto.png)

Ambdues arquitectures són fonamentals per a l'enteniment de les xarxes i els protocols de comunicació, encara que el
model TCP/IP és el que s'utilitza predominantment en Internet i en la majoria de les xarxes informàtiques actuals.

## Programació amb sòcols en Java

La programació amb sòcols (_sockets_) és una part essencial de la comunicació en xarxa, permetent a les aplicacions
intercanviar dades a través de diferents xarxes.

### Introducció als sòcols

Els sòcols són el punt final d'una comunicació bidireccional en xarxa entre dos programes executant-se en la xarxa.
Funcionen com una interfície per a la programació d'aplicacions de xarxa en el sistema operatiu, permetent als
desenvolupadors enviar i rebre dades a través de protocols com TCP o UDP. *
*_Un sòcol està definit per una adreça IP combinada amb un número de port_**, creant un punt únic d'accés per a la
comunicació.

Els sòcols són fonamentals per a la creació d'aplicacions de xarxa com navegadors web, jocs en línia, aplicacions de
xat, i qualsevol altra aplicació que requereixi comunicació en xarxa. Proporcionen els mitjans necessaris per a que les
dades siguin intercanviades entre clients i servidors, facilitant així el desenvolupament de serveis i aplicacions
distribuïdes.

En Java, la programació amb sòcols s'executa mitjançant les
classes [Socket](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/Socket.html
) i [ServerSocket](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/ServerSocket.html
) de la biblioteca [java.net](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/package-summary.html
). Un objecte `Socket` representa un sòcol de client que pot connectar-se a un servidor, mentre que un
objecte `ServerSocket` representa un sòcol de servidor que espera connexions entrants.

### Exemple

#### [Part del servidor](exemples/echoserver)

```java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
    public static void main(String[] args) {
        int portNumber = 1234; // Port on escolta el servidor. Alerta, per norma el port ha de ser major de 1024

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("EchoServer està escoltant al port " + portNumber);
            while (true) {
                try (Socket clientSocket = serverSocket.accept();
                     PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                     BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {

                    InetAddress clientAddress = clientSocket.getInetAddress();
                    System.out.println("Client amb adreça IP " + clientAddress.getHostAddress() + " s'ha connectat.");

                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        System.out.println("El client amb adreça IP " + clientAddress.getHostAddress() + " ha enviat: " + inputLine);
                        out.println(inputLine);
                    }
                    System.out.println("El client amb adreça IP " + clientAddress.getHostAddress() + " s'ha desconnectat.");
                } catch (IOException e) {
                    System.out.println("Excepció capturada quan s'intentava comunicar amb un client.");
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            System.out.println("Excepció capturada quan s'intentava escoltar al port " + portNumber);
            System.out.println(e.getMessage());
        }
    }
}
```

#### [Part del client](exemples/echoclient)

```java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class EchoClient {
    public static void main(String[] args) {
        String hostName = "127.0.0.1"; // Adreça IP del servidor
        int portNumber = 1234; // Port on escolta el servidor. Alerta, per norma el port ha de ser major de 1024

        try (Socket echoSocket = new Socket(hostName, portNumber);
             PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            System.out.println("Connectat al servidor Echo. Escriu un missatge:");

            String userInput;
            while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                System.out.println("echo: " + in.readLine());
            }
        } catch (UnknownHostException e) {
            System.err.println("No es coneix l'amfitrió " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No es pot obtenir la I/O per la connexió a " + hostName);
            System.exit(1);
        }
    }
}
```

En aquest exemple, el servidor de sòcol `EchoServer` escolta en un port específic esperant connexions entrants. Quan un
client s'uneix, el servidor llegeix les línies de text enviades pel client i les retorna, creant un efecte d'eco. El
client de sòcol `EchoClient` es connecta al servidor, envia missatges i imprimeix les respostes que rep del servidor.

Fixa't que s'executen des de dues aplicacions diferents... (les trobaràs als [exemples](exemples), prova-ho!!!)

## Desenvolupament d'aplicacions de serveis en xarxa

El desenvolupament d'aplicacions de serveis en xarxa implica la creació de programari que opera a través de la xarxa, proporcionant funcionalitats específiques o dades a clients remots.
Aquestes aplicacions poden incloure servidors web, serveis de correu electrònic, servidors de bases de dades, aplicacions de xat, i molt més. Tot un món per descobrir.

### Llibreries de classes en Java

Java ofereix una àmplia gamma de llibreries de classes que simplifiquen el desenvolupament de serveis en xarxa. Aquestes llibreries encapsulen els detalls de baix nivell de la comunicació de xarxa, permetent als desenvolupadors i desenvolupadores centrar-se en la lògica d'aplicació. Algunes de les llibreries més utilitzades són:

- [java.net:](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/package-summary.html) Conté classes bàsiques per a la comunicació en xarxa, incloent sòcols i adreces IP.
- [javax.net](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/javax/net/package-summary.html): Ofereix funcionalitats per a connexions segures a través de sòcols SSL i TLS.
- [java.nio](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/nio/package-summary.html): Proporciona classes per a programació d'entrada/sortida no bloquejant, la qual cosa pot millorar l'escalabilitat d'aplicacions en xarxa.

#### Exemple: un servei web elemental

```java
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

public class SimpleHttpServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/welcome", new WelcomeHandler());
        server.setExecutor(null); // Utilitza un executor per defecte
        server.start();
        System.out.println("El servidor està escoltant al port 8000");
    }

    static class WelcomeHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "Benvingut al Servei Web Simple!";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}
```

Prova l'anterior [codi](exemples/simple_web_server) al teu IDE. Una vegada estigui executant-se, ves al següent enllaç http://localhost:8000/welcome amb el teu navegador preferit.

👉 Consulta la documentació de la classe [HttpServer](https://docs.oracle.com/en/java/javase/11/docs/api/jdk.httpserver/com/sun/net/httpserver/package-summary.html).  

### Criteris d’eficiència i disponibilitat

Quan es desenvolupen serveis en xarxa, és crucial considerar l'eficiència i la disponibilitat. Això inclou la gestió adequada dels recursos del servidor, l'escalabilitat per manejar grans càrregues, i la robustesa per assegurar la continuïtat del servei. Algunes estratègies per millorar aquests aspectes són:

- Implementació de tècniques de programació concurrents, com ara fils o executors, per processar múltiples sol·licituds simultàniament.
- Utilització de càrrega i balancejadors per distribuir les peticions entre múltiples instàncies del servei.
- Aplicació de mecanismes de recuperació davant errors, com ara reintents, connexions redundants, i failover automàtic.

Podem concretar una mica més:

#### Eficiència

L'eficiència en les aplicacions de xarxa es refereix a la capacitat d'una aplicació per manejar una gran quantitat de tràfic de xarxa i processar sol·licituds amb el mínim de recursos i en el menor temps possible. Per aconseguir aquesta eficiència, cal considerar els següents aspectes, entre altres:

* **Optimització de codi**: el codi ha de ser net i eficient, eliminant operacions innecessàries, optimitzant bucles i algorismes, i utilitzant estructures de dades adequades per a la tasca en qüestió.

* **I/O no bloquejant**: les operacions d'entrada/sortida (I/O) no bloquejant permeten a l'aplicació continuar processant altres tasques mentre espera que es completin les operacions d'I/O, millorant l'ús de recursos i permetent un major paral·lelisme.

* **Memòria cau (_cache_)**: emmagatzemar les dades freqüentment accedides o costoses de generar pot reduir significativament els temps de resposta i la càrrega en els sistemes de bases de dades o altres serveis back-end.

* **Concurrència**: aprofitar la concurrència i el paral·lelisme per a processar múltiples sol·licituds simultàniament, utilitzant models com `ThreadPool` o frameworks que suporten operacions asincròniques.

* **Balanç de càrrega**: distribuir la càrrega entre múltiples servidors o instàncies de l'aplicació pot ajudar a manejar pics de tràfic i utilitzar de manera més eficient els recursos disponibles.

#### Disponibilitat

La disponibilitat es refereix a la capacitat d'una aplicació de serveis en xarxa per ser accessible i operativa quan els usuaris la necessiten. Hi ha diverses estratègies per a assegurar una alta disponibilitat:

* **Redundància**: implementar múltiples còpies redundants de components crítics (servidors, bases de dades, etc.) assegura que si un component falla, hi ha altres que poden prendre el relleu sense interrompre el servei.

* **Balancejadors de càrrega**: els balancejadors de càrrega distribueixen el tràfic entre els servidors disponibles, millorant la capacitat de resposta i prevenint punts únics de fallada.

* **Clustering**: el clustering permet a múltiples servidors treballar junts com un únic sistema, proporcionant una millor escalabilitat i disponibilitat.

* **Monitoratge i alertes**: un sistema de monitoratge en temps real amb alertes permet detectar ràpidament problemes i respondre abans que afectin significativament els usuaris.

* **Estratègies de failover**: preparar mecanismes de failover automàtics o manuals per a transferir el processament a sistemes de reserva quan es detecta un fall.

* **Proves de càrrega i estrès**: realitzar proves de càrrega i estrès per a identificar punts febles en l'aplicació i la infraestructura abans que es produeixin en un entorn de producció.

Certament, puc proporcionar-te exemples simples en Java que il·lustren el concepte d'I/O no bloquejant i l'ús de memòria caixa (cache).

#### Exemple d'I/O no bloquejant

Com hem dit abans el paquet `java.nio` proporciona funcionalitats per a I/O no bloquejant. A continuació es mostra un exemple molt bàsic d'utilització d'un canal i un buffer per a llegir dades de manera no bloquejant d'un fitxer:

```java
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.file.*;

public class NonBlockingIOExample {
    public static void main(String[] args) {
        Path file = Paths.get("example.txt"); // Canvia aquesta ruta al teu fitxer

        try (FileChannel fileChannel = (FileChannel.open(file, StandardOpenOption.READ))) {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            while (fileChannel.read(buffer) > 0) {
                buffer.flip(); // Prepare the buffer to read
                while (buffer.hasRemaining()) {
                    System.out.print((char) buffer.get()); // Llig 1 byte cada vegada
                }
                buffer.clear(); // Esborra el buffer per tornar a escriure de nou
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

### Exemple simple de memòria cau (_cache_)

Seguit trobareu la implementació d'una memòria _cache_ utilitzant un `HashMap` per emmagatzemar dades que s'accedeixen freqüentment:

```java
import java.util.*;

public class SimpleCache<K, V> {
    private final Map<K, V> cache;

    public SimpleCache() {
        this.cache = new HashMap<>();
    }

    public V get(K key) {
        return cache.get(key);
    }

    public void put(K key, V value) {
        cache.put(key, value);
    }
}

class CacheExample {
    public static void main(String[] args) {
        SimpleCache<String, String> cache = new SimpleCache<>();

        // Suposem que aquesta dada és costosa de generar
        String expensiveData = "Dada molt costosa de generar";
        String key = "clau123";

        // Emmagatzemem les dades a la cau (cache)
        cache.put(key, expensiveData);

        // En el futur, podem obtenir les dades directament des de la caixa sense el cost de generar-les
        String cachedData = cache.get(key);
        System.out.println(cachedData);
    }
}
```

Com comprendreu aquesta simple solució no és molt útil en entorns de producció per això es recomanaria utilitzar implementacions de memòries cau més avançades com [Google Guava](https://github.com/google/guava) o [Caffeine](https://github.com/ben-manes/caffeine) entre altres alternatives.


## El Protocol HTTP

El Protocol de Transferència d'Hipertext (HTTP) és el protocol de comunicació que serveix com a fonament per a la World Wide Web. Desenvolupat per Tim Berners-Lee al CERN a principis dels anys 90, HTTP va ser creat per facilitar la transferència d'hipertext, permetent als usuaris accedir a documents interconnectats (pàgines web) mitjançant enllaços. Des de llavors, HTTP ha evolucionat significativament i actualment suporta no només documents d'hipertext, sinó també la transferència de dades entre clients (normalment navegadors web o aplicacions mòbils) i servidors a través d'Internet.

### Funcionament

HTTP és un protocol orientat a sol·licitud-resposta en el model client-servidor. Això significa que un client (per exemple, un navegador web) inicia una petició HTTP cap a un servidor, i el servidor processa aquesta petició i retorna una resposta al client. La comunicació entre client i servidor es realitza mitjançant missatges HTTP, que contenen capçaleres (headers) i, en alguns casos, un cos de missatge.

### Model sol·licitud-resposta

1. **Sol·licitud del client**: quan un usuari fa clic en un enllaç, introdueix una URL al navegador, o envia un formulari web, el navegador genera una sol·licitud HTTP al servidor. Aquesta sol·licitud inclou un mètode HTTP (com GET o POST), l'URI del recurs sol·licitat, la versió de HTTP, capçaleres addicionals que proporcionen informació sobre la sol·licitud, i potencialment un cos de missatge.

2. **Processament del servidor**: el servidor rep la sol·licitud, l'interpreta i determina com respondre. Això pot implicar recuperar un arxiu HTML, processar dades d'un formulari, o realitzar una consulta a una base de dades.

3. **Resposta del servidor**: un cop el servidor ha completat el processament de la sol·licitud, genera una resposta HTTP que envia de tornada al client. Aquesta resposta inclou una línia d'estat amb el codi d'estat HTTP (per exemple, 200 OK, 404 Not Found), capçaleres de resposta que proporcionen informació sobre el servidor i les dades retornades, i un cos de missatge amb les dades sol·licitades (per exemple, el contingut d'una pàgina web).

### Mètodes HTTP

Els mètodes web HTTP, també coneguts com a mètodes de sol·licitud, defineixen l'acció que el client desitja realitzar sobre el recurs identificat per l'URI (Uniform Resource Identifier) en la sol·licitud. Cada mètode té un propòsit específic i semàntica associada, que indica al servidor com processar la sol·licitud. A continuació, es detallen, breument, els mètodes HTTP més comuns:

- **GET**: per obtenir recursos.
- **POST**: per enviar dades a un recurs per a la creació o actualització.
- **PUT**: per actualitzar o crear un recurs.
- **DELETE**: per eliminar un recurs.
- **PATCH**: per aplicar actualitzacions parcials a un recurs.
- **HEAD**: similar a GET però solament recupera, únicament, les capçaleres del recurs.
- **OPTIONS**: per obtenir els mètodes HTTP suportats per un recurs.

### Petició HTTP
Cada petició HTTP conté elements clau que informen el servidor sobre què es demana i com processar la sol·licitud. Aquests elements inclouen, entre altres:

* **Línia de sol·licitud**: conté el mètode HTTP, l'URI del recurs sol·licitat, i la versió de HTTP.
* **Capçaleres de sol·licitud**: proporcionen informació addicional sobre el client, el recurs sol·licitat, o la petició mateixa. Inclouen camps com Host, User-Agent, Accept, entre altres.
* **Cos de la sol·licitud**: ppcional, inclòs en alguns tipus de sol·licituds com POST o PUT, conté les dades enviades al servidor.

### Resposta HTTP
L'estructura típica d'una resposta HTTP és:
- **Línia d'estat**: versió de HTTP, codi d'estat, descripció de l'estat, ... per exemple **_HTTP/1.1 200 OK_**.
- **Capçaleres (headers)**: són les capçaleres de la resposta, per exemple **_Content-Type: text/html_**.
- **Cos de la resposta**: opcional són les dades retornades pel servidor.

### Codis d'estat HTTP

Els codis d'estat HTTP són part de la resposta HTTP enviada pel servidor per indicar l'estat de la sol·licitud realitzada pel client. Aquests codis són estandarditzats per l'RFC 7231 i altres especificacions relacionades, proporcionant una manera ràpida d'identificar com ha estat processada la sol·licitud. Es classifiquen en cinc categories, segons el primer dígit del codi:

* **1xx: Respostes informatives**: indiquen que la sol·licitud ha estat rebuda i el procés continua.
  - **100 Continue**: el servidor ha rebut les capçaleres de la sol·licitud i el client hauria de continuar enviant el cos de la sol·licitud.
  - **101 Switching Protocols**: el servidor està canviant els protocols segons el que ha estat sol·licitat pel client.

* **2xx: Èxit**: signifiquen que la sol·licitud va ser rebuda, entesa, acceptada i processada amb èxit.
  - **200 OK**: la sol·licitud ha tingut èxit. La informació retornada depèn del mètode utilitzat en la sol·licitud.
  - **201 Created**: la sol·licitud ha tingut èxit i s'ha creat un nou recurs.
  - **204 No Content**: la sol·licitud ha tingut èxit però el servidor no retorna cap contingut.

* **3xx: Redireccions**: aquests codis indiquen que es necessiten accions addicionals per completar la sol·licitud, normalment redireccionar a un altre URI.
  - **301 Moved Permanently**: el recurs ha estat mogut de manera permanent a una nova URI.
  - **302 Found**: el recurs ha estat trobat en una altra URI de manera temporal.
  - **304 Not Modified**: el recurs no ha estat modificat des de l'última sol·licitud.

* **4xx: Errors del client**: indiquen que hi va haver un problema amb la sol·licitud que va impedir que el servidor la processés.
  - **400 Bad Request**: la sol·licitud no s'ha pogut processar degut a una sintaxi incorrecta.
  - **401 Unauthorized**: la sol·licitud requereix autenticació de l'usuari.
  - **404 Not Found**: el recurs sol·licitat no s'ha trobat en aquest servidor.
  - **405 Method Not Allowed**: el mètode utilitzat en la sol·licitud no és permès per aquest recurs.

* **5xx: Errors del servidor**: aquests codis indiquen que el servidor ha fallat en completar una sol·licitud aparentment vàlida.
  - **500 Internal Server Error**: el servidor s'ha trobat amb una situació que no sap com manejar.
  - **503 Service Unavailable**: el servidor no està disponible temporalment, sovint per manteniment o sobrecàrrega.

### Què és un servidor web

Un **servidor web** és un programari que accepta sol·licituds HTTP dels clients, normalment navegadors web, i els serveix contingut web, com ara pàgines HTML, imatges, i arxius, o genera contingut dinàmic en resposta a les peticions dels usuaris. A més de servir contingut estàtic, els servidors web moderns solen ser capaços de suportar scripts del costat del servidor per generar pàgines web dinàmiques basades en les accions de l'usuari, les dades sol·licitades, o qualsevol altre criteri.

Alguns dels servidors web més utilitzats actualment inclouen:

* **_[Apache HTTP Server](https://httpd.apache.org/)_**: és un dels servidors web més antics i utilitzats àmpliament. És conegut per la seva robustesa, flexibilitat i gran quantitat de mòduls disponibles que permeten ampliar les seves funcionalitats.

* **_[Nginx](https://www.nginx.com/)_**: és un servidor web/proxy invers lleuger i d'alt rendiment. A més de servir contingut estàtic i suportar contingut dinàmic a través de la interfície de passarel·la d'aplicacions comunes (CGI), també s'utilitza àmpliament per la seva capacitat per manejar un gran nombre de connexions simultànies.

* **_[Microsoft Internet Information Services (IIS)](https://www.iis.net/)_**: és un conjunt de serveis de servidor d'Internet per a sistemes operatius Windows. Suporta HTTP, HTTP/2, HTTPS, FTP, FTPS, SMTP i NNTP. IIS és conegut per la seva integració amb altres productes de Microsoft i per suportar una àmplia gamma de tasques d'administració web.

Tal com hem vist a l'anterior exemple Java ens permet desenvolupar un servidor web i ens prové de diferents paquets i classes que ens faciliten aquesta tasca.

#### Exemple: servidor web simple (reconeix GET i POST)

```java
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class MetodesWeb {
    private static final int DEFAULT_PORT = 8000;

    public static void main(String[] args) throws Exception {
        // Crea un servidor HTTP escoltant en el port 8000
        HttpServer server = HttpServer.create(new InetSocketAddress(DEFAULT_PORT), 0);

        // Gestor per a les sol·licituds GET i POST a la ruta "/"
        server.createContext("/", new HomePage());
        server.setExecutor(null); // Crea un executor per defecte
        server.start();
        System.out.println("El servidor està escoltant en el port " + DEFAULT_PORT);
    }

    private static String handleGetRequest(HttpExchange exchange) {
        // En un cas real, llegiries el cos de la sol·licitud aquí
        return "<H1>Resposta del mètode GET<H1>";
    }


    private static String handlePostRequest(HttpExchange exchange) {
        // Llegeix el cos de la sol·licitud
        InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String formData = br.lines().collect(Collectors.joining(System.lineSeparator()));

        // Mostra els paràmetres rebuts
        System.out.println("Dades rebudes en la sol·licitud POST: " + formData);

        return "Resposta del mètode POST amb dades: " + formData;
    }


    static class HomePage implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String resposta;
            int errorCode = 200;
            // Obtenim el mètode que ens ha enviat el client
            if ("GET".equals(exchange.getRequestMethod())) {
                resposta = handleGetRequest(exchange);
            } else if ("POST".equals(exchange.getRequestMethod())) {
                resposta = handlePostRequest(exchange);
                errorCode = 201;
            } else {
                resposta = "Mètode no suportat";
                errorCode = 405;
            }
            exchange.sendResponseHeaders(errorCode, resposta.getBytes(StandardCharsets.UTF_8).length);
            OutputStream os = exchange.getResponseBody();
            os.write(resposta.getBytes(StandardCharsets.UTF_8));
            os.close();
        }
    }
}

```

Aquest [exemple](exemples/metodes_web_server) permet executar diferents mètodes en funció del mètode que ens ha enviat el client a la seva petició. És una simulació.

Per poder-ho executar hauràs de fer-ho des d'un navegador amb [Postman](https://www.postman.com/) o l'eina [curl](https://curl.se/).

```Bash
curl http://localhost:8000/
```
```Bash
curl -d "param1=value1&param2=value2" -X POST http://localhost:8000/
```

### Client web

Els **clients web** són aplicacions que permeten als usuaris interactuar amb els continguts disponibles a Internet, principalment a través del protocol HTTP/HTTPS. Inclouen navegadors web, clients d'API, eines de desenvolupament i scripts automatitzats que realitzen peticions a servidors web. Els clients web més comuns són els navegadors, que interpreten el codi HTML, CSS i JavaScript per mostrar pàgines web de manera interactiva. A continuació, es detallen alguns dels clients web (navegadors) més utilitzats i clients d'API per a desenvolupadors:

#### Navegadors Web

* **_[Google Chrome](https://www.google.com/chrome/)_**: és un navegador web ràpid, segur i de codi tancat basat en el projecte de codi obert Chromium. És el navegador més popular del món i destaca per la seva velocitat, seguretat i extensibilitat mitjançant extensions.

* **_[Mozilla Firefox](https://www.mozilla.org/firefox/)_**: és un navegador web de codi obert conegut per la seva personalització, privacitat i eines de desenvolupament. Té un enfocament fort en la protecció de la privacitat de l'usuari.

* **_[Safari](https://www.apple.com/safari/)_**: és el navegador web desenvolupat per Apple, inclos amb macOS i iOS. Destaca per la seva eficiència energètica, integració amb l'ecosistema d'Apple i funcions de privacitat.

* **_[Microsoft Edge](https://www.microsoft.com/edge)_**: és el navegador web desenvolupat per Microsoft, inclòs amb Windows 10 i versions posteriors. Basat en Chromium des del 2020, ofereix integració amb els serveis de Microsoft i eines de privacitat.

#### Clients d'API i eines de desenvolupament

* **_[Postman](https://www.postman.com/)_**: plataforma popular per a desenvolupadors que facilita el desenvolupament, proves i documentació d'APIs. Permet realitzar peticions HTTP, personalitzar capçaleres, inspeccionar respostes i molt més.

* **_[cURL](https://curl.se/)_**: eina de línia d'ordres utilitzada per transferir dades amb URL. Suporta diversos protocols, que inclou HTTP, HTTPS, FTP, entre d'altres, i és àmpliament utilitzada per a proves d'APIs i scripts automatitzats.

* **_[Insomnia](https://insomnia.rest/)_**: aplicació de codi obert que proporciona una interfície gràfica d'usuari per provar i depurar API's. Suporta GraphQL, REST i gRPC, facilitant la creació, organització i execució de peticions HTTP.


Òbviament, Java, també ens permet realitzar connexions. Per connectar-se a un servidor web i realitzar sol·licituds HTTP des d'un client en Java, es pot utilitzar la classe `HttpURLConnection`, que és una subclasse de `URLConnection` i proporciona suport per a les característiques específiques de les connexions HTTP. Aquesta classe permet enviar sol·licituds GET i POST, llegir respostes del servidor, així com gestionar les capçaleres de sol·licitud i resposta.

#### Exemple: connexió senzilla a un servidor web

```java
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ClientWeb {
    public static void main(String[] args) {
        // Defineix l'URL del recurs que vols sol·licitar
        String urlString = "http://www.exemple.cat";

        try {
            // Crea un objecte URL
            URL url = new URL(urlString);

            // Obre una connexió HTTP a l'URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Configura el mètode de la sol·licitud (GET és el mètode per defecte)
            connection.setRequestMethod("GET");

            // Obté el codi de resposta de la connexió
            int responseCode = connection.getResponseCode();
            System.out.println("Codi de Resposta: " + responseCode);

            // Llegeix la resposta del servidor
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();

            // Mostra la resposta completa
            System.out.println(response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

👉 Prova el [codi anterior](exemples/client_web) i no oblidis posar una adreça web vàlida !!

## Introducció a l'arquitectura d'aplicacions

L'arquitectura d'una aplicació es refereix a l'organització estructurada del seu codi i components, dissenyada per facilitar el desenvolupament, l'escalabilitat i el manteniment de l'aplicació. Una manera útil de conceptualitzar una aplicació és considerar-la com a dividida en quatre components principals:

1. **Dades**: El conjunt de dades amb què l'aplicació treballa, emmagatzemades típicament en bases de dades. Les dades són el cor de moltes aplicacions, proporcionant la informació necessària per realitzar tasques útils.

2. **Lògica d'Accés a Dades**: La capa que gestiona l'accés i la manipulació de les dades. Aquesta capa abstrau les dades de la seva font d'emmagatzematge (com ara una base de dades SQL o NoSQL) i proporciona mètodes per recuperar, crear, actualitzar i esborrar dades.

3. **Lògica de l'Aplicació**: També coneguda com la capa de lògica de negoci, aquesta capa conté la funcionalitat central de l'aplicació, processant les entrades, realitzant càlculs o prenent decisions basades en les regles de negoci, i determina les sortides o accions a realitzar.

4. **Presentació**: La interfície amb la qual els usuaris interactuen, que pot ser una interfície gràfica d'usuari (GUI) en un navegador web, una aplicació mòbil, o fins i tot una API per a altres programes. Aquesta capa es centra en la visualització de dades i la captura d'entrades de l'usuari.

### Què és una API

Les APIs (Interfícies de Programació d'Aplicacions) són un exemple clau d'arquitectura d'aplicacions que separa clarament aquests quatre components. 

#### Cicle de vida d'una API

El cicle de vida d'una API descriu les fases per les quals passa una API des del seu disseny inicial fins a la seva retirada. Aquest cicle de vida pot variar lleugerament depenent de l'organització o del projecte, però en general inclou les següents etapes:

1. **Planificació i anàlisi**: determinació de les necessitats dels usuaris i definició dels objectius de l'API.
2. **Disseny**: especificació de l'API, incloent la definició dels endpoints, mètodes, paràmetres, formats de missatges i codis d'estat.
3. **Desenvolupament**: implementació de l'API basada en l'especificació de disseny.
4. **Proves**: validació de la funcionalitat, rendiment, seguretat i compatibilitat de l'API.
5. **Desplegament**: posada en producció de l'API en un entorn accessible pels consumidors.
6. **Gestió i monitoratge**: supervisió de l'ús de l'API, rendiment i seguretat; gestió dels accessos i versions.
7. **Depreciació i retirada**: notificació als consumidors sobre la finalització de l'API i retirada final del servei.

![Cicle de vida d'una API](/resources/cicle_vida_api.png "API Life Cycle"){width=50%}


Algunes API força utilitzades són:

* **_[Google Maps Platform](https://developers.google.com/maps)_**: proporciona APIs per integrar mapes, geocodificació, rutes i altres serveis relacionats amb localització en aplicacions web i mòbils.

* **_[Twitter API](https://developer.twitter.com/en/docs)_**: permet als desenvolupadors accedir a dades de Twitter, publicar tweets, obtenir informació d'usuaris i més.

* **_[Facebook Graph API](https://developers.facebook.com/docs/graph-api)_**: la principal manera de llegir i escriure a la graella social de Facebook, permetent integrar funcionalitats de Facebook en aplicacions externes.

* **_[Spotify Web API](https://developer.spotify.com/documentation/web-api/)_**: ofereix accés a informació relacionada amb catàleg musical de Spotify, playlists, recomanacions i més.

* **_[GitHub API](https://docs.github.com/en/rest)_**: permet interactuar amb GitHub per obtenir informació sobre repositoris, usuaris, commits i més.


Un cas particular i àmpliament utilitzat són les **APIs RESTful**. Sota el protocol HTTP proporcionen una manera estandarditzada per què els clients web (com navegadors o aplicacions mòbils) interactuïn amb els serveis de l'aplicació.

#### Característiques de l'API RESTful:

- **Sense estat**: Cada petició de l'API conté tota la informació necessària per al servidor per processar-la. Això fa que les interaccions entre client i servidor siguin independents unes de les altres.
- **Recursos**: Els components de dades de l'aplicació són conceptualitzats com a "recursos" que poden ser creats, llegits, actualitzats i esborrats (operacions CRUD) mitjançant peticions HTTP utilitzant mètodes com GET, POST, PUT i DELETE.
- **Representacions**: Els recursos poden ser representats en diversos formats com JSON o XML, permetent una fàcil integració amb diferents tipus de clients.

#### Beneficis de les APIs RESTful:

- **Separació entre client i servidor**: la lògica de presentació (client) es separa de la lògica de l'aplicació i l'accés a dades (servidor), facilitant el desenvolupament i manteniment.
- **Escalabilitat**: com que el servidor no emmagatzema estat de la clientela, és més fàcil escalar l'aplicació afegint més servidors.
- **Flexibilitat i portabilitat**: _front-ends_ diferents (web, mòbil, desktop) poden reutilitzar la mateixa API _back-end_.
- **Independència de plataforma**: les APIs RESTful poden ser consumides per qualsevol client que suporti peticions HTTP.
