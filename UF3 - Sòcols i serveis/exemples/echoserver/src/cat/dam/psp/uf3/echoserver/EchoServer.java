package cat.dam.psp.uf3.echoserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
    public static void main(String[] args) {
        int portNumber = 1234; // Port on escolta el servidor. Alerta, per norma el port ha de ser major de 1024

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("EchoServer està escoltant al port " + portNumber);
            while (true) {
                try (Socket clientSocket = serverSocket.accept();
                     PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                     BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {

                    InetAddress clientAddress = clientSocket.getInetAddress();
                    System.out.println("Client amb adreça IP " + clientAddress.getHostAddress() + " s'ha connectat.");

                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        System.out.println("El client amb adreça IP " + clientAddress.getHostAddress() + " ha enviat: " + inputLine);
                        out.println(inputLine);
                    }
                    System.out.println("El client amb adreça IP " + clientAddress.getHostAddress() + " s'ha desconnectat.");
                } catch (IOException e) {
                    System.out.println("Excepció capturada quan s'intentava comunicar amb un client.");
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            System.out.println("Excepció capturada quan s'intentava escoltar al port " + portNumber);
            System.out.println(e.getMessage());
        }
    }
}