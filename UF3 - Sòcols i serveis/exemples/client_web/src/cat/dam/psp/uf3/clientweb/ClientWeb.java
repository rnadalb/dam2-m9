package cat.dam.psp.uf3.clientweb;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ClientWeb {
    public static void main(String[] args) {
        // Defineix l'URL del recurs que vols sol·licitar
        String urlString = "http://m13.ies-eugeni.cat";

        try {
            // Crea un objecte URL
            URL url = new URL(urlString);

            // Obre una connexió HTTP a l'URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Configura el mètode de la sol·licitud (GET és el mètode per defecte)
            connection.setRequestMethod("GET");

            // Obté el codi de resposta de la connexió
            int responseCode = connection.getResponseCode();
            System.out.println("Codi de Resposta: " + responseCode);

            // Llegeix la resposta del servidor
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();

            // Mostra la resposta completa
            System.out.println(response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
