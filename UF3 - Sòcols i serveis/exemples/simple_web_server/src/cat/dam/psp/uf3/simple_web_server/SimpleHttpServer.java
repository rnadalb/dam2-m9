package cat.dam.psp.uf3.simple_web_server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class SimpleHttpServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        // El path indica la ruta (path) que haurem d'introduir al nostre navegador
        server.createContext("/welcome", new WelcomeHandler());
        server.setExecutor(null); // Utilitza un executor per defecte
        server.start();
        System.out.println("El servidor està escoltant al port 8000");
    }

    static class WelcomeHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "Benvingut al Servei Web Simple!";
            // El codi 200 indica que tot ha estat bé !
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}
