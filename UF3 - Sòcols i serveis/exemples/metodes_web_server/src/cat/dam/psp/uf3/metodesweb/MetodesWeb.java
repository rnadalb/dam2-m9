package cat.dam.psp.uf3.metodesweb;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class MetodesWeb {
    private static final int DEFAULT_PORT = 8000;

    public static void main(String[] args) throws Exception {
        // Crea un servidor HTTP escoltant en el port 8000
        HttpServer server = HttpServer.create(new InetSocketAddress(DEFAULT_PORT), 0);

        // Gestor per a les sol·licituds GET i POST a la ruta "/"
        server.createContext("/", new HomePage());
        server.setExecutor(null); // Crea un executor per defecte
        server.start();
        System.out.println("El servidor està escoltant en el port " + DEFAULT_PORT);
    }

    private static String handleGetRequest(HttpExchange exchange) {
        // En un cas real, llegiries el cos de la sol·licitud aquí
        return "<H1>Resposta del mètode GET<H1>";
    }


    private static String handlePostRequest(HttpExchange exchange) {
        // Llegeix el cos de la sol·licitud
        InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String formData = br.lines().collect(Collectors.joining(System.lineSeparator()));

        // Mostra els paràmetres rebuts
        System.out.println("Dades rebudes en la sol·licitud POST: " + formData);

        return "Resposta del mètode POST amb dades: " + formData;
    }


    static class HomePage implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String resposta;
            int errorCode = 200;
            // Obtenim el mètode que ens ha enviat el client
            if ("GET".equals(exchange.getRequestMethod())) {
                resposta = handleGetRequest(exchange);
            } else if ("POST".equals(exchange.getRequestMethod())) {
                resposta = handlePostRequest(exchange);
                errorCode = 201;
            } else {
                resposta = "Mètode no suportat";
                errorCode = 405;
            }
            exchange.sendResponseHeaders(errorCode, resposta.getBytes(StandardCharsets.UTF_8).length);
            OutputStream os = exchange.getResponseBody();
            os.write(resposta.getBytes(StandardCharsets.UTF_8));
            os.close();
        }
    }
}
